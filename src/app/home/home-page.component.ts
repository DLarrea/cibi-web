import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from 'src/app/dialog/login/login.component';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent {

  constructor(
    public dialog: MatDialog
  ) { }

  openLogin(): void {
    const loginDialog = this.dialog.open(LoginComponent, {
      disableClose: true,
      data: {
      }
    })
    .afterClosed();

    // loginDialog.afterClosed().subscribe(data => {
    //   // Preguntar si se presiono sobre Cancelar
    //   const isConfirm = typeof data === 'boolean' && data === true;
    //   if (isConfirm) {
    //     this.snackBarService.openSnackBar({
    //       message: ('Logeando')
    //     }as Snackbar);
    //   }
    // });
  }

  onLogin(val: any): void {
    if(val){
      this.openLogin();
    }
  }
}
