import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { url } from 'src/app/environments/url';
import { HomePageComponent } from './home-page.component';

const routes: Routes = [
  // { path: '', redirectTo: url.home._root, pathMatch: 'full' },
  {
    path: url.home._root,
    component: HomePageComponent,
    children: [
      {
        path: url.home._root,
        loadChildren: () =>
          import('src/app/pages/landing/landing.module').then((m) => m.LandingModule),
      },
      {
        path: url.register._root,
        loadChildren: () =>
          import('src/app/pages/register/register.module').then((m) => m.RegisterModule),
      },
      {
        path: url.products._root,
        loadChildren: () =>
          import('src/app/pages/products/products.module').then((m) => m.ProductsModule),
      },
      {
        path: url.mycarts._root,
        loadChildren: () =>
          import('src/app/pages/mycarts/all-my-carts/all-my-carts.module').then((m) => m.AllMyCartsModule),
      },
      {
        path: url.myaddress._root,
        loadChildren: () =>
          import('src/app/pages/my-address/my-address.module').then((m) => m.MyAddressModule),
      },
      {
        path: url.shoppinghistory._root,
        loadChildren: () =>
          import('src/app/pages/shopping-history/shopping-history.module').then((m) => m.ShoppingHistoryModule),
      },
      {
        path: url.mypreferences._root,
        loadChildren: () =>
          import('src/app/pages/my-preferences/my-preferences.module').then((m) => m.MyPreferencesModule),
      },
      {
        path: url.mybilling._root,
        loadChildren: () =>
          import('src/app/pages/my-billing/my-billing.module').then((m) => m.MyBillingModule),
      },
      {
        path: url.myreport._root,
        loadChildren: () =>
          import('src/app/pages/my-report/my-report.module').then((m) => m.MyReportModule),
      },
      {
        path: url.videos._root,
        loadChildren: () =>
          import('src/app/pages/videos/videos.module').then((m) => m.VideosModule),
      },
      {
        path: url.mypurchasebuy._root,
        loadChildren: () =>
          import('src/app/pages/my-purchase-checkout/my-purchase-checkout.module')
            .then((m) => m.MyPurchaseCheckoutModule),
      },
      {
        path: url.changepassword._root,
        loadChildren: () =>
          import('src/app/pages/change-password/change-password.module').then((m)=> m.ChangePasswordModule)
      },
      {
        path: url.profile._root,
        loadChildren: () =>
        import('src/app/pages/profile/profile.module').then((m)=> m.ProfileModule)
      },
      {
        path: url.storesnearby._root,
        loadChildren: () =>
        import('src/app/pages/register-list-stores-nearby/register-list-stores-nearby-routing.module').then((m)=> m.RegisterListStoresNearbyRoutingModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule { }
