import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material/material.module';
import { HomePageComponent } from './home-page.component';
import { LandingModule } from '../pages/landing/landing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastComponent } from './toast/toast.component';

//primeng
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';

@NgModule({
  declarations: [
    HomePageComponent,
    ToastComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    MaterialModule,
    LandingModule,
    ReactiveFormsModule,
    ToastModule
  ],
  providers: [
    MessageService
  ]
})
export class HomeModule { }
