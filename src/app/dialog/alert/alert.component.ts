import { Component, Inject, OnInit } from '@angular/core';
import { MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { Snackbar } from 'src/app/interfaces/snackbar';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  alertColors = {};

  constructor(
    private sB: MatSnackBar,
    @Inject(MAT_SNACK_BAR_DATA) public data: Snackbar
  ) { }

  ngOnInit( ): void {
    this.alertColors = {
      error: this.data.classAction === 'error',
      success: this.data.classAction === 'success',
      info: this.data.classAction === 'info',
      warning: this.data.classAction === 'warning',
    };
  }

  dismiss(): void{
    this.sB.dismiss();
  }

}
