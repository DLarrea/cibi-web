import { Injectable } from '@angular/core';
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Observable, of} from "rxjs";
import {map, take} from "rxjs/operators";
import {ConfirmActionComponent} from "./confirm-action.component";

@Injectable({
  providedIn: 'root'
})
export class ConfirmActionService {

  dialogRef: MatDialogRef<ConfirmActionComponent> | undefined;

  constructor(private dialog: MatDialog) { }

  public open(options: any) {
    this.dialogRef = this.dialog.open(ConfirmActionComponent, {
      data: {
        title: options.title,
        message: options.message,
        cancelText: options.cancelText,
        confirmText: options.confirmText
      }
    });
  }
  public confirmed(): Observable<any> {
    if (this.dialogRef) {
      return this.dialogRef.afterClosed().pipe(take(1), map(res => {
          return res;
        }
      ));
    }
    return of(null);
  }
}
