import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';


@Component({
  selector: 'app-stores-nearby-maps',
  templateUrl: './stores-nearby-maps.component.html',
  styleUrls: ['./stores-nearby-maps.component.scss']
})
export class StoresNearbyMapsComponent implements OnInit {

  lat: number = 0;
  lng: number = 0;

  constructor(
    public dialogRef: MatDialogRef<StoresNearbyMapsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

  ngOnInit(): void {
    this.lat = this.data.lat_lng.lat;
    this.lng = this.data.lat_lng.lng;
    console.log("lat and lng: ", this.lat, this.lng);
  }


  close(){
    this.dialogRef.close();
  }
}
