import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoresNearbyMapsComponent } from './stores-nearby-maps.component';

describe('StoresNearbyMapsComponent', () => {
  let component: StoresNearbyMapsComponent;
  let fixture: ComponentFixture<StoresNearbyMapsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoresNearbyMapsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoresNearbyMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
