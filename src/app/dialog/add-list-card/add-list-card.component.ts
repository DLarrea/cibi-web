import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { CreateListModel } from 'src/app/models/cart';
import { Product } from 'src/app/models/product';
import { ListService } from 'src/app/services/list.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-add-list-card',
  templateUrl: './add-list-card.component.html',
  styleUrls: ['./add-list-card.component.scss']
})
export class AddListCardComponent implements OnInit {

  form = this.formBuilder.group({
    listName: [null, Validators.required],
    search: ''
  });

  get name(): FormControl {
    return this.form.get('listName') as FormControl;
  }

  get search(): FormControl {
    return this.form.get('search') as FormControl;
  }

  headText = "";
  searchValue = "";
  productList: Product[] = [];
  productToAdd: Product[] = [];
  list: CreateListModel = new CreateListModel();
  listItems: any[] = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public formBuilder: FormBuilder,
    private productService: ProductService,
    public dialogRef: MatDialogRef<AddListCardComponent>,
    private listService: ListService
  ) { }

  ngOnInit(): void {
    // console.log(this.data.listInfo)
    this.getData();
    this.setType();
    console.log("Asd", this.data)
    if(!this.data.isToCreate){
      // this.list.products = this.data.listInfo;
      // this.listInfo = this.data.listInfo;
    }
  }

  getData(): void {
    const getProducts$ = this.productService.getProducts();

    forkJoin([getProducts$]).subscribe(
      results => {
        this.productList = results[0];
      },
      errors => {
        console.log(errors[0]);
      }
    );
  }

  setType(): void{
    if(this.data.agregarItems){
      this.headText = "Crear Lista";
    } else if (!this.data.agregarItems) {
      this.headText = "Agregar productos a la lista";
    }
  }

  getSearchData(): void {
    if (!!this.searchValue.length && this.searchValue.length >= 3) {
      const getProducts$ =  this.productService.getAllPopularProducts(this.searchValue);

      forkJoin([getProducts$]).subscribe(
        results => {
          this.productList = results[0];
        },
        errors => {
          console.log(errors[0]);
        }
      );
    } else if (!this.searchValue.length) {
      console.log("Se volvio!!");
      //this.getData();
    }
  }

  productAddToList(item: Product): void {
    if(item.checked) {
      this.productToAdd.push(item);
      this.list.products.push(item);
    } else {
      this.list.products.splice(this.list.products.indexOf(item), 1);
      this.productToAdd.splice(this.productToAdd.indexOf(item), 1);
    }
  }

  async createList(): Promise<void> {
    const list = {
      name: this.form.value.listName,
      cart_id: this.data.cartId,
      shopping_list_items: []
    };

    await this.listService.createList(list).toPromise().then((resp: any) => {
      console.log(resp);
      this.list.products.forEach(item => {
        const data = {
          product_id: item.id,
          qty: 1,
          list_id: resp.id,
          choose_item: false,
        };
        console.log('data: ', data);
        this.listService.addItem(data).subscribe((result: any) => {
          console.log(result);
        });
      });
    });
    this.dialogRef.close(this.form.value);
  }

  addItemList() {
    console.log(this.list)
    let shoppingListItems: any[] = [];
    this.list.products.forEach(item => {
      const data = {
        product_id: item.id,
        qty: 1,
        list_id: this.data.listId,
        choose_item: true,
      };
      shoppingListItems.push(data);
    });
    console.log("listafull: ", shoppingListItems)
    // actualizar la lista completa
    this.listService.addToList(this.data.listId, shoppingListItems).subscribe(resp=> {
      console.log(resp);
      this.dialogRef.close(shoppingListItems);
    }, error => {
      console.log(error);
    });
  }

  close(): void {
    this.dialogRef.close(false);
  }

  @HostListener("keydown.esc")
  public onEsc() {
    this.close();
  }

}
