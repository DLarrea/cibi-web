import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Snackbar } from 'src/app/interfaces/snackbar';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { LoadingService } from 'src/app/services/loading.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isLoading$ = this.loadingService.getLoadingObservable();
  hide = true;
  recoverPassword = false;
  loginForm = this.formBuilder.group({
    username: [null, [Validators.required, Validators.email]],
    password: [null, Validators.required]
  });

  recoverForm = this.formBuilder.group({
    username: [null, [Validators.required, Validators.email]],
  });

  get username(): FormControl {
    return this.loginForm.get('username') as FormControl;
  }

  get password(): FormControl {
    return this.loginForm.get('password') as FormControl;
  }

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private dialogRef: MatDialogRef<LoginComponent>,
    public formBuilder: FormBuilder,
    private authService: AuthService,
    private cartService: CartService,
    private snackBarService: SnackbarService,
    private loadingService: LoadingService,
    private messageService: MessageService
    // private facebook: Facebook,
    // private googlePlus: GooglePlus,
    // private apple: SignInWithApple,
  ) { }

  ngOnInit(): void {
    if (this.data.firstTime) {
      this.loginForm.get('username')?.setValue(this.data.email);
    }
  }

  registrar(): void {
    this.dialogRef.close();
    this.router.navigate(['/register']);
  }

  login(): void {
    console.log("Se esta loading", this.isLoading$);
    const username = this.loginForm.get('username')?.value || '';
    const password = this.loginForm.get('password')?.value || '';
    this.authService.login(username, password).subscribe(
        resp => {
          this.messageService.add({key: 'toast-plub', severity:'info', summary:'Login exitoso', detail:`Bienvenido ${resp.user.name}`, life: 4000, closable: false})
          this.cartService.setCartActive$(resp.user.id);
          this.dialogRef.close();
          this.router.navigate(['/'], { replaceUrl: true });
        },
        error => {
          console.log(error);
          if (error.status === 401) {
            this.messageService.add({key: 'toast-plub', severity:'error', summary:'Error', detail:`Verifique sus credenciales` , life: 4000, closable: false})
          } else {
            this.messageService.add({key: 'toast-plub', severity:'error', summary:'Error', detail:`Ha ocurrido un error inesperado. Intente nuevamente...`, life: 4000, closable: false})
          }
        },
    );
  }

  close(): void {
    this.dialogRef.close();
  }
}
