import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Product, ProductGroup } from 'src/app/models/product';
import { CharacteristicsService } from 'src/app/services/characteristics.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  isLoading$ = this.loadingService.getLoadingObservable();
  isLoaded = false;
  imageApi = environment.image_api;

  /* Para el boton de agregar */
  showAdd = true;
  showTrash = false;
  value = 1;
  i = 1;
  
  /* Para las caracteristicas, tamaños y sus detalles */
  currentProductId = 0;
  currentproductGroupId: number = 0;
  charsSlide = [];
  productGroupList: ProductGroup[] = [];
  productVariants: Product[] = [];
  
  //productLabel = '';
  //productLabelShort = '';
  //productPrice = '';
  //currentProductQty = 1; 
  
  productRelatedList: Product[] = [];
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Product,
    private router: Router,
    public formBuilder: FormBuilder,
    public dialog: MatDialog,
    private loadingService: LoadingService,
    private userService: UserService,
    private snackBarService: SnackbarService,
    private productService: ProductService,
    private charsService: CharacteristicsService,
    public dialogRef: MatDialogRef<ProductDetailsComponent>,
  ) { }

  ngOnInit(): void { 
    this.charsSlide = [];
    this.setInitialData();
    this.getInitialData();
    this.getCharsByProduct();
    //this.isLoaded = true;
    //this.isLoading$;
  }

  setInitialData() {
    this.currentProductId = this.data.id;
  }

  getInitialData() {
    const productGroupList$ = this.productService.getProductGroup();
    productGroupList$.subscribe( result => {
      this.productGroupList = result;
      const indexProductGroup = this.productGroupList.findIndex(prodGroup => prodGroup.id === this.data.productGroupId);
      if (indexProductGroup > -1) {
        this.productVariants = this.productGroupList[indexProductGroup].variants
        console.log(this.productVariants);
      }
    })
    this.productService.getProducts().subscribe(
      resp => {
        this.productRelatedList = resp;
      }
    )
  }

  getCharsByProduct() {
    this.charsSlide = [];
    this.charsService.getCharsByProductId(this.currentProductId).subscribe(resp => {
      this.charsSlide = resp;
    });
  }

  // changeProductGroupVariant(product: any) {
  //   this.productGroupSelectedInPopUp.product = product;
  //   this.productLabel = this.productGroupSelectedInPopUp.product.name;
  //   this.productLabelShort = this.productGroupSelectedInPopUp.product.brand;
  //   this.productPrice = this.productGroupSelectedInPopUp.product.price.toLocaleString();
  //   this.currentProductQty = this.productGroupSelectedInPopUp.product.qty;
  //   this.getCharsByProduct();
  // }

  /* Funciones para el boton de agregar */
  handleMinus() {
    if (this.i > 0) {
      this.i--;
      this.value = this.i;
      if (this.i === 1) {
        this.showTrash = true;
      }
      console.log('i: ', this.i, 'value: ', this.value, 'trash: ', this.showTrash);
    }
  }

  handlePlus() {
    if (this.i < 10) {
      this.showTrash = false;
      this.i++;
      this.value = this.i;
    }
  }

  handleDelete() {
    this.showAdd = true;
  }

  close() {
    this.dialogRef.close(false);
  }

  productDetailsDialog(data: Product): void {
    const dialog = this.dialog
      .open(ProductDetailsComponent, {
        height: '810px',
        width: '1400px',
        disableClose: false,
        data,
      });

    // dialog.afterClosed().subscribe(data => {
    //   if (data) {
    //     const address = new Address();
    //     address.name = data.name;
    //     address.address = data.address;
    //     address.latLng = {"lat": data.lat,"lon": data.long};
    //     this.addressList.push(address); 
    //     console.log(data);
    //   }
    // });
  }
}
