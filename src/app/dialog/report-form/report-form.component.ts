import { NumberFormatStyle } from '@angular/common';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/interfaces/user';
import { ReportExpenses, ReportExpensesCategory } from 'src/app/models/report';


@Component({
  selector: 'app-report-form',
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.scss']
})
export class ReportFormComponent implements OnInit {

  title = 'Presupuesto mensual';

  form = this.formBuilder.group({
    totalForMonth: [null, Validators.required],
  });

  get totalForMonth(): FormControl {
    return this.form.get('totalForMonth') as FormControl;
  }

  @Input() user!: User;
  @Input() ultimoPresupuesto!: number;

  constructor(
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<ReportFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { reportEx: ReportExpenses, title: string, reportExCategory: ReportExpensesCategory }
  ) { }

  ngOnInit(): void {

    if (!this.data.title) this.data.title = "Presupuesto mensual";

    if (this.data && this.data.reportEx) {
      if (this.ultimoPresupuesto) {
        this.form.setValue({
          totalForMonth: this.ultimoPresupuesto,
        });
      }
    }
  }

  onSubmit() {
    this.dialogRef.close(this.form.value);
  }

  close(): void {
    this.dialogRef.close(false);
  }
}
