import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BillingInfo } from 'src/app/models/billing-info';

@Component({
  selector: 'app-billing-form',
  templateUrl: './billing-form.component.html',
  styleUrls: ['./billing-form.component.scss']
})
export class BillingFormComponent implements OnInit {

  form = this.formBuilder.group({
    razonsocial: [null, Validators.required],
    ruc: [null, Validators.required],
    // address: '',
    // phone: ''
  });

  get razonsocial(): FormControl {
    return this.form.get('razonsocial') as FormControl;
   }

   get ruc(): FormControl {
     return this.form.get('ruc') as FormControl;
   }

  //  get address(): FormControl {
  //    return this.form.get('address') as FormControl;
  //  }

  //  get phone(): FormControl {
  //    return this.form.get('phone') as FormControl;
  //  }

  constructor(
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<BillingFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { billing: BillingInfo, title: string }
  ) { }

  ngOnInit(): void {
    // do nothing.
    if (!this.data.title) this.data.title = "Crear facturación";

    if (this.data && this.data.billing) {
      //const initData = this.data.billing;
      // this.form.get('razonsocial')?.setValue(initData.razonSocial);
      // this.form.get('ruc')?.setValue(initData.ruc);
      // this.form.get('address')?.setValue(initData.address);
      // this.form.get('phone')?.setValue(initData.phone);
      //   }
      //}

    }
  }

  crear(): void {
    const formData = {
      razon_social: this.form.value.razonsocial,
      ruc: this.form.value.ruc,
    }
    this.dialogRef.close(formData);
  }

  close(): void {
    this.dialogRef.close(false);
  }

  @HostListener("keydown.esc")
  public onEsc() {
    this.close();
  }
}