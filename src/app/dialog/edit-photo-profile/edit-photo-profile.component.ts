import { Component, HostListener, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserPhotoService } from 'src/app/services/user-photo.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-photo-profile',
  templateUrl: './edit-photo-profile.component.html',
  styleUrls: ['./edit-photo-profile.component.scss']
})
export class EditPhotoProfileComponent implements OnInit {

  user: any;
  imageFile: File = new File([], '');
  imageApi = environment.image_api;

  profileForm = this.formBuilder.group({
    image: ['', Validators.required]
  });

  constructor(
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditPhotoProfileComponent>,
    private photoService: UserPhotoService,
    private userService: UserService) { }

  imageProgress(fileInput: any) {
    this.imageFile = fileInput.target.files[0] as File;
    this.profileForm.setValue({image:this.imageFile})
    console.log('name', this.imageFile);
  }


  ngOnInit(): void {
    this.getUserData()
  }

  getUserData() {
    this.userService.me().subscribe(resp => {
      this.user = resp;
      console.log(this.user)

    }, error => {
      console.log(error)
    });
  }


  onSubmit(): void {
    const fdImage = new FormData();
    fdImage.append('file', this.imageFile);
    console.log(fdImage);
    if (this.profileForm.valid) {
      this.photoService.ImgUser(this.user.id, this.imageFile);
      this.dialogRef.close(fdImage);
    }
    else {
      console.log("error");
    }
  }

}
