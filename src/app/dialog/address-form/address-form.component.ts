import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Address } from "../../models/address";

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit {
  latitude = -25.292547;
  longitude = -57.601337;
  locationChosen = false;

  form = this.formBuilder.group({
    name: [null, Validators.required],
    address: [null, Validators.required],
    lat_lng: null
  });

  get name(): FormControl {
    return this.form.get('name') as FormControl;
  }

  get address(): FormControl {
    return this.form.get('address') as FormControl;
  }

  constructor(
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AddressFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { address: Address, title: string }
  ) { }

  ngOnInit(): void {
    // do nothing.
    if (!this.data.title) this.data.title = "Crear dirección";

    if (this.data && this.data.address) {
      const initData = this.data.address;
      this.form.get('name')?.setValue(initData.name);
      this.form.get('address')?.setValue(initData.address);
      this.form.get('lat_lng')?.setValue(initData.lat_lng);

      if (initData.lat_lng) {
        this.latitude = initData.lat_lng.lat;
        this.longitude = initData.lat_lng.lng;
        this.locationChosen = true;
      }
    }
  }

  choseLocation(event: any) {
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.locationChosen = true;
    this.form.get('lat_lng')?.setValue({lat: this.latitude, lng: this.longitude})
  }

  crear(): void {
    this.dialogRef.close(this.form.value);
  }

  close(): void {
    this.dialogRef.close(false);
  }

  @HostListener("keydown.esc")
  public onEsc() {
    this.close();
  }

}
