import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { CartInfo } from 'src/app/models/cart';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { ListService } from 'src/app/services/list.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-shopping-lists',
  templateUrl: './shopping-lists.component.html',
  styleUrls: ['./shopping-lists.component.scss']
})
export class ShoppingListsComponent implements OnInit {

  user: any;
  shoppingList = [];
  cart: CartInfo = new CartInfo();
  order: any;

  purchaseType = 'pickup';
  userObservable$ = this.authService.getLoggedUserObservable();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private listService: ListService,
    private cartService: CartService,
    private orderService: OrderService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.userObservable$.subscribe(
      resp => {
        this.user = resp;
      }
    )
    this.getCartInfo(this.data.cart.id);
  }

  getCartInfo(cartId: number) {
    const user$ = this.authService.getLoggedUserObservable()
    const lists$ = this.listService.getByCartId(cartId);
    const orderActive$ = this.orderService.getActiveOrderByCart(cartId);

    forkJoin([lists$, orderActive$]).subscribe(
      results => {
      this.cart.listas = results[0];
      this.order = results [1];
      },
      error => {
        console.log("Error", error);
      }
    );
  }

  aceptar() {

    const lista = this.cart.listas?.filter(list => list.checked);
    console.log("Lista de de checked", lista);

    // if (lista?.length === 0) {
    //   this.loadingService.dismissLoading('addListas');
    //   this.dismiss();
    //   return;
    // }

    const ordenProductos: any = [];
    const cartProductos: any = [];

    if (!this.order) {
      if (lista) {
        lista.forEach(r => {
          r.shoppingListItems.forEach((item: any) => {
            const orderItem = {
              quantity: item.qty,
              product_id: item.productId,
              price: item.price,
            };
            const cartItem = {
              cart_id: item.cartId,
              product_id: item.productId,
              qty: item.qty,
              choose_item: item.chooseItem,
            };
            cartProductos.push(cartItem);
            ordenProductos.push(orderItem);
          });
        });

        const orden = {
          status: 'ACTIVE',
          purchase_type: this.purchaseType,
          total_amount: 0,
          owner_id: this.user.id,
          cart_id: this.cart.id,
          store_id: 1, // this.form.value.storeId, PREGUNTAR A BEA PORQUE PIDE YA AHORA
          order_items: ordenProductos
        };

        this.orderService.addOrder(orden).subscribe(resp => {
          console.log("Se anhadio bien la orden", resp);
          this.order = resp;
        });
      }
    } else {
      if (lista) {
        lista.forEach(r => {
          r.shoppingListItems.forEach((item: any) => {
            const orderItem = {
              quantity: item.qty,
              product_id: item.productId,
              price: item.price,
            };
            const cartItem = {
              cart_id: item.cartId,
              product_id: item.productId,
              qty: item.qty,
              choose_item: item.chooseItem,
            };
            ordenProductos.push(orderItem);
            cartProductos.push(cartItem);
          });
        });

        const orderData = {
          order_items: ordenProductos,
        };
        this.orderService.addBulkOrder(orderData, this.order.id).subscribe((resp: any) => {
          console.log("Se anhadio bien la orden 2", resp);
          console.log('ORDEN ANADIR', resp);
        });

      }
    }

    const cartData = {
      cart_items: cartProductos,
    };
    this.cartService.addBulkItems(cartData, this.data.cart.id as number).subscribe((resp: any) => {
      console.log("Se añadio las listas al carrito", resp);
      // this.dismiss();
      // const newMensaje = 'Se ha agregado la lista "' + lista[lista.length - 1].name + '" con éxito';
      // const mensaje = lista.length > 1 ? '¡Se han agregado ' + lista.length + ' listas con éxito!' : newMensaje;
      // // presentar modal
      // const alertOptions = {
      //   title: 'AGREGAR LISTA',
      //   message: mensaje,
      //   btnConfirmar: 'Aceptar',
      //   alertClass: 'success'
      // } as AlertModalInterface;
      // this.loadingService.dismissLoading('addListas');
      // this.alertService.showAlertModal(alertOptions);
    });
  }

}
