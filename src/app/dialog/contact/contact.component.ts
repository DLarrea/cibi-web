import { Component, OnInit } from '@angular/core';
import { CallCenterService } from 'src/app/services/call-center.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  callCenters: any[] = [];
  whatsapp: any = null;
  llamadas: any = null;

  constructor(private callCenterService: CallCenterService) { }

  ngOnInit(): void {
    this.getCallCenter();
  }

  getCallCenter() {
    this.callCenterService.getList().subscribe(
      data => {
        this.callCenters = data;
        this.whatsapp = this.callCenters.find(el => {
          return el.type == "WhatsApp";
        });
        this.llamadas = this.callCenters.find(el => {
          return el.type == "Llamadas";
        });
        console.log(data);
      },
      error => {
        this.callCenters = [];
      }
    )
  }
}
