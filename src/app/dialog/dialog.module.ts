import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './login/login.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';
import { HomeModule } from '../home/home.module';
import { AddressFormComponent } from './address-form/address-form.component';
import { AgmCoreModule } from '@agm/core';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ConfirmActionComponent } from './confirm-action/confirm-action.component';
import { ConfirmActionService } from "./confirm-action/confirm-action.service";
import { AddListCardComponent } from './add-list-card/add-list-card.component';
import { InviteCartComponent } from './invite-cart/invite-cart.component';
import { ShoppingListsComponent } from './shopping-lists/shopping-lists.component';
import { AlertComponent } from './alert/alert.component';
import { BillingFormComponent } from './billing-form/billing-form.component';
import { ReportFormComponent } from './report-form/report-form.component';
import { AddProductComponent } from './add-product/add-product.component';
import { EditPhotoProfileComponent } from './edit-photo-profile/edit-photo-profile.component';
import { ContactComponent } from './contact/contact.component';
import { StoresNearbyMapsComponent } from './stores-nearby-maps/stores-nearby-maps.component';

//primeng
import {CardModule} from 'primeng/card';
import {AvatarModule} from 'primeng/avatar';
import {TooltipModule} from 'primeng/tooltip';

@NgModule({
  declarations: [
    LoginComponent,
    RecoverPasswordComponent,
    AddressFormComponent,
    ProductDetailsComponent,
    ConfirmActionComponent,
    AddListCardComponent,
    InviteCartComponent,
    ShoppingListsComponent,
    AlertComponent,
    BillingFormComponent,
    ReportFormComponent,
    AddProductComponent,
    EditPhotoProfileComponent,
    ContactComponent,
    StoresNearbyMapsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    HomeModule,
    FormsModule,
    CardModule,
    AvatarModule,
    TooltipModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCw7vbSy9ID2KmpyTbcBTCnoyDc2kSthLA'
    }),
  ],
  providers: [ConfirmActionService]
})
export class DialogModule { }
