import { HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Snackbar } from 'src/app/interfaces/snackbar';
import { LoadingService } from 'src/app/services/loading.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent {
  isLoading$ = this.loadingService.getLoadingObservable();
  isRecoverPassword = false;
  pattern = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

  recoverForm = this.formBuilder.group({
    username: [null, [Validators.required, Validators.pattern(this.pattern)]],
  });

  get username(): FormControl {
    return this.recoverForm.get('username') as FormControl;
  }

  @Input() set recorverPassword(val: boolean) {
    setTimeout(() => {
      this.isRecoverPassword = val;
    });
  }

  @Output() recoverEvent = new EventEmitter<boolean>();

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    private loadingService: LoadingService,
    private userService: UserService,
    private snackBarService: SnackbarService,
  ) { }

  userRecoverPassword() {
    if(this.recoverForm.valid){
        this.userService.recoverPassword(this.recoverForm.value.username).subscribe(res => {
          this.isLoading$;
          this.snackBarService.openSnackBar({
            message: ('Se ha enviado un email para recuperar su contraseña')
          }as Snackbar);
          this.recorverPassword = true;
          this.back();
        }, error => {
          this.loadingService.getLoadingObservable();
          console.log(error);
          if(error.status == 400){
            this.snackBarService.openSnackBar({
              code: error.status,
              message: ('No se encontro un usuario con el correo ingresado')
            }as Snackbar);
          }
        })
    }else{
      this.snackBarService.openSnackBar({
        message: ('Por favor ingrese su email para recuperar su contraseña')
      }as Snackbar) ;
    }
    // const username = this.recoverForm.get('username')?.value || '';
    // if(this.username === null){
    //   this.snackBarService.openSnackBar({
    //     message: ('Complete los campos' +
    //     'Por favor ingrese su email para recuperar su contraseña')
    //   }as Snackbar) ;
    // } else {
    //   this.userService.recoverPassword(username).subscribe(
    //     resp => {
    //       this.snackBarService.openSnackBar({
    //         message: ("Se ha enviado un email para recuperar la contraseña")
    //       }as Snackbar) ;
    //       this.router.navigate(['/account/login'], { replaceUrl: true });
    //     },
    //     error => {
    //       console.log(error);
    //       if (error.status === 400) {
    //         this.snackBarService.openSnackBar({
    //           code: error.status,
    //           message: ('No se encontro un usuario con el mail ingresado')
    //         }as Snackbar);
    //       } else {
    //         this.snackBarService.openSnackBar({
    //           code: error.status,
    //           message: ('Ha ocurrido un error ' + 'Vuelva a intentar mas tarde')
    //         }as Snackbar);
    //       }
    //     },
    //   );
    // }
  }

  back(): void {
    if(this.isRecoverPassword === true){
      this.recoverEvent.emit(false);
    }
  }
}
