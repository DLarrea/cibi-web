import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteCartComponent } from './invite-cart.component';

describe('InviteCartComponent', () => {
  let component: InviteCartComponent;
  let fixture: ComponentFixture<InviteCartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InviteCartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
