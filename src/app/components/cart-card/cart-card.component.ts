import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CartInfo } from 'src/app/models/cart';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-cart-card',
  templateUrl: './cart-card.component.html',
  styleUrls: ['./cart-card.component.scss']
})
export class CartCardComponent implements OnInit {
  @Input() cart: CartInfo = {};
  @Input() user: any;
  @Output() selectionClick = new EventEmitter<any>();
  @Output() selectionCart = new EventEmitter<any>();
  @Output() sharedCart = new EventEmitter<any>();

  hideSharedButton = false;

  constructor() { }

  ngOnInit(): void {
    if (this.cart?.default) {
      this.cart.showIconLeft = false;
      this.cart.showIconRight = false;
    }
    else if (this.cart?.ownerId === this.user.id) {
      this.cart.showIconLeft = true;
      this.cart.showIconRight = true;
    }
  }

  onSelectCartLogo(cart: any) {
    this.selectionCart.emit(cart);
  }

  onSelectCart(descriptionCard: CartInfo) {
    this.selectionClick.emit(descriptionCard);
  }

  onSharedCart(cart: any) {
    this.sharedCart.emit(cart);
  }

}
