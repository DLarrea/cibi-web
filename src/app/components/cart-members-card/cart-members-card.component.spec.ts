import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartMembersCardComponent } from './cart-members-card.component';

describe('CartMembersCardComponent', () => {
  let component: CartMembersCardComponent;
  let fixture: ComponentFixture<CartMembersCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartMembersCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartMembersCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
