import { Component, Input } from '@angular/core';
import { Contact } from 'src/app/models/cart';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cart-members-card',
  templateUrl: './cart-members-card.component.html',
  styleUrls: ['./cart-members-card.component.scss']
})
export class CartMembersCardComponent {

  imageApi = environment.image_api;

  @Input() cart: any;
  @Input() membersList?: Contact[] = [];

  constructor() { }



}
