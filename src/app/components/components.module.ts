import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartCardComponent } from './cart-card/cart-card.component';
import { MaterialModule } from '../material/material.module';
import { CartMembersCardComponent } from './cart-members-card/cart-members-card.component';
import { CartListCardComponent } from './cart-list-card/cart-list-card.component';
import { ShoppingHistoryCardComponent } from './shopping-history-card/shopping-history-card.component';



@NgModule({
  declarations: [
    CartCardComponent,
    CartMembersCardComponent,
    CartListCardComponent,
    ShoppingHistoryCardComponent
  ],
  exports: [
    CartCardComponent,
    CartMembersCardComponent,
    CartListCardComponent,
    ShoppingHistoryCardComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class ComponentsModule { }
