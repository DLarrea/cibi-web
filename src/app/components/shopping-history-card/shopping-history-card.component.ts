import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CartInfo, Order, OrderFull } from 'src/app/models/cart';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-shopping-history-card',
  templateUrl: './shopping-history-card.component.html',
  styleUrls: ['./shopping-history-card.component.scss']
})
export class ShoppingHistoryCardComponent implements OnInit {
  @Input() data: OrderFull = new OrderFull;
  cart: CartInfo = new CartInfo();
  order: OrderFull = new OrderFull();
  qty = 0;

  pType = [
    {
      type: 'delivery',
      icon: 'home-outline-orange'
    },
    {
      type: 'pickup',
      icon: 'car-sport-outline-orange',
    }
  ];
  user: any;
  userObservable$ = this.authService.getLoggedUserObservable();

  @Output() selectSeeProducts = new EventEmitter<any>();



  constructor(
    private authService: AuthService,
    private orderService: OrderService,
    private cartService: CartService,
    private router: Router
    ) {}

  ngOnInit(): void {
    console.log(this.data)
    if(this.data.items?.length){
      this.qty = this.data.items?.length;
    }

    this.userObservable$.subscribe(data => {
      this.user = data;
    });

    this.cartService.getcartActiveObservable().subscribe(res => {
      this.cart = res
      console.log("cart, ", this.cart)
      if(this.cart.id){
        this.orderService.getActiveOrderByCart(this.cart.id).subscribe((resp: any) => {
          this.order = resp
          console.log(this.order)
        })
      }
    })
  }

  onSelectSeeProducts (id: number | undefined): void {
    this.selectSeeProducts.emit(id);
  }

  async aceptar() {
    // const successOptions = {
    //   title: 'COMPRAR DE NUEVO',
    //   message: 'Se han agregado correctamente los productos al carrito',
    //   btnConfirmar: 'Aceptar',
    //   alertClass: 'success'
    // } as AlertModalInterface;

    const ordenProductos = [] as any;
    const parseProductos = [] as any;
    const productos = [] as any;
    // this.globalService.setCurrentCart(this.cart); // SET CURRENT CARRITO

    if(this.data.items){
      this.data?.items.forEach(item => {
        // PARSEANDO cada CARTITEM
        const data = {
          cart_id: this.cart.id,
          product_id: item.product?.id,
          qty: item.quantity,
          choose_item: false,
          price: item.price,
          length: this.data.items?.length
        };
        productos.push(data);
      });
    }
    console.log("ORDEN: ", this.order)
    if (!this.order) {
      // TIPO DE COMPRA / FORMA DE COMPRA
      console.log("CREA UNA ORDEN")

      // const purchaseType = this.globalService.getPurchaseCheckedData();
      // if (!purchaseType || purchaseType.value === 'store') {
      //   this.coreService.presentModalPurchaseType(purchaseType, this.routerOutlet);
      //   return;
      // }


      productos.forEach((r: any) => {
        const orderItem = {
          quantity: r.qty,
          product_id: r.product_id,
          price: r.price,
        };
        ordenProductos.push(orderItem);
      });
      const orden = {
        status: 'ACTIVE',
        purchase_type: 'delivery',
        total_amount: 0,
        owner_id: this.user.id,
        cart_id: this.cart.id,
        // product_group: null,
        store_id: 1, // this.form.value.storeId,
        order_items: ordenProductos,
      };


      this.orderService.addOrder(orden).subscribe(resp => {
        console.log(resp)
      });

    } else {
      productos.forEach((p: any) => { // PARSEA PRODUCTOS PARA ANHADIR TODOS DE ONE
        const orderItem = {
          quantity: p.qty,
          product_id: p.product_id,
          price: p.price
        };
        parseProductos.push(orderItem);
      });

      const orderData = {
        order_items: parseProductos,
      };
      if(this.order.id){
        this.orderService.addBulkOrder(orderData, this.order.id).subscribe(resp => {
          console.log(resp);
        }, error => {
          console.log(error);
        });
      }
    }

    const cartData = {
      cart_items: productos,
    };
    if(this.cart.id){
    this.cartService.addBulkItems(cartData, this.cart.id).subscribe(async (resp: any) => {
      // this.globalService.setCurrentCart(this.cart); // PARA SETEAR ITEMS EN CARRITO
      // actualizar listado de productos del carrtito y total  para el modal
      if(this.cart.id){
      const wait = await this.getProductCartsItem(this.cart.id);
      }
      // this.loadingService.dismissLoading('addBulkItems');
      // this.alertService.showAlertModal(successOptions);
    });
    }
  }

  getProductCartsItem(cartId: number) {
    this.cartService.getCartItems(cartId).subscribe(resp => {
      console.log(resp)
      this.router.navigate(['/products']);
    });
  }
}
