import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingHistoryCardComponent } from './shopping-history-card.component';

describe('ShoppingHistoryCardComponent', () => {
  let component: ShoppingHistoryCardComponent;
  let fixture: ComponentFixture<ShoppingHistoryCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShoppingHistoryCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingHistoryCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
