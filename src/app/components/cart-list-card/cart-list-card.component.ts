import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AddListCardComponent } from 'src/app/dialog/add-list-card/add-list-card.component';
import { CartInfo, OrderFull, ShoppingList } from 'src/app/models/cart';
import { environment } from 'src/environments/environment';
import { CartService } from 'src/app/services/cart.service';
import { OrderService } from 'src/app/services/order.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-cart-list-card',
  templateUrl: './cart-list-card.component.html',
  styleUrls: ['./cart-list-card.component.scss']
})
export class CartListCardComponent implements OnInit {

  imageApi = environment.image_api;
  cart: CartInfo = new CartInfo();
  order: OrderFull = new OrderFull();
  user: any;
  userObservable$ = this.authService.getLoggedUserObservable();
  items: any[] = [];
  @Input() data: any;
  @Input() shoppingList?: ShoppingList;
  @Input() cartId?: number = 0;
  @Output() isDeleted = new EventEmitter<number>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private authService: AuthService,
    private orderService: OrderService,
    private cartService: CartService,
  ) { }

  ngOnInit(): void {
    // console.log(this.shoppingList);

    this.userObservable$.subscribe(data => {
      this.user = data;
    });

    this.cartService.getcartActiveObservable().subscribe(res => {
      this.cart = res
      // console.log("cart, ", this.cart)
      if(this.cart.id){
        this.orderService.getActiveOrderByCart(this.cart.id).subscribe((resp: any) => {
          this.order = resp
          // console.log(this.order)
        })
      }
    });
  }

  editList (): void {
    this.router.navigate(['/mycarts/cart', this.cartId, 'edit-list', this.shoppingList?.id]);
  }

  deleteList(idList: number | undefined): void {
    this.isDeleted.emit(idList);
  }

  async aceptar() {
    // const successOptions = {
    //   title: 'COMPRAR DE NUEVO',
    //   message: 'Se han agregado correctamente los productos al carrito',
    //   btnConfirmar: 'Aceptar',
    //   alertClass: 'success'
    // } as AlertModalInterface;

    const ordenProductos = [] as any;
    const parseProductos = [] as any;
    const productos = [] as any;
    // this.globalService.setCurrentCart(this.cart); // SET CURRENT CARRITO
    console.log(this.data)
    if(this.data){
      this.data.forEach((item: any) => {
        // PARSEANDO cada CARTITEM
        const data = {
          cart_id: this.cart.id,
          product_id: item.product?.id,
          qty: item.quantity,
          choose_item: false,
          price: item.price,
          length: this.items?.length
        };
        productos.push(data);
      });
    }
    console.log("dat: ", productos)
    console.log("ORDEN: ", this.order)
    if (!this.order) {
      // TIPO DE COMPRA / FORMA DE COMPRA
      console.log("CREA UNA ORDEN")

      // const purchaseType = this.globalService.getPurchaseCheckedData();
      // if (!purchaseType || purchaseType.value === 'store') {
      //   this.coreService.presentModalPurchaseType(purchaseType, this.routerOutlet);
      //   return;
      // }


      productos.forEach((r: any) => {
        const orderItem = {
          quantity: r.qty,
          product_id: r.product_id,
          price: r.price,
        };
        ordenProductos.push(orderItem);
      });
      const orden = {
        status: 'ACTIVE',
        purchase_type: 'delivery',
        total_amount: 0,
        owner_id: this.user.id,
        cart_id: this.cart.id,
        // product_group: null,
        store_id: 1, // this.form.value.storeId,
        order_items: ordenProductos,
      };

      console.log(orden)

      this.orderService.addOrder(orden).subscribe(resp => {
        console.log(resp)
      });

    } else {
      productos.forEach((p: any) => { // PARSEA PRODUCTOS PARA ANHADIR TODOS DE ONE
        const orderItem = {
          quantity: p.qty,
          product_id: p.product_id,
          price: p.price
        };
        parseProductos.push(orderItem);
      });

      const orderData = {
        order_items: parseProductos,
      };
      if(this.order.id){
        console.log(orderData)
        this.orderService.addBulkOrder(orderData, this.order.id).subscribe(resp => {
          console.log(resp);
        }, error => {
          console.log(error);
        });
      }
    }

    const cartData = {
      cart_items: productos,
    };
    if(this.cart.id){
    this.cartService.addBulkItems(cartData, this.cart.id).subscribe(async (resp: any) => {
      // this.globalService.setCurrentCart(this.cart); // PARA SETEAR ITEMS EN CARRITO
      // actualizar listado de productos del carrtito y total  para el modal
      if(this.cart.id){
      const wait = await this.getProductCartsItem(this.cart.id);
      }
      // this.loadingService.dismissLoading('addBulkItems');
      // this.alertService.showAlertModal(successOptions);
    });
    }
  }

  getProductCartsItem(cartId: number) {
    this.cartService.getCartItems(cartId).subscribe(resp => {
      // console.log(resp)
      // this.router.navigate(['/products']);
    });
  }

  updateProductToList(productGroup: any) {

  }

}
