import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../material/material.module';
import { OrderTimelineComponent } from './order-timeline.component';



@NgModule({
  declarations: [OrderTimelineComponent],
  exports: [OrderTimelineComponent],
  imports: [
    CommonModule,
    MaterialModule
    // QrPopupModule
  ]
})
export class OrderTimelineModule { }
