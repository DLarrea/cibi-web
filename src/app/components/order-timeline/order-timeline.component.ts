import { Component, Input, OnInit } from '@angular/core';
// import { ModalController } from '@ionic/angular';
import { interval, Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { GlobalService } from 'src/app/services/global.service';


@Component({
  selector: 'app-order-timeline',
  templateUrl: './order-timeline.component.html',
  styleUrls: ['./order-timeline.component.scss'],
})
export class OrderTimelineComponent implements OnInit {
  // subscription: Subscription;
  orderStatusList = [
    'Preparando',
    'Listo',
    'En camino',
    'Entregado'
  ];
  orderStatus = '';
  statusIndex = -1;
  user: any;
  userObservable$ = this.authService.getLoggedUserObservable();
  // userOrderList: Order[] = [];

  @Input() userOrder: any;
  // @Input() boxNumber: string = null;

  constructor(
    // private modalController: ModalController,
    private globalService: GlobalService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    console.log("order: ", this.userOrder)
    this.userObservable$.subscribe(data => {
      this.user = data;
    });
    
    if (this.userOrder.purchaseType === 'pickup'){
      this.orderStatusList = [
        'Preparando',
        'Listo',
        'En Tienda',
        'En Box',
        'Retirado'
      ];
    }

    //Delivery
    if (this.userOrder.purchaseType === 'delivery') {
      if (this.userOrder.status === 'assigned_to_operator' || this.userOrder.status === 'in_progress' || this.userOrder.status === 'in_cash_register'){this.updateStatus(1); }
      else if (this.userOrder.status === 'delivery_pending') {this.updateStatus(2); }
      else if (this.userOrder.status === 'assigned_to_driver' || this.userOrder.status === 'shipped') {this.updateStatus(3); }
      else if (this.userOrder.status === 'delivered') {this.updateStatus(4); }
    }

    // Pickup
    if (this.userOrder.purchaseType === 'pickup') {
      if (this.userOrder.status === 'pending_for_pickup' || this.userOrder.status === 'assigned_to_operator' || this.userOrder.status === 'in_progress' || this.userOrder.status === 'in_cash_register' || this.userOrder.status === 'shipped'){this.updateStatus(1); }
      else if (this.userOrder.status === 'ready_for_pickup') {this.updateStatus(2); }
      else if (this.userOrder.status === 'in_store') {this.updateStatus(3); }
      else if (this.userOrder.status === 'in_box') {this.updateStatus(4); }
      else if (this.userOrder.status === 'delivered') {this.updateStatus(5); }
    // this.updateStatus()
    }
  }

  updateStatus(index: number) {
    // if (this.orderStatus === this.orderStatusList[this.orderStatusList.length - 1]) { this.subscription.unsubscribe(); return; }
    this.statusIndex = this.orderStatusList.indexOf(this.orderStatus) + index;
    const newStatus = this.orderStatusList[this.statusIndex];
    this.orderStatus = newStatus;
  }
}
