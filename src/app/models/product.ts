export class Product {
  id: number = 0;
  name: string = '';
  price: number = 0;
  brand: string = '';
  status: string = '';
  subcategoryId: any = 0;
  image: string = '';
  active: boolean = false;
  barcode: string = '';
  qty: number = 0;
  productGroupId: any = 0;
  details: ProductDetail[] | any[] = [];
  stringDetails: string = '';
  size: string = '';
  checked: boolean = false;
  inCart: boolean = false;
  cartItemId: number = 0;
  showTrash: boolean = true;
  existInCart?: boolean;
  shortName?: string;
  extra?:any;
  off?:string;
  offer_price?:number;
  subcategory_id?:number;
  product_group_id?:number;
  discount?:string;
}
export class ProductDetail {
  descripcion: string = '';
  cantidad: number = 0;
  um: string = '';
}
export class ProductGroup {
  id: number = 0;
  product: Product = new Product();
  name: string = '';
  brand: string = '';
  subcategoryId: number = 0;
  variants: Product[] = [];
  subcategory_id?:any;
}

export class Promotion {
  id: number = 0;
  name: string = '';
  description: string = '';
  startDate: string = '';
  endDate: string = '';
  productId: number = 0;
  productGroupId: number = 0;
  image: string = '';
}

export class PopularProduct {
  product?: Product;
  total?: number;
}

export class ProductSubCategories {
  description?: string;
  id: number = 1;
  displayName?: string;
  disabled?: boolean;
  active?:boolean;
  index?:boolean = false;
  constructor(displayName?: string){
    if(displayName){
      this.displayName = displayName;
      this.disabled = false;
      this.id = 0;
      this.description = displayName;
    }
  }
}

export class Filters {
  brands: string[] = [];
  priceFrom: number = 0;
  priceTo: number = 0;
}