export class Notifications {
    id?: number;
    active: boolean = false;
    classification: string = '';
    checked: boolean = false;
    email: boolean = true;
    sms: boolean = true;
    push: boolean = true;
}
