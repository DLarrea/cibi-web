export class ReportExpenses {
    ownerId: number=0;
    date!: Date;
    month!: string;
    total!: number;
}

export class ReportExpensesCategory {
    ownerId!: number;
    category!: string;
    total!: number;
}

export class Month {
    value!: string;
    viewValue!: string;
}