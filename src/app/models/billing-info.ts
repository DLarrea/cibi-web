export class BillingInfo {
    razonSocial?: string = '';
    ruc?: string = '';
    phone?: string = '';
    address?: string  = '';
    isActive?: boolean = false;
    id?: number = 0;
    userId?: number = 0;
}
