export class Store {
  id?: number | any;
  name: string = '';
  address: string = '';
  active: boolean = false;
  checked: boolean = false;
  type?: string;
  mapImage?: string;
  location?: string;
  businessHours?: string;
  lat_lng?: object;
}
