export class Address {
    id?: number = 0;
    name: string = '';
    address: string = '';
    lat_lng: any;
    isActive?: boolean;
    userId?: number;
    checked?: boolean;
    city?: string;
}
