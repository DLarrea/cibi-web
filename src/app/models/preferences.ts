export class PreferenceCategory {
    id: number = 0;
    name: string = '';
    description: string = '';
    isActive: boolean = false;
    preferenceList: Preference[] = [];
    count: number = 0
}

export class Preference {
    id: number = 0;
    preferenceCategoryId: number = 0;
    name: string = '';
    description: string = '';
    isActive: boolean = false;
    checked: boolean = false;
}

export class UserPreference {
    id: number = 0;
    name: string = '';
    description: string = '';
    userId: number = 0;
    isActive: boolean = false;
    preferenceId: number = 0;
    preferenceCategoryId: number = 0;
    checked: boolean = false;
}

export class UserPreferenceCategory {
    id: number = 0;
    userId: number = 0;
    isActive: boolean = false;
    preferenceCategoryId: number = 0;
}
export class PreferenceUserForNewAccount {
    preferenceCategoryId: number = 0;
    preferenceUserList: Preference[] = [];
}