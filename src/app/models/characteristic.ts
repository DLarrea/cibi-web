export class Characteristic {
    icon: string = '';
    description: string = '';
    id: number = 0;
    value: string = '';
    type: string = '';
}
