import { Product } from './product';

export class MediaResource {
  id!: number;
  url!: string;
  name!: string;
  mediaType!: string;
  active!: boolean;
  image!: string;
  description!: string;
  type!: string;
}

export class MediaItem {
  id!: number;
  productId!: number;
  mediaResourceId!: number;
  time!: number;
  product!: Product;
}
