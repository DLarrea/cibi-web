export class UserFormData {
    name: string = '';
    email: string = '';
    phone: string = '';
    bussinesName: string = '';
    ruc: string = '';
    password: string = '';
    password2: string = '';
}

export class User {
    id: number = 0;
    name: string = '';
    lastName: string = '' ;
    email: string = '';
    address: string = '';
    phoneNumber: string = '';
    bussinesName: string = '';
    ruc: string = '';
    image: string = '';
    cibi_user: boolean = false;
    google_user: boolean = false;
    fb_user: boolean = false;
    apple_user: boolean = false;
    password: boolean = false;
    isActive: boolean = false;
    dateOfBirth: any;
    gender: string = '';
}

export class RecoverPassword {
    email: string = '';
}
