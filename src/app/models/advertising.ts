export class Advertising {
    description: string;
    end_date: string;
    id?: number;
    image: string | null;
    name: string;
    product?:any;
    product_id?:number;
    product_group?:any;
    start_date: string;

    constructor(ad?:any){
        this.description = ad?.description || '';
        this.end_date = ad?.end_date || '';
        this.id = ad?.id || 0;
        this.image = ad?.image || null;
        this.name = ad?.name || '';
        this.product = ad?.product || null;
        this.product_id = ad?.product_id || null;
        this.product_group = ad?.product_group || null;
        this.start_date = ad?.start_date || '';
    }
}
