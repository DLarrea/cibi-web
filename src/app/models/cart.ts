import { BillingInfo } from './billing-info';
import { Product } from './product';
import { Store } from './store';
import { User } from './user';

export class Contact {
  userId: number = 0;
  name: string = '';
  avatar: string = '';
  isOwner: boolean = false;
  status: string = '';
  pendiente: boolean = false;
  role: string = '';
}

export class UserCartInvitationData {
  cartId: number = 0;
  name: string = '';
  fromUser: string = '';
  avatar: string = '';
}

export class ProductToDelete {
  p: Product = new Product();
  toDelete = false;
}

export class ProductListSlide {
  id?: number = 0;
  name: string = '';
  marca: string = '';
  size?: string = '';
  showSlide: boolean = false;
  products: any[] = [];
}

export class ProductList {
  id?: number = 0;
  name: string = '';
  expand: boolean = false;
  products: Product[] = [];
}

export class CartInfo {
  id?: number = 0;
  active?: boolean = false;
  cartItems?: CartItems[] = [];
  color?: string = '';
  name?: string = '';
  ownerId?: number = 0;
  default?: boolean = false;
  showIconLeft?: boolean = false;
  showIconRight?: boolean = false;
  integrantes?: Contact[] = [];
  listas?: any[] = [];
  length?: number = 0;
  inUse?: boolean = false;
}

export class CartItems {
  id: number = 0;
  cartID: number = 0;
  cartItems?: ItemProduct;
  chooseItem?: boolean = false;
  productId: number = 0;
  qty: number = 1;
  checked = false;
}

export class ItemProduct {
  id: number = 0;
  active: boolean = false;
  barcode: string = '';
  brand: string = '';
  image: string = '';
  name: string = '';
  price: number = 0;
  status: string = '';
  subcategoryId: number = 0;
  productGroupId: number = 0;
}

export class ShoppingList {
  id: number = 0;
  name: string = '';
  cartId: number = 0;
  active: boolean = false;
  color?: string;
  shoppingListItems: Item[] = [];
}

export class Item {
  id: number = 0;
  qty: number = 0;
  chooseItem: boolean = false;
  product?: Product;
  checked?: boolean; // use for checkboxes of items
  price: number = 0;
  productId: number = 0;
  quantity: number = 0;
  status: string = '';
  image?: string = '';
  name?: string = '';
}

export class Cart {
  id: number = 0;
  name: string = '';
  color: string = '';
  ownerId: number = 0;
}

export class CreateListModel {
  name = '';
  products: Product[] = [];
}

export class Order {
  active?: boolean = false;
  address?: string = '';
  billingInformationId?: number = 0;
  cartId?: number;
  createdAt?: Date;
  id?: number;
  ownerId?: number;
  purchaseType?: string;
  status?: string;
  storeId?: number;
  totalAmount?: number;
  userPaymentMethodId?: number;
}

export class AddressUser {
  address?: string;
  id?: number;
  isActive?: boolean;
  latLng?: any;
  name?: string;
  userId?: number;
}

export class UserPaymentMethod {
  id?: string;
  isActive?: boolean;
  paymentMethodId?: number;
  paymentMethod?: PaymentMethod;
  userId?: number;
  type?: any;
}

export class PaymentMethod {
  id?: number;
  name?: string;
  description?: string;
  type?: string;
  img?: string;
  checked?: boolean;
}

export class OrderFull extends Order {
  addressUser?: AddressUser;
  billingInfo?: BillingInfo;
  cart?: Cart;
  items?: Item[];
  owner?: User;
  store?: Store;
  userPaymentMethod?: UserPaymentMethod;
}

export class PreferenceCategory {
  id?: number;
  name?: string;
  description?: string;
  isActive?: boolean;
}

export class Preference {
  id?: number;
  preferenceCategoryId?: number;
  name?: string;
  description?: string;
  isActive?: boolean;
  checked?: boolean;
}

export class UserPreference {
  id?: number;
  name?: string;
  description?: string;
  userId?: number;
  isActive?: boolean;
  preferenceId?: number;
  preferenceCategoryId?: number;
  checked?: boolean;
}

export class UserPreferenceCategory {
  id?: number;
  userId?: number;
  isActive?: boolean;
  preferenceCategoryId?: number;
}
export class PreferenceUserForNewAccount {
    preferenceCategoryId?: number;
    preferenceUserList?: Preference[];
}
