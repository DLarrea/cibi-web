import { Component, OnInit } from '@angular/core';
import { PanelService } from "../../services/panel.service";
import { CartService } from "../../services/cart.service";
import { OrderService } from "../../services/order.service";
import { CartInfo, CartItems } from "../../models/cart";
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-my-purchase',
  templateUrl: './my-purchase.component.html',
  styleUrls: ['./my-purchase.component.scss']
})
export class MyPurchaseComponent implements OnInit {

  products: Product[] = []
  cartOrder: any;
  cartInfo: CartInfo | undefined = new CartInfo();
  cart: any = {
    products: []
  };
  totalAmount: number = 0;
  totalProductsQtyLabel: string = '0 Artículos';
  userObservable$ = this.authService.getLoggedUserObservable();
  user = new User();

  constructor(
    private cartService: CartService,
    private authService: AuthService,
    private orderService: OrderService,
    private panelService: PanelService,
    private router: Router,
    ) { }

  ngOnInit(): void {
    this.userObservable$.subscribe(
      (resp: any) =>{
        this.user = resp
      });

    this.cartService.getcartActiveObservable().subscribe(res => {
      this.cartInfo = res
      if(res.id){
        this.orderService.getActiveOrderByCart(res.id).subscribe(resp => {
          this.cartOrder = resp;
        });

        this.cartService.getCartItems(res.id).subscribe((resp: CartItems[]) => {
          this.cart.products = resp;
          this.cart.products.map((item: any) => {
            if (item.cartItems) {
              this.totalAmount += item.qty * item.cartItems.price;
            }
          });
          // etiqueta
          this.totalProductsQtyLabel = this.cart.products.length === 1 ? '1 Artículo' : `${this.cart.products.length} Artículos`;
          console.log(this.cart.products, 'productos');
        })
      }
    });
  }

  closeMyPurchase() {
    this.panelService.close();
  }

  comprar(): void{
    this.panelService.close();
    this.router.navigate(['/mypurchase']);
  }

  onProductPlus(event: any) {
    //console.log(event)
    this.updateCountProduct(event);
  }

  onProductMinus(event: any) {
    // console.log(event)
    if (event.qty >= 1) {
      this.updateCountProduct(event);
    }
  }

  updateCountProduct(product: any) {
    // this.productGroupSelectedInSlide.product.qty = this.currentProductQty;
    // actualizar el producto en el carrito
    this.updateQuantityProductInCart(product.id, product.qty);
    // actualizar el producto en la orden
    if(this.cartInfo?.id){
      // console.log("Entro")
      this.updateQuantityProductInOrder(this.cartInfo.id, product);
    }
  }


  // actualizar el producto en el carrito
  updateQuantityProductInCart(itemId: number, qty: number) {
    this.cartService.updateQuantityProduct(itemId, qty)
      .subscribe(resp => {
        console.log(resp);
        // this.getProductCartsItem(this.cartInfo.id);
      }, error => {
        console.log(error);
      });
  }

  // actualizar el producto en el carrito
  updateQuantityProductInOrder(cartId: number, product: any) {
    const data = {
      quantity: product.qty,
      product_id: product.productId
    }
    this.orderService.updateQuantityProduct(cartId, data)
      .subscribe(resp => {
        // console.log(resp);
      }, error => {
        // console.log(error);
      });
  }

  removeFromCart(x: any): void {
    console.log("Se quita del carrito", x.p, x.toDelete);
    const productsId = [x.p.productId];
    if(this.cartInfo?.id){
      // eliminar del carrito
      this.cartService.deleteItemCart(x.p.id).subscribe(resp => {
        console.log(resp);
        this.cartService.setCartActive$(this.user.id);

        // this.getProductCartsItem(this.cartInfo.id);
      }, error => {
        console.log(error);
      });

      //eliminar de la orden
      this.orderService.deleteItemsCart(this.cartInfo?.id, productsId).subscribe(resp => {
        console.log(resp);
      }, error => {
        console.log(error);
      });
    }
  }
}
