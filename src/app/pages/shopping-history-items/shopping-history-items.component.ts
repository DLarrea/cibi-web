import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderFull } from 'src/app/models/cart';
import { OrderService } from 'src/app/services/order.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-shopping-history-items',
  templateUrl: './shopping-history-items.component.html',
  styleUrls: ['./shopping-history-items.component.scss']
})
export class ShoppingHistoryItemsComponent implements OnInit {
  imageApi = environment.image_api;
  order: any;

  qty = 0;
  items: any[] = [];

  pType = [
    {
      type: 'delivery',
      icon: 'home-outline-orange'
    },
    {
      type: 'pickup',
      icon: 'car-sport-outline-orange',
    }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private orderService: OrderService
  ) { }

  ngOnInit(): void {
    const orderId = this.route.snapshot.params.id;
    this.getOrderById(orderId);
  }

  getOrderById(id: number): void {
    this.orderService.getOrderById(id).subscribe(
      resp => {
        this.order = resp;
        this.qty = this.order.items?.length;
        this.items = this.order.items;
        console.log(this.items);
      }
    );
  }

  back(): void {
    this.router.navigate(['/shopping-history']);
  }
}
