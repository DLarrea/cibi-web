import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingHistoryItemsComponent } from './shopping-history-items.component';

describe('ShoppingHistoryItemsComponent', () => {
  let component: ShoppingHistoryItemsComponent;
  let fixture: ComponentFixture<ShoppingHistoryItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShoppingHistoryItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingHistoryItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
