import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingHistoryItemsRoutingModule } from './shopping-history-items-routing.module';
import { ShoppingHistoryItemsComponent } from './shopping-history-items.component';
import { MaterialModule } from 'src/app/material/material.module';
import { ComponentsModule } from 'src/app/components/components.module';


@NgModule({
  declarations: [
    ShoppingHistoryItemsComponent
  ],
  imports: [
    CommonModule,
    ShoppingHistoryItemsRoutingModule,
    MaterialModule,
    ComponentsModule
  ]
})
export class ShoppingHistoryItemsModule { }
