import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShoppingHistoryItemsComponent } from './shopping-history-items.component';

const routes: Routes = [
  {
    path: '',
    component: ShoppingHistoryItemsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShoppingHistoryItemsRoutingModule { }
