import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { LoginComponent } from 'src/app/dialog/login/login.component';
import { Snackbar } from 'src/app/interfaces/snackbar';
import { AddressService } from 'src/app/services/address.service';
import { GlobalService } from 'src/app/services/global.service';
import { LoadingService } from 'src/app/services/loading.service';
import { NotificationService } from 'src/app/services/notification.service';
import { PreferenceService } from 'src/app/services/preference.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { StoreService } from 'src/app/services/store.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  isLoading$ = this.loadingService.getLoadingObservable();
  hide = true;
  registerForm = this.formBuilder.group({
    name: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    phone: [null, [Validators.required, Validators.pattern(/^([0-9]+)$/)]],
    bussinesName: [null, Validators.required],
    ruc: [null, Validators.required],
    password: [null, Validators.required],
    password2: [null, Validators.required],
  });

  get name(): FormControl {
    return this.registerForm.get('name') as FormControl;
  }

  get email(): FormControl {
    return this.registerForm.get('email') as FormControl;
  }

  get phone(): FormControl {
    return this.registerForm.get('phone') as FormControl;
  }

  get bussinesName(): FormControl {
    return this.registerForm.get('bussinesName') as FormControl;
  }

  get ruc(): FormControl {
    return this.registerForm.get('ruc') as FormControl;
  }

  get password(): FormControl {
    return this.registerForm.get('password') as FormControl;
  }

  get password2(): FormControl {
    return this.registerForm.get('password2') as FormControl;
  }

  constructor(
    private router: Router,
    public dialog: MatDialog,
    public formBuilder: FormBuilder,
    private addressService: AddressService,
    private globalService: GlobalService,
    private loadingService: LoadingService,
    private notificationService: NotificationService,
    private preferenceService: PreferenceService,
    private storeService: StoreService,
    private userService: UserService,
    private snackBarService: SnackbarService
  ) { }

  ngOnInit(): void {
    const userFormDataStored = this.globalService.getUserFormData();
    this.registerForm.setValue(userFormDataStored); 
  }

  saveUserFormData() {
    this.globalService.setUserFormData(this.registerForm.value);
  }

  register(): void {
    if (!this.validateForm()) return;

    const addressList = this.globalService.getAddressListForNewAccountData();
    const notificationList = this.globalService.getNotificationForNewAccountData();
    const preferenceUserList = this.globalService.getPreferenceUserForNewAccountData();
    const nearbyStoreList = this.globalService.getNearbyStoreForNewAccountData();

    const formDataUserOptions = {
      addressList,
      notificationList,
      preferenceUserList,
      nearbyStoreList
    }

    this.userService.register(this.registerForm, formDataUserOptions, 'cibi').subscribe(
      resp => {
        console.log("Se esta loading", this.isLoading$);
        console.log('registerForm', this.registerForm)
        console.log('resp', resp);
        this.snackBarService.openSnackBar({
          message: `Registro exitoso!`,
          position: 'top',
          className: 'snack-bar-class',
          classAction: 'success',
          duration: 6000,
        }as Snackbar) ;
        this.router.navigateByUrl('/');
        const dialog = this.dialog
        .open(LoginComponent, {
          disableClose: false,
          data: {
            email: this.registerForm.controls['email'].value,
            firstTime: true
          }
        });
        localStorage.clear();
      }, error => {
        //FALTA EL SNACKBAR y SPINNER ACA Y ARRIBA
        console.log('error', error.error.detail);
        this.snackBarService.openSnackBar({
          message: error.error.detail,
          position: 'top',
          className: 'snack-bar-class',
          classAction: 'error',
          duration: 200000,
        }as Snackbar) ;
      }
    );
  }

  validateForm() {
    if (!this.registerForm.valid) console.log('El formulario presenta campos inválidos');
    if (this.password.value !== this.password2.value) {
      console.log('Error', 'Las contraseñas no coinciden');
      this.snackBarService.openSnackBar({
        message: 'Las contraseñas no coinciden',
        position: 'top',
        className: 'snack-bar-class',
        classAction: 'error',
        duration: 200000,
      }as Snackbar) ;
      return false;
    }
    return true;
  }  

}
