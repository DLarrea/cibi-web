import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { url } from 'src/app/environments/url';
import { RegisterComponent } from './register.component';

const routes: Routes = [
  {
    path: '',
    component: RegisterComponent,
  },
  {
    path: url.register.notifications,
    loadChildren: () =>
      import('src/app/pages/register-notifications/register-notifications.module').then((m) => m.RegisterNotificationsModule),
  },
  {
    path: url.register.preferences,
    loadChildren: () =>
      import('src/app/pages/register-preferences/register-preferences.module').then((m) => m.RegisterPreferencesModule),
  },
  {
    path: url.register.listStoresNearby,
    loadChildren: () =>
      import('src/app/pages/register-list-stores-nearby/register-list-stores-nearby.module').then((m) => m.RegisterListStoresNearbyModule),
  },
  {
    path: url.register.address,
    loadChildren: () =>
      import('src/app/pages/register-address/register-address.module').then((m) => m.RegisterAddressModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
