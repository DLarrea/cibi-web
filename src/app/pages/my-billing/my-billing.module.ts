import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from "../../material/material.module";
import { AgmCoreModule } from "@agm/core";
import { MyBillingRoutingModule } from './my-billing-routing.module';
import { MyBillingComponent } from './my-billing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MyBillingComponent
  ],
  imports: [
    CommonModule,
    MyBillingRoutingModule,
    MaterialModule,
    AgmCoreModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MyBillingModule { }
