import { Component, OnInit } from '@angular/core';
import { BillingInfo } from 'src/app/models/billing-info';
import { BillingService } from 'src/app/services/billing.service';
import { Location } from "@angular/common";
import { Subscription } from "rxjs";
import { ConfirmActionService } from 'src/app/dialog/confirm-action/confirm-action.service';
import { AuthService } from 'src/app/services/auth.service';
import { BillingFormComponent } from 'src/app/dialog/billing-form/billing-form.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-my-billing',
  templateUrl: './my-billing.component.html',
  styleUrls: ['./my-billing.component.scss'],
  providers: [BillingInfo]
})
export class MyBillingComponent implements OnInit {
  loading = true;
  loadingSubscription?: Subscription;
  user: any
  messageNotinfobilling = 'La lista de Facturaciones esta vacía';
  billingList: BillingInfo[]=[];
  userObservable$ = this.authService.
  getLoggedUserObservable();
  
  constructor(private billingService: BillingService,
              private billingI: BillingInfo,
              private lct: Location,
              public dialog: MatDialog,
              private authService: AuthService,
              private confirmActionService: ConfirmActionService) { }

  ngOnInit(): void {
    this.userObservable$.subscribe(data => {
      this.user = data;
      // console.log(data)
    });
    this.loadBilling()
  }
  loadBilling() {
    //TODO: Manejo de mensajes de error
    this.billingService.getList(this.user.id).subscribe(data => {
      this. billingList = data;
    });
  }
  back() {
    this.lct.back();
  }

  addBillingDialog(): void {
    const dialog = this.dialog
      .open(BillingFormComponent, {
        height: '780px',
        width: '930px',
        disableClose: false,
        data: {},
      });

    dialog.afterClosed().subscribe(data => {
      if (data) {
        // console.log(data);
        this.billingService.create(this.user.id, data).subscribe(response => {
          // console.log('se creo el billing', response);
          this.loadBilling();
        })
      }
    });
  }

  handleClickDelete(id: number | undefined) {
    if (id) {
      const options = {
        title: 'Eliminar facturación',
        message: '¿Desea eliminar lo seleccionado?',
        cancelText: 'Cancelar',
        confirmText: 'Sí'
      };
      this.confirmActionService.open(options);

      this.confirmActionService.confirmed().subscribe(confirmed => {
        if (confirmed) {
          this.deleteBilling(id);
        }
      });
    }
  }

  deleteBilling(id: number) {
    this.billingService.delete(this.user.id, id).subscribe(response => {
      this.loadBilling();
    })
  }

  editBillingDialog(billing: BillingInfo): void {
    if (billing && billing.id) {
      const dialog = this.dialog
        .open(BillingFormComponent, {
          height: '780px',
          width: '930px',
          disableClose: false,
          data: {
            billing : billing,
            title: 'Editar Facturación'},
        });

      dialog.afterClosed().subscribe(data => {
        if (data && billing.id) {
          this.billingService.edit(this.user.id, billing.id, data).subscribe(response => {
            this.loadBilling();
          });
        }
      });
    }



  }
}
