import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyBillingComponent } from './my-billing.component';

const routes: Routes = [
  {
    path: '',
    component: MyBillingComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyBillingRoutingModule { }
