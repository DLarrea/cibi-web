import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyBillingComponent } from './my-billing.component';

describe('MyBillingComponent', () => {
  let component: MyBillingComponent;
  let fixture: ComponentFixture<MyBillingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyBillingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
