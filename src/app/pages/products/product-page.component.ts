import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { Snackbar } from 'src/app/interfaces/snackbar';
import { CartInfo, CartItems, OrderFull, ProductList } from 'src/app/models/cart';
import { Product, ProductSubCategories } from 'src/app/models/product';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { OrderService } from 'src/app/services/order.service';
import { ProductService } from 'src/app/services/product.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {

  items: MenuItem[] = [
    {
      label: 'A-Z',
      command: () => {}
    },
    {
      label: 'Z-A',
      command: () => {}
    },
    {
      label: 'Precio menor a mayor',
      command: () => {}
    },
    {
      label: 'Precio mayor a menor',
      command: () => {}
    },
  ];
  displayFilter = false;
  userObservable$ = this.authService.getLoggedUserObservable();
  user: any;
  subCategorias: ProductSubCategories[] = [new ProductSubCategories('Inicio')];
  productosPopulares: Product[] = [];
  productosOfertas: Product[] = [];
  productosPreferidos: Product[] = [];
  productos: Product[] = [];
  breakPoints = [
    {
      breakpoint: '1500px',
      numVisible: 4,
      numScroll: 4
    },
    {
      breakpoint: '1200px',
      numVisible: 3,
      numScroll: 3
    },
    {
      breakpoint: '768px',
      numVisible: 2,
      numScroll: 2
    },
    {
      breakpoint: '576px',
      numVisible: 1,
      numScroll: 1
    }
  ];
  subCategory: number | null = null;
  subCategoryName: string = '';
  popularLimit = 100;
  popularSkip = 0;
  productLimit = 8;
  productSkip = 0;
  cartItems: CartItems[] = [];
  cartInfo: CartInfo | null = null;
  cartIsSet: boolean = false;
  brands: any[] = [];
  itemsSubCategorias: MenuItem[] = [];



  productList: Product[] = [];
  

  selectedOptions: boolean[] = [];
  cartOrder: OrderFull | null = new OrderFull();

  limit = 10;
  skip = 0;


  productListPromo: Product[] = [];
  productAllList: Product[] = [];
  productLikesBySubcategory: Product[] = [];
  productPopularBySubcategory: Product[] = [];
  productRelatedBySubcategory: Product[] = [];
  productPromoBySubcategory: Product[] = [];
  productAllBySubcategory: Product[] = [];
  sortNum: number = 0;
  searchValue = "";
  

  constructor(
    private authService: AuthService,
    private productService: ProductService,
    private cartService: CartService,
    private snackBarService: SnackbarService,
    private orderService: OrderService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params => {
      if(params['subcategory'] && params['subcategory'] != 0){
        this.subCategory = Number(params['subcategory']);
      }
    });
  }

  ngOnInit(): void {

    this.userObservable$.subscribe(resp => {
      this.user = resp;
      this.user && this.getPreferidos();
    });

    this.getSubCategorias();
    this.getOfertas();
    this.getPopulares();
    this.getBrands();
    !this.subCategory && this.getProductos();

    this.cartService.getcartActiveObservable().subscribe(res => {
      if(Object.keys(res).length !== 0){
        this.cartInfo = {...res};
      }
     
      if (!this.cartIsSet && this.cartInfo && this.cartInfo.id) {
        this.cartIsSet = true;
        this.getOrder(this.cartInfo.id);
        this.getCartItems(this.cartInfo.id);
      } 
      
      if (!this.cartIsSet && Object.keys(res).length === 0) {
        this.getCarts();
      }
    })

  }

  getSubCategorias = () => {
    this.productService.getSubCategories().subscribe({
      next:(result:any) => {
        let subsMobile: MenuItem[] = [];
        this.subCategorias = this.subCategorias.concat(result);
        this.subCategory ? 
          this.subCategorias.forEach(el => {
            if(el.id  == this.subCategory){
              el.index = true;
              this.subCategoryName = el.displayName ? el.displayName : '';
            }
          }) : this.subCategorias[0].index = true;
        
        this.subCategorias.forEach(el => {
          subsMobile.push({label: el.displayName})
        });
        this.itemsSubCategorias = subsMobile;
      },
      error:(err:HttpErrorResponse) => {
        console.log(err.error);
      }
    })
  }

  getOfertas = () => {
    this.productService.getProductsOffers().subscribe({
      next:(result:any) => {
        this.productosOfertas = result.map((el:any) => {
          el.extra = el.price?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          return el;
        });
        
        let total = Number(4 - this.productosOfertas.length);
        let i = 0;
        while(total > 0){
          this.productosOfertas.push({...this.productosOfertas[i]});
          total--;
          i++;
        }
      },
      error:(err:HttpErrorResponse) => {
        console.log(err.error);
      }
    })
  }
  
  getPopulares = () => {
    this.productService.getPopularProducts(this.popularLimit, this.popularSkip).subscribe({
      next:(result:any) => {
        let products:Product[] = [];
        for(let el of result){
          if(el.total){
            el.Product.extra = el.Product.price?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            products.push(el.Product);
          }
        }
        this.productosPopulares = products;
      },
      error:(err:HttpErrorResponse) => {
        console.log(err.error);
      }
    })
  }

  getProductos = () => {
    this.productService.getProductsPaginated(this.productLimit, this.productSkip).subscribe({
      next:(result:any) => {
        result.forEach((el:any) => {
          el.extra = el.price?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          this.productos.push({...el});
        });
      },
      error:(err:HttpErrorResponse) => {
        console.log(err.error);
      }
    })
  }

  getPreferidos = () => {
    this.productService.getProductsByUserPreference(this.user.id).subscribe({
      next:(result:any) => {
        this.productosPreferidos = result;
        if(this.productosPreferidos.length > 0){
          let total = Number(4 - this.productosPreferidos.length);
          let i = 0;
          while(total > 0){
            this.productosPreferidos.push({...this.productosPreferidos[i]});
            total--;
            i++;
          }
        }
      },
      error:(err:HttpErrorResponse) => {
        console.log(err.error);
      }
    })
  }

  getCartItems = (id:number) => {
    this.cartService.getCartItems(id).subscribe({
      next:(result:any) => {
        this.cartItems = result;
      },
      error:(err:HttpErrorResponse) => {
        console.log(err.error);
      }
    })
  }

  getOrder(cartId: number) {
    this.orderService.getActiveOrderByCart(cartId).subscribe({
      next:(result:any) => {
        this.cartOrder = result;
      },
      error:(err:HttpErrorResponse) => {
        console.log(err.error);
      }
    })
  }

  getCarts() {
    this.cartService.getCartsByUserID(this.user.id).subscribe({
      next:(result:any) => {
        result[0].id && this.cartService.setCartActive$(result[0].id);
      },
      error:(err:HttpErrorResponse) => {
        console.log(err.error);
      }
    });
  }

  getBrands(): void {
    this.productService.getBrandsAll().subscribe({
      next:(result:any) => {
        this.brands = result;
      },
      error:(err:HttpErrorResponse) => {
        console.log(err.error);
      }
    })
  }

  changeSubCategory = (sub:ProductSubCategories) => {
    if(sub.id != 0){
      this.subCategory = sub.id;
      this.subCategoryName = sub.displayName ? sub.displayName : '';
      this.subCategorias.forEach(el => el.index = el.id == this.subCategory)
      let qp: Params = { subcategory: sub.id };
      this.router.navigate(
        [], 
        {
          relativeTo: this.route,
          queryParams: qp,
          queryParamsHandling: 'merge'
        }
      );
    } else {
      this.subCategory = null;
      this.subCategoryName = '';
      this.subCategorias.forEach(el => el.index = el.id == 0)
      this.router.navigate(
        [], 
        {
          relativeTo: this.route,
          queryParams: null,
          queryParamsHandling: 'merge'
        }
      );
    }
  }

  paginateProducts = () => {
    this.productSkip += this.productLimit + 1;
    this.getProductos();
  }



  getAllDataSubcategory = (cartId:number, subCategory: number) => {

    const getSubCategories$ = this.productService.getSubCategories();
    const getAllPopularProducts$ = this.productService.getPopularProducts(this.popularLimit, this.popularSkip);
    const cartItems$ = this.cartService.getCartItems(cartId);
    const getProduct$ = this.productService.getProductsPaginated(this.productLimit, this.productSkip, subCategory);

    forkJoin([getSubCategories$, getAllPopularProducts$, cartItems$, getProduct$]).subscribe(
      results => {
        // if(this.subCategorias.length <= 1){
        //   this.subCategorias = [this.itemInicio].concat(results[0]);
        // }
        // this.subCategorias.forEach((el, i) => {el.active = i == 0});
        // this.productosPopulares = results[1].map(el => {
        //   el.Product.extra = el.Product.price?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        //   return el.Product;
        // });
        this.cartItems = results[2];
        this.productosPreferidos = results[3];
      },
      errors => {
        console.log(errors);
      }
    );
  }


  productInCart(list: any[]){
    if(list){
      let lisProdIds: number[] = [];
      this.cartItems.forEach(element => {
        // console.log("product Id: ", element.productId)
        lisProdIds.push(element.productId);
        const index = list.findIndex(p => p.id === element.productId);
        if (index > -1) {
          list[index].inCart = true;
          list[index].cartItemId = element.id;
          list[index].qty = element.qty;
        }
      });
    }
  }

  getData(cartId: number): void {
    const getSubCategories$ = this.productService.getSubCategories();
    const getAllPopularProducts$ = this.productService.getAllPopularProducts('');
    const getProducts$ = this.productService.getProducts();
    const cartItems$ = this.cartService.getCartItems(cartId);


    forkJoin([getSubCategories$, getAllPopularProducts$, getProducts$, cartItems$]).subscribe(
      results => {
        // this.subCategorias = [this.itemInicio].concat(results[0]);
        this.subCategorias.forEach((el, i) => {
          el.active = i == 0;
        });
        
        results[1].forEach(
          resp => {
            // console.log(resp)
            if(resp.total){
              this.productosPopulares.push(resp.product);
            }
          }
        );
        this.productList = results[2];
        //console.log(results);
        this.cartItems = results[3]
        this.productInCart(this.productosPopulares)
      },
      errors => {
        console.log(errors[0]);
        console.log(errors[1]);
        console.log(errors[2]);
      }
    );
  }


  getSearchData(id?: number): void {
    if (!!this.searchValue.length && this.searchValue.length >= 3) {
      this.productPopularBySubcategory = [];
      const getAllPopularProducts$ = this.productService.getAllPopularProducts(this.searchValue);
      const getProductsBySubCategory$ = this.productService.getProductsBySubCategory(id, this.searchValue, 0, 100);
      const getPopularProductsBySubCategory$ = this.productService.getPopularProductsBySubCategory(id, this.searchValue);

      forkJoin([getAllPopularProducts$, getProductsBySubCategory$, getPopularProductsBySubCategory$]).subscribe(
        results => {
          this.productosPopulares = results[0];
          this.productList = results[0];
          this.productAllBySubcategory = results[1];
          results[2].forEach(
            resp => {
              this.productPopularBySubcategory.push(resp.product);
            }
          );
        },
        errors => {
          console.log(errors[0]);
        }
      );
    } else if (!this.searchValue.length) {
      // console.log("Se volvio!!");
      //this.getData();
    }
  }

  getProductsBySubCategory(id: number): void {
    this.productAllBySubcategory = [];
    this.productPopularBySubcategory = [];
    this.skip = 0;
    const getSubCategories$ = this.productService.getSubCategories();
    const getProductsBySubCategory$ = this.productService.getProductsBySubCategory(id, '', 0, 5);
    const getPopularProductsBySubCategory$ = this.productService.getPopularProductsBySubCategory(id, '');

    forkJoin([getSubCategories$, getProductsBySubCategory$, getPopularProductsBySubCategory$]).subscribe(
      results => {
        // this.subCategorias = [this.itemInicio].concat(results[0]);
        this.subCategorias.forEach((el, i) => {
          el.active = i == 0;
        });
        this.subCategorias.forEach(el => el.active =  el.id == id);
        this.productAllBySubcategory = results[1];
        results[2].forEach(
          resp => {
            if(resp.total){
              this.productPopularBySubcategory.push(resp.product);
            }
          }
        );
        this.skip += 5;
        // console.log(this.productAllBySubcategory)
        // console.log(this.productPopularBySubcategory)
        this.productInCart(this.productAllBySubcategory)
        this.productInCart(this.productPopularBySubcategory)
      },
      errors => {
        console.log(errors[1]);
      }
    );
    

  }

  getMoreProducts(id: number): void {
    this.productService.getMoreProducts(id, this.skip, this.limit).subscribe(
      resp => {
        // console.log(resp);
        this.productAllBySubcategory.push.apply(this.productAllBySubcategory, resp);
        // console.log(this.productAllBySubcategory);
        this.skip += this.limit;
      },
      error => {
        console.log('Error de ver mas:', error);
      }
    );
  }

  filter(data: any): void {
    this.productService.filterBrandPrice(data).subscribe(
      resp => {
        if (resp.length === 0) {
          this.snackBarService.openSnackBar({
            message: `No se ha podido encontrar productos con estas opciones ${resp}`,
            position: 'top',
            className: 'snack-bar-class',
            classAction: 'success',
          } as Snackbar);
        } else {
          // console.log("EL array que trae:", resp);
          this.productList = resp;
        }
      },
      error => {
        this.snackBarService.openSnackBar({
          message: `${error}`,
          position: 'top',
          className: 'snack-bar-class',
          classAction: 'success',
        } as Snackbar);
      }
    )
  }

  changeMenuSeleccion(index: number, item: any): void {
    this.selectedOptions = new Array(this.subCategorias.length).fill(false);
    this.selectedOptions[index] = true;
    this.getProductsBySubCategory(item.id);
  }

  addToCart(product: Product): void {
    if (this.cartInfo && this.cartInfo.id) {
      // console.log(product)
      const item = {
        cart_id: this.cartInfo?.id,
        product_id: product.id,
        qty: product.qty,
        choose_item: false,
      };

      if (!this.cartOrder) {
        const purchaseType = 'delivery'
        const orderItem = {
          // quantity: this.currentProductQty === 0 ? 1 : this.currentProductQty,
          quantity: 1,
          product_id: product.id,
          price: product.price,
        };
        const order = {
          status: 'ACTIVE',
          purchase_type: purchaseType,
          total_amount: 0,
          owner_id: this.cartInfo?.ownerId,
          cart_id: this.cartInfo?.id,
          store_id: 1, // this.form.value.storeId,
          order_items: [orderItem]
        };
        this.orderService.addOrder(order).subscribe(resp => {
          this.cartOrder = resp;
          // console.log(resp)
        });
      }
      else if (this.cartOrder && this.cartOrder.id) {
        // existe la orden
        const orderItem = {
          // quantity: this.currentProductQty === 0 ? 1 : this.currentProductQty,
          quantity: 1,
          product_id: product.id,
          price: product.price
        };
        this.orderService.addItem(this.cartOrder.id, orderItem).subscribe(resp => {
          // console.log(this.cartOrder);
          // this.cartOrder.items.push(resp);
        });
      }

      const dataItem = {
        cart_items: [item],
      };


      this.cartService.addBulkItems(dataItem, this.cartInfo.id).subscribe((resp: any) => {
        // console.log(resp);
        this.cartService.setCartActive$(this.user.id)
      }, error => {
        console.log(error)
      });
    }
  }

  onProductPlus(event: Product) {
    // console.log(event)
    this.updateCountProduct(event);
  }
  onProductMinus(event: Product) {
    // console.log(event)
    if (event.qty >= 1) {
      this.updateCountProduct(event);
    }
  }

  updateCountProduct(product: Product) {
    // this.productGroupSelectedInSlide.product.qty = this.currentProductQty;
    // actualizar el producto en el carrito
    this.updateQuantityProductInCart(product.cartItemId, product.qty);
    // actualizar el producto en la orden
    if(this.cartInfo?.id){
      // console.log("Entro")
      this.updateQuantityProductInOrder(this.cartInfo.id, product);
    }
  }


  // actualizar el producto en el carrito
  updateQuantityProductInCart(itemId: number, qty: number) {
    this.cartService.updateQuantityProduct(itemId, qty)
      .subscribe(resp => {
        console.log(resp);
        // this.getProductCartsItem(this.cartInfo.id);
      }, error => {
        console.log(error);
      });
  }

  // actualizar el producto en el carrito
  updateQuantityProductInOrder(cartId: number, product: Product) {
    const data = {
      quantity: product.qty, 
      product_id: product.id
    }
    this.orderService.updateQuantityProduct(cartId, data)
      .subscribe(resp => {
        // console.log(resp);
      }, error => {
        // console.log(error);
      });
  }

  removeFromCart(x: any): void {
    console.log("Se quita del carrito", x.p, x.toDelete);
    const productsId = [x.p.id];
    if(this.cartInfo?.id){
      // eliminar del carrito
      this.cartService.deleteItemCart(x.p.cartItemId).subscribe(resp => {
        console.log(resp);
        this.cartService.setCartActive$(this.user.id);

        // this.getProductCartsItem(this.cartInfo.id);
      }, error => {
        console.log(error);
      });

      //eliminar de la orden
      this.orderService.deleteItemsCart(this.cartInfo?.id, productsId).subscribe(resp => {
        console.log(resp);
      }, error => {
        console.log(error);
      });
    }
  }
}
