import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductPageComponent } from './product-page.component';
import { ProductsRoutingModule } from './products-routing.module';

//primeng
import {CardModule} from 'primeng/card';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TieredMenuModule} from 'primeng/tieredmenu';
import {SidebarModule} from 'primeng/sidebar';
import {CarouselModule} from 'primeng/carousel';
import {TabMenuModule} from 'primeng/tabmenu';
import {ScrollTopModule} from 'primeng/scrolltop';

@NgModule({
  declarations: [
    ProductPageComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ProductsRoutingModule,
    SharedModule,
    FormsModule,
    CardModule,
    InputTextModule,
    ButtonModule,
    TieredMenuModule,
    SidebarModule,
    TabMenuModule,
    ScrollTopModule,
    CarouselModule
  ]
})
export class ProductsModule { }
