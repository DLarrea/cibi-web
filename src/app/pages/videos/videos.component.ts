import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MediaItem, MediaResource } from 'src/app/models/media';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {

  imageApi = environment.image_api;
  selectedOptions: boolean[] = [];
  user: any;
  
  brands: any[] = [];
  sortNum: number = 0;

  
  videoList: MediaResource []=[];
  VideosPopulares: MediaResource []=[];
  VideosRecomendados: MediaResource []=[];
  mediaItem: MediaItem []=[];

  iconLeft = 'chevron-back-outline';
  showHeaderWithLogo = true;
  
  videoUrl = 'http://20.102.121.158:99/ejgj0o.mp4/raw';
  videoPlay = false;


  constructor(private router: Router) { }

  ngOnInit() {
  }

  playPauseBtnVideo(tagId: string) {
    const myVideo: any = document.getElementById(tagId);
    if (myVideo.paused) {
      myVideo.play();
      myVideo.controls = true;
      this.videoPlay = true;
    }
  }

  pauseVideo(tagId: string) {
    const myVideo: any = document.getElementById(tagId);
    myVideo.controls = false;
    this.videoPlay = false;
  }

  playVideo(tagId: string) {
    const myVideo: any = document.getElementById(tagId);
    myVideo.controls = true;
    this.videoPlay = true;
  }

  goToVertodos() {
    return this.router.navigate(['menu/gallery/detail']);
  }

}
