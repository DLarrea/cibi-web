import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { CartInfo } from 'src/app/models/cart';
import { CartService } from 'src/app/services/cart.service';
import { Location } from '@angular/common';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { ConfirmActionService } from 'src/app/dialog/confirm-action/confirm-action.service';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-edit-cart',
  templateUrl: './edit-cart.component.html',
  styleUrls: ['./edit-cart.component.scss']
})
export class EditCartComponent implements OnInit {

  imageApi = environment.image_api;

  headText = "";
  isToCreate = false;
  user: any;
  cart: CartInfo = new CartInfo();
  cartColor = '#c4540b';

  form = this.formBuilder.group({
    name: ['', Validators.required]
  });

  get name(): FormControl {
    return this.form.get('name') as FormControl;
  }

  userObservable$ = this.authService.getLoggedUserObservable();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cartService: CartService,
    private _location: Location,
    public formBuilder: FormBuilder,
    private confirmActionService: ConfirmActionService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    const cartID = this.route.snapshot.params.id;
    this.userObservable$.subscribe(
      resp => {
        this.user = resp;
      }
    );

    if(cartID){
      this.headText = "Editar Carrito"
      this.getCartInfo(cartID);
    }else{
      this.headText = "Crear Carrito"
      this.isToCreate = true;
      console.log("se esta creando el carrito");
    }
  }

  getCartInfo(cartId: number) {
    const cart$ = this.cartService.getCart(cartId);
    const integrantes$ = this.cartService.getUserToCarts(cartId);

    forkJoin([cart$, integrantes$]).subscribe(results => {
      this.cart = results[0];
      this.cart.integrantes = results[1];
      this.form.setValue({
        name: results[0].name
      });
    });
  }

  handleClickDelete(id: number, name: string) {
    if (id) {
      const options = {
        title: 'Eliminar usuario',
        message: `¿Desea eliminar a ${name} del carrito?`,
        cancelText: 'Cancelar',
        confirmText: 'Sí'
      };
      this.confirmActionService.open(options);

      this.confirmActionService.confirmed().subscribe(confirmed => {
        if (confirmed) {
          this.deleteUser(id);
        }
      });
    }
  }

  deleteUser(id: number): void {
    const cartId = this.cart.id as number;
    this.cartService.deleteIntegrante(id, cartId).subscribe(resp => {
      console.log(resp);
      this.getCartInfo(cartId);
    });
  }

  save(): void {
    const name = this.form.value.name;
    const object = {
      name: name,
      color: this.cartColor
    }
    this.cartService.editCart(object, this.cart.id as number).subscribe(resp => {
      console.log(resp);
      // actualizar los datos del carrito en memoria
      this.cart.name = resp.name;
      this.cart.color = resp.color;
      this.router.navigate(['/mycarts/cart/', this.cart.id]);
      // alert de exito
    }, error => {
      console.log(error);
      // alert de error
    });
  }

  createCart(): void {
    const name = this.form.value.name;
    const object = {
      name: name,
      color: this.cartColor,
      owner_id: this.user.id,
      default: false
    }

    this.cartService.addcart(object).subscribe(resp => {
      console.log(resp);
      this.router.navigate(['/mycarts']);
    }, error => {
      console.log(error);
    });
  }

  cancel(): void {
    this._location.back();
  }
}
