import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditCartComponent } from './edit-cart.component';

const routes: Routes = [
  {
    path: '',
    component: EditCartComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditCartRoutingModule { }
