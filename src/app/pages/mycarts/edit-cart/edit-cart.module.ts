import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditCartRoutingModule } from './edit-cart-routing.module';
import { EditCartComponent } from './edit-cart.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    EditCartComponent
  ],
  imports: [
    CommonModule,
    EditCartRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EditCartModule { }
