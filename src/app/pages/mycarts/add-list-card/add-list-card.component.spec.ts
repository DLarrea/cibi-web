import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddListCardComponent } from './add-list-card.component';

describe('AddListCardComponent', () => {
  let component: AddListCardComponent;
  let fixture: ComponentFixture<AddListCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddListCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddListCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
