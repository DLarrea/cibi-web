import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from 'src/app/components/components.module';
import { MaterialModule } from 'src/app/material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SingleProductCardGroupModule } from 'src/app/shared/single-product-card-group/single-product-card-group.module';
import { AddListCardRoutingModule } from './add-list-card-routing.module';
import { AddListCardComponent } from './add-list-card.component';



@NgModule({
  declarations: [
    AddListCardComponent
  ],
  imports: [
    CommonModule,
    AddListCardRoutingModule,
    MaterialModule,
    ComponentsModule,
    ReactiveFormsModule,
    SharedModule,
    SingleProductCardGroupModule
  ]
})
export class AddListCardModule { }
