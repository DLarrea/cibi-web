import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddListCardComponent } from './add-list-card.component';

const routes: Routes = [
  {
    path: '',
    component: AddListCardComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddListCardRoutingModule { }
