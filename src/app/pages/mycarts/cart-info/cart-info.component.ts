import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { ConfirmActionService } from 'src/app/dialog/confirm-action/confirm-action.service';
import { CartInfo, OrderFull } from 'src/app/models/cart';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { ListService } from 'src/app/services/list.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart-info.component.html',
  styleUrls: ['./cart-info.component.scss']
})
export class CartInfoComponent implements OnInit {

  user: any;
  cart: CartInfo = new CartInfo();
  shoppingHistoryList: OrderFull[] = [];
  order: any;

  userObservable$ = this.authService.getLoggedUserObservable();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private cartService: CartService,
    private cartInfoService: CartService,
    private listService: ListService,
    private orderService: OrderService,
    public dialog: MatDialog,
    private confirmActionService: ConfirmActionService
  ) { }

  ngOnInit(): void {
    this.cart.integrantes = [];
    this.userObservable$.subscribe(
      resp => {
        this.user = resp;
      }
    )
    const cartID = this.route.snapshot.params.id;
    this.getCartInfo(cartID);
  }

  getCartInfo(cartId: number) {
    const cart$ = this.cartInfoService.getCart(cartId);
    const integrantes$ = this.cartInfoService.getUserToCarts(cartId);
    const cartList$ = this.listService.getByCartId(cartId);
    const shoppintHistoryList$ = this.orderService.getOrdersHistoryByCart(this.user.id, 'delivered', cartId);
    const cartItems$ = this.cartInfoService.getCartItems(cartId);
    const orderActive$ = this.orderService.getActiveOrderByCart(cartId);

    forkJoin([cart$, integrantes$, cartList$, shoppintHistoryList$, cartItems$, orderActive$]).subscribe(results => {
      this.cart = results[0];
      this.cart.integrantes = results[1];
      this.cart.listas = results[2];
      this.shoppingHistoryList = results[3];
      this.cart.cartItems = results[4];
      this.order = results[5];

      // console.log("En CART INFO Order", this.order);
    });
  }

  goToAddLisCart(): void {
    // url para agregar listas de carrito
    const url = `/mycarts/cart/${this.cart.id}/add-list`;
    this.router.navigateByUrl(url);
  }

  handleClickDelete(id: number | undefined) {
    if (id) {
      const options = {
        title: 'Eliminar lista',
        message: '¿Desea eliminar la lista seleccionada?',
        cancelText: 'Cancelar',
        confirmText: 'Sí'
      };
      this.confirmActionService.open(options);

      this.confirmActionService.confirmed().subscribe(confirmed => {
        if (confirmed) {
          this.deleteListCart(id);
        }
      });
    }
  }

  deleteListCart(idList: number): void {
    this.listService.removeList(idList).subscribe(
      resp => {
        this.ngOnInit();
      });
  }

  editCart(): void {
    this.router.navigate(['/mycarts/cart', this.cart.id, 'edit-cart']);
  }

  deleteCart(): void {
    const options = {
      title: 'Eliminar carrito',
      message: `¿Desea eliminar el carrito ${this.cart.name}?`,
      cancelText: 'Cancelar',
      confirmText: 'Sí'
    };
    this.confirmActionService.open(options);

    this.confirmActionService.confirmed().subscribe(confirmed => {
      if (confirmed) {
        this.confirmedDeletCart();
      }
    });
  }

  confirmedDeletCart(): void {
    this.cartInfoService.deleteCart(this.cart.id as number).subscribe(resp => {
      // console.log(resp);
      this.router.navigate(['/mycarts']);
    }, error => {
      console.log(error);
    });
  }

  back(): void {
    this.router.navigate(['/mycarts']);
  }
}
