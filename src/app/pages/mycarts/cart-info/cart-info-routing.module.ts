import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { url } from 'src/app/environments/url';
import { CartInfoComponent } from './cart-info.component';

const routes: Routes = [
  {
    path: '',
    component: CartInfoComponent,
  },
  {
    path: 'edit-list/:id',
    loadChildren: () =>
      import('src/app/pages/mycarts/edit-list-cart/edit-list-cart.module').then((m) => m.EditListCartModule),
  },
  {
    path: 'edit-cart',
    loadChildren: () =>
      import('src/app/pages/mycarts/edit-cart/edit-cart.module').then((m) => m.EditCartModule),
  },

  {
    path: 'add-list',
    loadChildren: () =>
      import('src/app/pages/mycarts/add-list-card/add-list-card.module').then((m) => m.AddListCardModule),
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartInfoRoutingModule { }
