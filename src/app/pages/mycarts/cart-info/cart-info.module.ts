import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from 'src/app/components/components.module';
import { MaterialModule } from 'src/app/material/material.module';
import { CartInfoRoutingModule } from './cart-info-routing.module';
import { CartInfoComponent } from './cart-info.component';



@NgModule({
  declarations: [
    CartInfoComponent
  ],
  imports: [
    CommonModule,
    CartInfoRoutingModule,
    MaterialModule,
    ComponentsModule
  ]
})
export class CartInfoModule { }
