import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditListCartComponent } from './edit-list-cart.component';

const routes: Routes = [
  {
    path: '',
    component: EditListCartComponent,
  },
  {
    path: 'agregar',
    loadChildren: () =>
      import('src/app/pages/mycarts/add-list-card/add-list-card.module').then((m) => m.AddListCardModule),
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditListCartRoutingModule { }
