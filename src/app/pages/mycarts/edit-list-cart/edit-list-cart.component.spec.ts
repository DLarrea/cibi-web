import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditListCartComponent } from './edit-list-cart.component';

describe('EditListCartComponent', () => {
  let component: EditListCartComponent;
  let fixture: ComponentFixture<EditListCartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditListCartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditListCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
