import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditListCartRoutingModule } from './edit-list-cart-routing.module';
import { EditListCartComponent } from './edit-list-cart.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    EditListCartComponent
  ],
  imports: [
    CommonModule,
    EditListCartRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EditListCartModule { }
