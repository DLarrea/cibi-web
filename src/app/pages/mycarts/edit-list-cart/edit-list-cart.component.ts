import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AddListCardComponent } from 'src/app/dialog/add-list-card/add-list-card.component';
import { ShoppingList } from 'src/app/models/cart';
import { ListService } from 'src/app/services/list.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-list-cart',
  templateUrl: './edit-list-cart.component.html',
  styleUrls: ['./edit-list-cart.component.scss']
})
export class EditListCartComponent implements OnInit {

  imageApi = environment.image_api;
  cartId: number = 0;
  listId: number = 0;
  nameList: string = '';
  list: any[] = [];

  showTrash = true;
  value = 1;
  i = 1;

  form = this.formBuilder.group({
    listName: ['', Validators.required]
  });

  get name(): FormControl {
    return this.form.get('listName') as FormControl;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    public formBuilder: FormBuilder,
    private listService: ListService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.listId = this.route.snapshot.params.id;
    this.getItemsList(this.listId);
  }

  getItemsList(id: number): void {
    this.listService.getListById(id).subscribe(
      (resp: any) => {
        this.nameList = resp.name,
        this.list = resp.shopping_list_items;
        this.cartId = resp.cart_id;
        this.form.setValue({
          listName: resp.name
        });
      }
    );
  }

  deleteItems(): void {
    let itemsToDelete: any = [];
    this.list.filter(p => p.checked).map(
      r => itemsToDelete.push(r)
    );
    this.listService.removeItemsFromList(this.listId, itemsToDelete).subscribe(
      resp => {
        this.getItemsList(this.listId);
      }
    );
  }

  addProductsToList(): void {
    console.log(this.list)
    // const dialog = this.dialog
    //   .open(AddListCardComponent, {
    //     height: '780px',
    //     width: '930px',
    //     disableClose: false,
    //     data: {
    //       listInfo: this.list,
    //       isToCreate: false,
    //       listId: this.listId
    //     },
    //   });
    this.listService.setShoppingList$(this.list);
    this.router.navigate(['/mycarts/cart', this.cartId, 'edit-list', this.listId, 'agregar']);

    // dialog.afterClosed().subscribe(data => {
    //   this.getItemsList(this.listId);
    // });
  }

  save(): void {
    const name = this.form.value.listName;
    const object = {
      name: name,
      active: true
    }
    this.listService.editList(this.listId, object).subscribe(res => {
      this.router.navigate(['/mycarts/cart/', this.cartId]);
    });
  }


  handleMinus(item: any) {
    if (item.qty > 0) {
      item.qty--
      if (item.qty === 1) {
        this.showTrash = true;
      }
      this.listService.updateQuantityList(item.id, item.qty).subscribe(res=>{
        console.log(res);
      })
    }
  }

  handlePlus(item: any) {
    if (item.qty < 10) {
      this.showTrash = false;
      item.qty++;
      this.listService.updateQuantityList(item.id, item.qty).subscribe(res=>{
        console.log(res);
      })
    }
    console.log(this.list);
  }

  handleDelete(item: any) {
    item.existInCart = false;
    this.listService.removerItem(item.id).subscribe(res => {
      console.log(res)
      this.getItemsList(this.listId);
    })
  }


  cancel(): void {
    this._location.back();
  }
}
