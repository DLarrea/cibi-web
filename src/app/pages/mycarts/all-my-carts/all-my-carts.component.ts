import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
// import { ShoppingListsComponent } from 'src/app/dialog/shopping-lists/shopping-lists.component';
import { CartInfo } from 'src/app/models/cart';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-all-my-carts',
  templateUrl: './all-my-carts.component.html',
  styleUrls: ['./all-my-carts.component.scss']
})
export class AllMyCartsComponent implements OnInit {
  myCarts: CartInfo[] = [];
  user: any;
  cartId: number = 0;

  userObservable$ = this.authService.getLoggedUserObservable();

  constructor(
    private router: Router,
    private authService: AuthService,
    private cartService: CartService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.myCarts = [];
    this.userObservable$.subscribe(
      resp =>{
        this.user = resp;
      }
    );
    this.getLoadingUserCarts();
  }

  getLoadingUserCarts() {
    this.cartService.getCartsByUserID(this.user.id).subscribe(resp => {
      this.myCarts = resp;
    });
  }

  async onSelectionCartButton(cart: CartInfo): Promise<void> {
    console.log('El carrito seleccionado: ', cart);
    /* const dialog = this.dialog
      .open(ShoppingListsComponent, {
        height: '780px',
        width: '930px',
        disableClose: false,
        data: {
          cart: cart
        },
      });

    dialog.afterClosed().subscribe(data => {
      if (data) {
        // this.getCartInfo(this.cart.id as number);
        this.router.navigate(['/products']);
      }
    }); */

  }

  onSelectionCartLogo(cart: CartInfo): void {
    console.log('click en el logo del carrito: ', cart);
    this.router.navigate(['/mycarts/cart', cart.id]);
  }

  sharedCart(cart: CartInfo) {
    console.log('se presiono boton de compartir', cart);
  }

  createCart(): void {
    this.router.navigate(['/mycarts/cart/create']);
  }

}
