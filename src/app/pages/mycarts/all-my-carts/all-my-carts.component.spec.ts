import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMyCartsComponent } from './all-my-carts.component';

describe('AllMyCartsComponent', () => {
  let component: AllMyCartsComponent;
  let fixture: ComponentFixture<AllMyCartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllMyCartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMyCartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
