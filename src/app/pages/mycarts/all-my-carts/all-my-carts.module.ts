import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllMyCartsRoutingModule } from './all-my-carts-routing.module';
import { AllMyCartsComponent } from './all-my-carts.component';
import { MaterialModule } from 'src/app/material/material.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { DialogModule } from 'src/app/dialog/dialog.module';


@NgModule({
  declarations: [
    AllMyCartsComponent
  ],
  exports: [
    AllMyCartsComponent
  ],
  imports: [
    CommonModule,
    AllMyCartsRoutingModule,
    MaterialModule,
    ComponentsModule,
    // DialogModule
  ]
})
export class AllMyCartsModule { }
