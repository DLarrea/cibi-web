import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { url } from 'src/app/environments/url';
import { AllMyCartsComponent } from './all-my-carts.component';

const routes: Routes = [
  {
    path: '',
    component: AllMyCartsComponent,
  },
  {
    path: url.mycarts.cart + '/create',
    loadChildren: () =>
      import('src/app/pages/mycarts/edit-cart/edit-cart.module').then((m) => m.EditCartModule),
  },
  {
    path: url.mycarts.cart + '/:id',
    loadChildren: () =>
      import('src/app/pages/mycarts/cart-info/cart-info.module').then((m) => m.CartInfoModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllMyCartsRoutingModule { }
