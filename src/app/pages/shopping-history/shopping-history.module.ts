import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingHistoryRoutingModule } from './shopping-history-routing.module';
import { ShoppingHistoryComponent } from './shopping-history.component';
import { MaterialModule } from 'src/app/material/material.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    ShoppingHistoryComponent
  ],
  imports: [
    CommonModule,
    ShoppingHistoryRoutingModule,
    MaterialModule,
    ComponentsModule,
    SharedModule
  ]
})
export class ShoppingHistoryModule { }
