import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShoppingHistoryComponent } from './shopping-history.component';

const routes: Routes = [
  {
    path: '',
    component: ShoppingHistoryComponent,
  },
  {
    path: ':id',
    loadChildren: () =>
      import('src/app/pages/shopping-history-items/shopping-history-items.module').then((m) => m.ShoppingHistoryItemsModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShoppingHistoryRoutingModule { }
