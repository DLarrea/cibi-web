import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderHistoryFilter } from 'src/app/interfaces/orderHistoryFilter';
import { OrderFull } from 'src/app/models/cart';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { OrderFilterService } from 'src/app/services/order-filter.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-shopping-history',
  templateUrl: './shopping-history.component.html',
  styleUrls: ['./shopping-history.component.scss']
})
export class ShoppingHistoryComponent implements OnInit {

  user: any;
  shoppingHistoryList: OrderFull[] = [];
  shoppingHistoryListOriginal: OrderFull[] = [];

  userObservable$ = this.authService.getLoggedUserObservable();
  filter: OrderHistoryFilter = {}

  constructor(
    private authService: AuthService,
    private orderService: OrderService,
    private router: Router,
    private orderFilterService: OrderFilterService,


  ) { }

  ngOnInit(): void {
    this.userObservable$.subscribe(
      resp =>{
        this.user = resp;
      }
    );
    this.getShoppingHistory(this.user);
    this.getFilteredList();
  }

  getShoppingHistory(user: User): void {
    console.log("user id", user.id);
    this.orderService.getOrdersHistory(user.id, 'delivered').subscribe((resp) => {
      this.shoppingHistoryList = resp;
      this.shoppingHistoryListOriginal = resp;
      this.orderFilterService.setList(resp)

      },
      error => {
        console.log("Error al traer el historial", error);
      }
    );
  }

  onSelectionViewItems(id: number): void {
    this.router.navigate(['/shopping-history', id]);
  }

  getFilteredList(){
    this.orderFilterService.getFilteredList().subscribe(list => {
      this.shoppingHistoryList = list
    })
  }

  filterOrders(data: OrderHistoryFilter){
    console.log(data)
    if(data){
      this.orderFilterService.setFilter(data)
    }
  }

  clearFilter(): void {
    this.shoppingHistoryList = this.shoppingHistoryListOriginal;
  }
}
