import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';
import { SwiperModule } from 'swiper/angular';
import { MaterialModule } from 'src/app/material/material.module';

import { OrderTimelineModule } from '../../components/order-timeline/order-timeline.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Primeng
import {CarouselModule} from 'primeng/carousel';
import {ButtonModule} from 'primeng/button';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {AvatarModule} from 'primeng/avatar';
import {TooltipModule} from 'primeng/tooltip';

@NgModule({
  declarations: [
    LandingComponent
  ],
  exports: [
    LandingComponent
  ],
  imports: [
    CommonModule,
    CarouselModule,
    OverlayPanelModule,
    ButtonModule,
    AvatarModule,
    LandingRoutingModule,
    MaterialModule,
    SwiperModule,
    OrderTimelineModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule
  ]
})
export class LandingModule { }
