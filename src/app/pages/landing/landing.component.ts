import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BREAK_POINTS } from 'src/app/constant';
import { Advertising } from 'src/app/models/advertising';
import { Product, ProductSubCategories } from 'src/app/models/product';
import { AdvertisingService } from 'src/app/services/advertising.service';
import { AuthService } from 'src/app/services/auth.service';
import { CallCenterService } from 'src/app/services/call-center.service';
import { MediaService } from 'src/app/services/media.service';
import { OrderService } from 'src/app/services/order.service';
import { ProductService } from 'src/app/services/product.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit{

  callCenters: any[] = [];
  whatsapp: any = null;
  llamadas: any = null;
  advertisings: Advertising[] = [];
  offers: Product[] = [];
  videos: any[] = [];
  user: any;
  userObservable$ = this.authService.getLoggedUserObservable();
  userOrderList: any = [];
  imageApi = environment.image_api;
  productSubCategories: ProductSubCategories[] = [];
  
  advertisingsBreakPoints = [
    {
      breakpoint: BREAK_POINTS.lg,
      numVisible: 2,
      numScroll: 1
    },
    {
      breakpoint: BREAK_POINTS.md,
      numVisible: 2,
      numScroll: 1
    },
    {
      breakpoint: BREAK_POINTS.sm,
      numVisible: 1,
      numScroll: 1
    }
  ];
  
  offersBreakPoints = [
    {
      breakpoint: BREAK_POINTS.lg,
      numVisible: 3,
      numScroll: 1
    },
    {
      breakpoint: BREAK_POINTS.md,
      numVisible: 2,
      numScroll: 1
    },
    {
      breakpoint: BREAK_POINTS.sm,
      numVisible: 1,
      numScroll: 1
    }
  ];
  
  videosBreakPoints = [
    {
      breakpoint: BREAK_POINTS.lg,
      numVisible: 4,
      numScroll: 1
    },
    {
      breakpoint: BREAK_POINTS.md,
      numVisible: 2,
      numScroll: 1
    },
    {
      breakpoint: BREAK_POINTS.sm,
      numVisible: 1,
      numScroll: 1
    }
  ];
  constructor(
    private authService: AuthService,
    private orderService: OrderService,
    private productService: ProductService,
    private advertisingService: AdvertisingService,
    private mediaService: MediaService,
    private callCenterService: CallCenterService,
    private router: Router

  ) { }
  ngOnInit(){
    this.userObservable$.subscribe(data => {
      this.user = data;
    });
    if(this.user){
      this.getOrders(this.user.id);
    }
    this.getAdvertisings();
    this.getOffers();
    this.getMedia();
    this.getCallCenter();
    this.getSubCategories();

    // Poner en una funcion
    this.productService.getKeyWords().subscribe(
      data => {
        console.log(data);
      }
    )
  }

  getOrders(id: any) {
    this.orderService.getOrdersByOwnerId(id).subscribe(resp => {
      this.userOrderList = resp.filter(o => o.status !== 'created' && o.status !== 'cancelled' && o.status !== 'closed');
    }, error => {
      console.log(error);
    });
  }

  getAdvertisings() {
    this.advertisingService.getList().subscribe(
      data => {
        this.advertisings = data;
        let total = Number(3 - this.advertisings.length);
        while(total > 0){
          this.advertisings.push({...this.advertisings[0]});
          total--;
        }
      },
      error => {
        this.advertisings = [];
      }
    )
  }
  
  getOffers() {
    this.productService.getProductsOffers().subscribe(
      data => {
        //this.offers = data;
        this.offers = data;
        let total = Number(4 - this.offers.length);
        while(total > 0){
          this.offers.push({...this.offers[0]});
          total--;
        }
        this.offers.forEach(el => {
          el.extra = el.offer_price?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        });
        console.log(this.offers.length);
      },
      error => {
        this.offers = [];
      }
    )
  }
  
  getMedia() {
    this.mediaService.getVideos().subscribe(
      data => {
        this.videos = data;
        let total = Number(5 - this.videos.length);
        while(total > 0){
          this.videos.push({...this.videos[0]});
          total--;
        }
      },
      error => {
        this.videos = [];
      }
    )
  }

  getCallCenter() {
    this.callCenterService.getList().subscribe(
      data => {
        this.callCenters = data;
        this.whatsapp = this.callCenters.find(el => {
          return el.type == "WhatsApp";
        });
        this.llamadas = this.callCenters.find(el => {
          return el.type == "Llamadas";
        });
        console.log(data);
      },
      error => {
        this.callCenters = [];
      }
    )
  }

  getSubCategories(){
    this.productService.getSubCategories().subscribe(
      data => {
        this.productSubCategories = data;
      },
      error => {
        this.productSubCategories = [];
      }
    )
  }

  goToSubCategory(name:string){
    let subCategoryId = null;
    for(let sc of this.productSubCategories){
      if(sc.description == name){
        subCategoryId = sc.id;
        break;
      }
    }
    subCategoryId != null && this.router.navigate(['/products'], {queryParams: {subcategory:subCategoryId}});
  }
}
