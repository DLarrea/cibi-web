import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterAddressRoutingModule } from './register-address-routing.module';
import { RegisterAddressComponent } from './register-address.component';
import { MaterialModule } from 'src/app/material/material.module';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [
    RegisterAddressComponent
  ],
  imports: [
    CommonModule,
    RegisterAddressRoutingModule,
    MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCNYuJWWvEBf_VTFxzQkhfF1xVKhURdD6M'
    }),
  ]
})
export class RegisterAddressModule { }
