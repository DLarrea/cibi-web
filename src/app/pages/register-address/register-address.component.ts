import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AddressFormComponent } from 'src/app/dialog/address-form/address-form.component';
import { Address } from 'src/app/models/address';
import { AddressService } from 'src/app/services/address.service';
import { GlobalService } from 'src/app/services/global.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-register-address',
  templateUrl: './register-address.component.html',
  styleUrls: ['./register-address.component.scss'],
})
export class RegisterAddressComponent implements OnInit {
  loading = true;
  loadingSubscription?: Subscription;
  addressList: Address[] = [];
  messageNotAddressList = 'La lista de direcciones esta vacia';

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private addressService: AddressService,
    private globalService: GlobalService,
    private loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    this.listenLoading();
    this.precargarAddressListAdded();
  }

  listenLoading(): void {
    this.loadingSubscription = this.loadingService
      .getLoadingObservable()
      .subscribe({
        next: (loading) => {
          setTimeout(() => {
            this.loading = loading;
          }, 0);
        },
      });
  }

  addAddressDialog(): void {
    const dialog = this.dialog
      .open(AddressFormComponent, {
        height: '780px',
        width: '930px',
        disableClose: false,
        data: {},
      });

    dialog.afterClosed().subscribe(data => {
      if (data) {
        const address = new Address();
        address.name = data.name;
        address.address = data.address;
        address.lat_lng = data.lat_lng;
        this.addressList.push(address);
        console.log(data);
      }
    });
  }

  precargarAddressListAdded() {
    const addedAddressList = this.globalService.getAddressListForNewAccountData();
    if (addedAddressList.length > 0) {
      console.log('Tenemos direcciones pre-cargadas');
      this.addressList = this.globalService.getAddressListForNewAccountData();
    }
  }

  back() {
    if (this.router.url.startsWith('/register/address')) {
      this.globalService.setAddresListForNewAccount(this.addressList);
      console.log(this.addressList);
      this.router.navigate(['/register']);
    }
  }
}
