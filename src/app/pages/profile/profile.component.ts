import { MenuItem } from './../../interfaces/menu-item';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Snackbar } from 'src/app/interfaces/snackbar';
import { GlobalService } from 'src/app/services/global.service';
import { AuthService } from 'src/app/services/auth.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { UserService } from 'src/app/services/user.service';
import { UserPhotoService } from 'src/app/services/user-photo.service';
import { environment } from 'src/environments/environment';
import { CartInfo } from 'src/app/models/cart';
import { MatMenuTrigger } from '@angular/material/menu';
import { EditPhotoProfileComponent } from 'src/app/dialog/edit-photo-profile/edit-photo-profile.component';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { getMonth } from 'date-fns';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
  imageApi = environment.image_api;
  imageFile: File = new File([], '');

  user: any;
  userObservable$ = this.authService.getLoggedUserObservable();
  cart: CartInfo = new CartInfo();
  generos: string[] = ['Hombre', 'Mujer', 'Sin Especificar'];
  meses: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  hide = true;
  form = this.formBuilder.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required, Validators.pattern(/^([0-9]+)$/)]],
    ruc: [''],
    // bussinesName: [''],
    image: [''],
    fecha: [''],
    genero: ['']
  });



  get name(): FormControl {
    return this.form.get('name') as FormControl;
  }

  get email(): FormControl {
    return this.form.get('email') as FormControl;
  }

  get phone(): FormControl {
    return this.form.get('phone') as FormControl;
  }

  // get image(): FormControl {
  //   return this.form.get('image') as FormControl;
  // }

  // get bussinesName(): FormControl {
  //   return this.form.get('bussinesName') as FormControl;
  // }

  // get ruc(): FormControl {
  // return this.form.get('ruc') as FormControl;
  // }

  constructor(
    public formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router,
    private authService: AuthService,
    private globalService: GlobalService,
    private userService: UserService,
    private snackBarService: SnackbarService,
    private UserPhotoService: UserPhotoService
  ) { }

  ngOnInit(): void {

    this.getUserData()

  }

  getUserData() {
    this.userService.me().subscribe(resp => {
      // console.log("resp: ", resp);
      this.user = resp;
      console.log(this.user);
      this.form.setValue({
        name: `${resp.name}`,
        email: resp.email,
        ruc: `${resp.ruc}`,
        phone: resp.phoneNumber,
        // bussinesName: resp.bussinesName,
        image: resp.image,
        fecha: resp.dateOfBirth,
        genero: resp.gender
      });

    }, error => {
      if (error.status === 401) {
        this.globalService.clearStorage().then(() => {
          this.router.navigate(['/login'], { replaceUrl: true });
          return;
        });
      }
    });
  }

  openDialog() {
    const dialogRef = this.dialog
    .open(EditPhotoProfileComponent, {
      restoreFocus: false,
      height: '800x',
      width: '550px',
      disableClose: false,
      data: {}, });

    // Manually restore focus to the menu trigger since the element that
    // opens the dialog won't be in the DOM any more when the dialog closes.
    dialogRef.afterClosed().subscribe((result) => {
      this.menuTrigger.focus();
      if (result){
        this.UserPhotoService.ImgUser(this.user.id, result).subscribe((resp: any) => {
          console.log(resp)
          this.getUserData()
        })

      }
    });
  }

  //Para seleccionar la fecha
  addEvent(event: MatDatepickerInputEvent<Date>) {
    console.log(`${event.value}`);
    let fecha = `${event.value}`;
    var fechaLis = fecha.split(" ", 4);
    fecha = '';
    fecha = fechaLis[3] + '-' + this.getMes(fechaLis[1]) + '-' + fechaLis[2];
    this.form.value.fecha = fecha;
  }

  getMes(mes: string){
    return String(this.meses.indexOf(mes) + 1);
  }

  onSubmit() {
    console.log("pq no mostras?", this.form.value);
    const formData = {
      email: this.form.value.email,
      name: this.form.value.name,
      last_name: '',
      phone_number: this.form.value.phone,
      ruc: '',
      gender: this.form.value.genero,
      date_of_birth: this.form.value.fecha
    };
    this.snackBarService.openSnackBar({
      message: 'Datos de perfil actualizados',
      position: 'top',
      className: 'snack-bar-class',
      classAction: 'success',
      success: false,
      duration: 5000
    } as Snackbar);
    // editar usuario
    this.userService.edit(this.user.id, formData).subscribe(resp => {
      this.globalService.setUser(resp);
      // this.router.navigate(['/']);

    }, error => {
      console.log(error, error.status);
    });
  };


  list: MenuItem[] = [
    {
      displayName: 'Cambiar contraseña',
      route: '/change-password',
      disabled: false,
      extra: null
    },
    {
      displayName: 'Datos de facturacion',
      route: '/mybilling',
      disabled: false,
      extra: null
    },
    {
      displayName: 'Métodos de pago',
      route: '',
      disabled: false,
      extra: null
    },
    {
      displayName: 'Mis direcciones',
      route: '/myaddress',
      disabled: false,
      extra: null
    },
    {
      displayName: 'Notificaciones',
      route: '',
      disabled: false,
      extra: null
    },

  ]
}
