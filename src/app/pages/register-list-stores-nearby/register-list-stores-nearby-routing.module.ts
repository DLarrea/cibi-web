import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterListStoresNearbyComponent } from './register-list-stores-nearby.component';

const routes: Routes = [
  {
    path: '',
    component: RegisterListStoresNearbyComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterListStoresNearbyRoutingModule { }
