import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { StoresNearbyMapsComponent } from './../../dialog/stores-nearby-maps/stores-nearby-maps.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from 'src/app/models/store';
import { GlobalService } from 'src/app/services/global.service';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-register-list-stores-nearby',
  templateUrl: './register-list-stores-nearby.component.html',
  styleUrls: ['./register-list-stores-nearby.component.scss']
})
export class RegisterListStoresNearbyComponent implements OnInit {
  storesList: Store[] = [];

  constructor(
    private router: Router,
    private storeService: StoreService,
    private globalService: GlobalService,
    public dialog: MatDialog,
    public formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getStoresList();
  }

  storesForm = this.formBuilder.group({
    stores: []
  })

  getStoresList() {
    this.storeService.getList().subscribe(resp => {
      for(let store of resp){
        this.storesList.push(store);
      }
      this.precargarStoreListSelected();
      console.log("todas las tienas: ", this.storesList);
    });
  }

  precargarStoreListSelected() {
    const checkedList = this.globalService.getNearbyStoreForNewAccountData();
    if (checkedList.length > 0) {
      console.log('Tenemos tiendas pre-seleccionadas');
      checkedList.forEach(store => {
        const storeObjIdx = this.storesList.findIndex(s => s.id === store.id);
        if (storeObjIdx > -1) {
          this.storesList[storeObjIdx].checked = true;
        }
      });
    }
  }

  back() {
    const storeCheckedList = this.storesList.filter(store => store.checked === true);

    this.globalService.setNearbyStoreForNewAccount(storeCheckedList);
    this.router.navigate(['']);
  }

  selectStores() {
    const storeListSelected = this.storesList.filter(s => s.checked === true);
    console.log(storeListSelected);
    if(storeListSelected.length > 0) {
      this.globalService.setNearbyStoreForNewAccount(this.storesList);
      this.back();
    }
  }

  openMap(id: number){
    var config: any;
    this.storesList.forEach(store => {
      if(store.id == id){
        config = store.lat_lng;
      }
    });
    let link = "https://maps.google.com/?q=" + config.lat + "," + config.lng;
    window.open(link);
  }

  submit(){
    this.selectStores();
  }
}
