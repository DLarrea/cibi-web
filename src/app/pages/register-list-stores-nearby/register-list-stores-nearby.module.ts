import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ButtonModule} from 'primeng/button';
import { RegisterListStoresNearbyRoutingModule } from './register-list-stores-nearby-routing.module';
import { RegisterListStoresNearbyComponent } from './register-list-stores-nearby.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    RegisterListStoresNearbyComponent
  ],
  imports: [
    CommonModule,
    RegisterListStoresNearbyRoutingModule,
    MaterialModule,
    FormsModule,
    ButtonModule
  ]
})
export class RegisterListStoresNearbyModule { }
