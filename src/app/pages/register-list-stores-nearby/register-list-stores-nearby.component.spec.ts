import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterListStoresNearbyComponent } from './register-list-stores-nearby.component';

describe('RegisterListStoresNearbyComponent', () => {
  let component: RegisterListStoresNearbyComponent;
  let fixture: ComponentFixture<RegisterListStoresNearbyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterListStoresNearbyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterListStoresNearbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
