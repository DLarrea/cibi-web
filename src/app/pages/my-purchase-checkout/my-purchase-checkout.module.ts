import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyPurchaseCheckoutRoutingModule } from './my-purchase-checkout-routing.module';
import { SwiperModule } from 'swiper/angular';
import { MaterialModule } from 'src/app/material/material.module';

// import Swiper core and required modules
import SwiperCore, { Autoplay, Keyboard, Pagination, Navigation } from "swiper";
import { SharedModule } from "../../shared/shared.module";
import { MyPurchaseResumeComponent } from "./resume/my-purchase-resume.component";
import { MyPurchaseCheckoutComponent } from "./checkout/my-purchase-checkout.component";
import {FormsModule} from "@angular/forms";

// install Swiper modules
SwiperCore.use([Autoplay, Keyboard, Pagination, Navigation]);

@NgModule({
  declarations: [
    MyPurchaseResumeComponent,
    MyPurchaseCheckoutComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MyPurchaseCheckoutRoutingModule,
    MaterialModule,
    SwiperModule,
    SharedModule,
  ]
})
export class MyPurchaseCheckoutModule { }
