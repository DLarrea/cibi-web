import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyPurchaseCheckoutComponent } from "./checkout/my-purchase-checkout.component";
import { MyPurchaseResumeComponent } from "./resume/my-purchase-resume.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'resume',
  },
  {
    path: 'resume',
    component: MyPurchaseResumeComponent,
  },
  {
    path: 'checkout',
    component: MyPurchaseCheckoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPurchaseCheckoutRoutingModule { }
