import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPurchaseResumeComponent } from './my-purchase-resume.component';

describe('MyPurchaseBuyComponent', () => {
  let component: MyPurchaseResumeComponent;
  let fixture: ComponentFixture<MyPurchaseResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyPurchaseResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPurchaseResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
