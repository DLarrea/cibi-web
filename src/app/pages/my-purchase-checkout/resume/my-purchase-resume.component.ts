import { Component, OnInit } from '@angular/core';
import { CartService } from "../../../services/cart.service";
import {OrderService} from "../../../services/order.service";
import {CartInfo, CartItems} from "../../../models/cart";
import {FormGroup} from "@angular/forms";
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-purchase-checkout',
  templateUrl: './my-purchase-resume.component.html',
  styleUrls: ['./my-purchase-resume.component.scss']
})
export class MyPurchaseResumeComponent implements OnInit {

  products: any = []
  cartOrder: any;
  cartInfo: CartInfo | undefined = new CartInfo();
  cart: any = {
    products: []
  };
  credit: number = 0;
  discountAmount = 0;
  deliveryAmount = 0;
  totalAmount: number = 0;
  totalProductsQtyLabel: string = '0 Artículos';
  userObservable$ = this.authService.getLoggedUserObservable();

  user = new User();

  constructor(private cartService: CartService, private orderService: OrderService, private authService: AuthService,
    private router: Router,
    ) {
  }

  ngOnInit(): void {
    this.userObservable$.subscribe(
      (resp: any) =>{
        this.user = resp
      });

    this.cartService.getcartActiveObservable().subscribe(res => {
      this.cartInfo = res;
      if(res.id){
        this.orderService.getActiveOrderByCart(res.id).subscribe(resp => {
          this.cartOrder = resp;
        });

        this.cartService.getCartItems(res.id).subscribe((resp: CartItems[]) => {
          this.cart.products = resp;
          this.totalAmount = 0;
          this.cart.products.map((item: any) => {
            console.log(item);
            if (item.cartItems) {
              this.totalAmount += item.qty * item.cartItems.price;
            }
          });
          // etiqueta
          this.totalProductsQtyLabel = this.cart.products.length === 1 ? '1 Artículo' : `${this.cart.products.length} Artículos`;
          console.log(this.cart.products, 'productos');
        })
      }
    })
  }

  onProductPlus(event: any) {
    //console.log(event)
    this.updateCountProduct(event);
  }

  onProductMinus(event: any) {
    // console.log(event)
    if (event.qty >= 1) {
      this.updateCountProduct(event);
    }
  }

  updateCountProduct(product: any) {
    // this.productGroupSelectedInSlide.product.qty = this.currentProductQty;
    // actualizar el producto en el carrito
    this.updateQuantityProductInCart(product.id, product.qty);
    // actualizar el producto en la orden
    if(this.cartInfo?.id){
      // console.log("Entro")
      this.updateQuantityProductInOrder(this.cartInfo.id, product);
    }
  }


  // actualizar el producto en el carrito
  updateQuantityProductInCart(itemId: number, qty: number) {
    this.cartService.updateQuantityProduct(itemId, qty)
      .subscribe(resp => {
        this.totalAmount = 0
        this.cart.products.map((item: any) => {
          if (item.cartItems) {
            this.totalAmount += item.qty * item.cartItems.price;
          }
        });
        // this.getProductCartsItem(this.cartInfo.id);
      }, error => {
        console.log(error);
      });
  }

  // actualizar el producto en el carrito
  updateQuantityProductInOrder(cartId: number, product: any) {
    const data = {
      quantity: product.qty,
      product_id: product.productId
    }
    this.orderService.updateQuantityProduct(cartId, data)
      .subscribe(resp => {
        // console.log(resp);
      }, error => {
        // console.log(error);
      });
  }

  removeFromCart(x: any): void {
    console.log("Se quita del carrito", x.p, x.toDelete);
    const productsId = [x.p.productId];
    if(this.cartInfo?.id){
      // eliminar del carrito
      this.cartService.deleteItemCart(x.p.id).subscribe(resp => {
        console.log(resp);
        this.cartService.setCartActive$(this.user.id);

        // this.getProductCartsItem(this.cartInfo.id);
      }, error => {
        console.log(error);
      });

      //eliminar de la orden
      this.orderService.deleteItemsCart(this.cartInfo?.id, productsId).subscribe(resp => {
        console.log(resp);
        this.totalAmount = 0
        this.cart.products.map((item: any) => {
          if (item.cartItems) {
            this.totalAmount += item.qty * item.cartItems.price;
          }
        });
      }, error => {
        console.log(error);
      });
    }
  }
  atras(){
    this.router.navigate(['/products']);
  }
}
