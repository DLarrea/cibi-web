import { Component, OnInit } from '@angular/core';
import { CartService } from "../../../services/cart.service";
import { OrderService } from "../../../services/order.service";
import { CartInfo, CartItems, Item, OrderFull } from "../../../models/cart";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Store } from "../../../models/store";
import { CheckoutService } from "../../../services/checkout.service";
import { GlobalService } from "../../../services/global.service";
import { AuthService } from 'src/app/services/auth.service';
import { AddressService } from 'src/app/services/address.service';
import { DeliveryTimeService } from 'src/app/services/delivery-time.service';
import { BillingService } from 'src/app/services/billing.service';
import { Router } from '@angular/router';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-my-purchase-checkout',
  templateUrl: './my-purchase-checkout.component.html',
  styleUrls: ['./my-purchase-checkout.component.scss']
})
export class MyPurchaseCheckoutComponent implements OnInit {

  step: number = 0;
  products: any = []
  cartOrder: any;
  totalAmount: number = 0;
  credit: number = 0;
  cart: any = {
    products: []
  };
  totalProductsQtyLabel: string = '0 artículos';
  form = this.fb.group({
    status: null,
    purchaseType: [null, Validators.required],
    totalAmount: [null, Validators.required],
    ownerId: [null, Validators.required],
    cartId: [null, Validators.required],
    storeId: null,
    userPaymentMethodId: [null, Validators.required],
    billingInfoId: [null, Validators.required],
    addressId: null,
    orderItems: null,
  });

  cartInfo: CartInfo | undefined;
  discountAmount = 0;
  deliveryAmount = 0;
  user: any;


  deliveryTime = {
    startTime: '',
    endTime: ''
  };

  delivery: any = {
    id: null,
    city: '',
    address: '',
    name: ''
  };

  purchaseType = {
    description: '',
    value: ''
  };

  billing: any = {
    razonSocial: '',
    ruc: ''
  }

  selectedPaymentMethod: any = {
    id: null,
    value: '',
    description: '',
    checked: false
  };

  billingInfo: any;
  deliverySchedule: any;
  storeList: any
  storeSelected = new Store();

  purchaseTypes: any[] = [
    {
      id: 1,
      value: 'delivery',
      description: 'Delivery',
      img: 'delivery',
      checked: false
    },
    {
      id: 2,
      value: 'pickup',
      description: 'Pick up',
      img: 'pickup',
      checked: false
    },
    {
      id: 3,
      value: 'store',
      description: 'En tienda',
      img: 'store',
      checked: false
    },
  ];

  paymentMethod: any[] = [
    {
      id: 1,
      value: 'efectivo',
      description: 'Efectivo',
      checked: false
    },
    {
      id: 3,
      value: 'tarjeta',
      description: 'Tarjeta de credito',
      checked: false
    }
  ];


  userObservable$ = this.authService.getLoggedUserObservable();

  addresses: any[] = [];


  constructor(private cartService: CartService,
    private orderService: OrderService,
    private fb: FormBuilder,
    private checkoutService: CheckoutService,
    private globalService: GlobalService,
    private authService: AuthService,
    private addresService: AddressService,
    private deliveryTimeService: DeliveryTimeService,
    private billingInfoService: BillingService,
    private router: Router,
    private storeService: StoreService
    ) {
  }

  ngOnInit(): void {
    // user, cartInfo
    // get active order by cart

    this.userObservable$.subscribe(data => {
      this.user = data;
      console.log(data);
      this.getAddresList();
      this.getDeliveryTimeList();
      this.getBillingInfoList();
      this.getStoreList();
    });

    this.cartService.getcartActiveObservable().subscribe(res => {
      this.cartInfo = res
      console.log(res)
      if(res.id){
        this.orderService.getActiveOrderByCart(res.id).subscribe(resp => {
          this.cartOrder = resp;
        });

        this.cartService.getCartItems(res.id).subscribe((resp: CartItems[]) => {
          this.cart.products = resp;
          this.cart.products.map((item: any) => {
            if (item.cartItems) {
              this.totalAmount += item.qty * item.cartItems.price;
            }
          });
          // etiqueta
          this.totalProductsQtyLabel = this.cart.products.length === 1 ? '1 Artículo' : `${this.cart.products.length} Artículos`;
          // this.completeForm();
          console.log(this.cart.products, 'productos');

        })
      }
    });

    const billingInfo = this.checkoutService.getBillingInfoOption();
    if (billingInfo) {
      this.billingInfo = billingInfo;
    }
    const purchaseType = this.globalService.getPurchaseCheckedData();
    if (purchaseType) {
      this.purchaseType = purchaseType;
    }
    // const deliveryTime = this.checkoutService.getDeliveryTimeOption();
    // if (deliveryTime) {
    //   this.deliveryTime = deliveryTime;
    // }
    // const paymentMethod = this.checkoutService.getPaymentMethodOption();
    // if (paymentMethod) {
    //   this.paymentMethod = paymentMethod;
    // }
    // const delivery = this.checkoutService.getDeliveryAddressOption();
    // if (delivery) {
    //   this.delivery = delivery;
    // }
    // const store = this.globalService.getStoreCheckedData();
    // if (store) {
    //   this.store = store;
    // }
  }

  completeForm() {
    this.form.setValue({
      status: 'pending',
      purchaseType: this.purchaseType.value,
      totalAmount: this.totalAmount,
      storeId: this.storeSelected.id ? this.storeSelected.id : null,
      userPaymentMethodId: this.selectedPaymentMethod ? this.selectedPaymentMethod.id : null,
      billingInfoId: this.billing ? this.billing.id : null,
      addressId: this.delivery ? this.delivery.id : null,
      orderItems: this.cart.products,
      ownerId: this.user.id,
      cartId: this.cartInfo?.id,
    });
    console.log("form value: ", this.form.value)

  }


  setStep(index: number) {
    this.step = index;
    // this.completeForm();
  }

  // nextStep() {
  //   this.step++;
  // }

  // prevStep() {
  //   this.step--;
  // }

  getAddresList() {
    this.addresService.getList(this.user.id).subscribe(resp => {
      this.addresses = resp;
      console.log(this.addresses);
      // this.showAddIcon = true;
      // this.loadingShow = false;
      // const item = this.checkoutService.getDeliveryAddressOption();
      // if (item) {
        // this.addresses.find(opt => opt.id === item.id).checked = true;
    //   }
    //   this.loadingService.dismissLoading();
    // }, error => {
    //   this.loadingShow = false;
    //   this.loadingService.dismissLoading();
    //   this.loadingService.presentToastMsg('Ha ocurrido un error', 'warning');
    });
 }

  getDeliveryTimeList() {
    this.deliveryTimeService.getSchedule().subscribe(resp => {
      console.log(resp)
      this.deliverySchedule = resp;
      // this.showAddIcon = false;
      // this.loadingShow = false;
      // const item = this.checkoutService.getDeliveryTimeOption();
      // if (item) {
        // this.deliverySchedule.find(opt => opt.id === item.id).checked = true;
      //}
    //   this.loadingService.dismissLoading();
    // }, error => {
    //   this.loadingShow = false;
    //   this.loadingService.dismissLoading();
    //   this.loadingService.presentToastMsg('Ha ocurrido un error', 'warning');
    });

  }

  getBillingInfoList() {
    this.billingInfoService.getList(this.user.id).subscribe(resp => {
      this.billingInfo = resp;
      console.log(resp)
    //   this.showAddIcon = true;
    //   this.loadingShow = false;
    //   const item = this.checkoutService.getBillingInfoOption();
    //   if (item) {
    //     this.billingInfo.find(opt => opt.id === item.id).checked = true;
    //   }
    //   this.loadingService.dismissLoading();
    // }, error => {
    //   this.loadingShow = false;
    //   this.loadingService.dismissLoading();
    });
  }

  getStoreList(){
    this.storeService.getList().subscribe(resp => {
      this.storeList = resp
      console.log("stores: ", resp)
    })
  }

  placeOrder() {
    const items = this.cart.products.map((item: any) => {
      return {
        quantity: item.qty,
        product_id: item.cartItems.id,
        price: item.cartItems.price,
        status: 'active'
      };
    });

    const order = {
      id: this.cartOrder.id,
      status: this.form.value.status,
      purchase_type: this.form.value.purchaseType,
      total_amount: this.form.value.totalAmount,
      owner_id: this.form.value.ownerId,
      cart_id: this.form.value.cartId,
      store_id: 1, // this.form.value.storeId,
      order_items: items,
      user_payment_method_id: this.form.value.userPaymentMethodId,
      billing_information_id: this.form.value.billingInfoId,
      address_id: this.form.value.addressId
    };

    this.orderService.confirmOrder(order).subscribe((resp: any) => {
      console.log("resp: ", resp)
      // if (resp.id) {
      this.clearItemCarrito(this.cartInfo);
      this.router.navigate(['/products'], { replaceUrl: true });
        // this.clearItemCarrito(this.cartInfo);
        // this.globalService.clearPurchaseChecked();
        // this.globalService.clearCheckout();
        // this.alertService.showAlertModal(this.alertOptionsSuccess);
        // this.router.navigate(['/menu/landing'], { replaceUrl: true });
    }, error => {
      // this.alertService.showAlertModal(this.alertOptionsError);
    });
  }

  clearItemCarrito(cartInfo: any) {
    // this.globalService.setCurrentCart(this.cartInfo);
    // this.cartService.deleteItemsCart(this.cartService.getItemsIds(cartInfo.cartItems)).subscribe(result => {
    //   console.log("delete items carrito: ", result)
    // }, error => {
    //   console.log(error);
    // });
    if(this.cartInfo?.id){
      this.cartService.deleteCartItems(this.cartInfo.id).subscribe(resp => {
        console.log("delete items carrito: ", resp)
      })
    }
  }
  atras(){
    this.router.navigate(['/mypurchase']);
  }
}
