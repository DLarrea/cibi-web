import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPurchaseCheckoutComponent } from './my-purchase-checkout.component';

describe('MyPurchaseBuyComponent', () => {
  let component: MyPurchaseCheckoutComponent;
  let fixture: ComponentFixture<MyPurchaseCheckoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyPurchaseCheckoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPurchaseCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
