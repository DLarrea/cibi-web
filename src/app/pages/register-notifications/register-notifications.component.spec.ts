import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterNotificationsComponent } from './register-notifications.component';

describe('RegisterNotificationsComponent', () => {
  let component: RegisterNotificationsComponent;
  let fixture: ComponentFixture<RegisterNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterNotificationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
