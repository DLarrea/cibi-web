import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterNotificationsRoutingModule } from './register-notifications-routing.module';
import { RegisterNotificationsComponent } from './register-notifications.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    RegisterNotificationsComponent
  ],
  imports: [
    CommonModule,
    RegisterNotificationsRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class RegisterNotificationsModule { }
