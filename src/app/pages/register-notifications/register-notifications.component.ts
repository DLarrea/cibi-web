import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Router } from '@angular/router';
import { Notifications } from 'src/app/models/notifications';
import { GlobalService } from 'src/app/services/global.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-register-notifications',
  templateUrl: './register-notifications.component.html',
  styleUrls: ['./register-notifications.component.scss']
})
export class RegisterNotificationsComponent implements OnInit {
  notificationList: Notifications[] = [];

  constructor(
    private router: Router,
    private globalService: GlobalService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit(): void {
    this.getNotifications();
  }

  getNotifications() {
    this.notificationService.getListNotification().subscribe((resp: Notifications[]) => {
      console.log(resp);
      this.notificationList = resp;
      this.precargarNotificationSelected();
    });
  }

  precargarNotificationSelected() {
    const checkedList = this.globalService.getNotificationForNewAccountData();
    if (checkedList.length > 0) {
      console.log('Tenemos notificaciones pre-seleccionadas');
      checkedList.forEach(notification => {
        const notificationObjIdx = this.notificationList.findIndex(n => n.id === notification.id);
        if (notificationObjIdx > -1) {
          this.notificationList[notificationObjIdx].checked = notification.checked;
          this.notificationList[notificationObjIdx].push = notification.push;
          this.notificationList[notificationObjIdx].email = notification.email;
          this.notificationList[notificationObjIdx].sms = notification.sms;
        } 
      });
    }
  }

  back() {
    const notificationCheckedList = this.notificationList.filter(notification => notification.checked === true);
    this.globalService.setNotificationForNewAccount(notificationCheckedList);
    this.router.navigate(['/register']);
  }

  toggle(val: MatSlideToggleChange, notification: any): void {
    notification.checked = val.checked;
  }

}
