import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { AgmCoreModule } from "@agm/core";
import {SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material/material.module';
import { MyReportRoutingModule } from './my-report-routing.module';
import { MyReportComponent } from './my-report.component';
import {MatListModule} from '@angular/material/list';
import { ChartsModule} from 'ng2-charts';


@NgModule({
  declarations: [
    MyReportComponent,
  ],
  imports: [
    CommonModule,
    MyReportRoutingModule,
    FormsModule,
    AgmCoreModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
    MatListModule,
    ChartsModule
  ]
})
export class MyReportModule { }
