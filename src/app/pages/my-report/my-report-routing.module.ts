import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyReportComponent } from './my-report.component';

const routes: Routes = [
  {
    path: '',
    component: MyReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyReportRoutingModule { }
