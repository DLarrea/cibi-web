import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartType } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { ReportFormComponent } from 'src/app/dialog/report-form/report-form.component';
import { ReportExpenses, ReportExpensesCategory, Month } from 'src/app/models/report';
import { User } from 'src/app/models/user';
import { GlobalService } from 'src/app/services/global.service';
import { ReportService } from 'src/app/services/report.service';
import { Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-my-report',
  templateUrl: './my-report.component.html',
  styleUrls: ['./my-report.component.scss']
})
export class MyReportComponent implements OnInit {

  foods = ['Mensual', 'Semanal', 'Diario', 'Anual'];
  totalForMonth = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  budgetForMonth = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  chartData: ChartDataSets[] = [
    {
      data: [1000000, 120000, 1000000, 800000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000],
      type: 'line',
      fill: false,
      backgroundColor: '#c4540b',
      pointRadius: 0,
      borderColor: '#c4540b',
      label: 'Presupuesto'
    },
    { data: [50000, 100000, 800000, 500000, 400000, 800000, 750000, 638000, 950000, 1200000, 500000, 1000000], label: 'Gasto' }
  ];
  chartLabels: Label[] = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

  chartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Historic Stock Price'
    },
    pan: {
      enabled: true,
      mode: 'xy'
    }
  };

  chartColors: Color[] = [
    {
      borderColor: 'aquamarine',
      backgroundColor: 'aquamarine'
    }
  ];
  chartType: ChartType = 'bar';
  showLegend = false;

  pieChartData: ChartDataSets[] = [
    {
      data: [235000, 450000, 125000, 50000, 800000],
      label: 'Budget',
      backgroundColor: [
        '#ff6384',
        '#fcde00',
        '#003e8c',
        '#2dd36f',
      ]
    }
  ];
  pieChartLabels: Label[] = ['Verduras', 'Carnes', 'Bebidas', 'Limpieza', 'Cosméticos'];
  pieChartType: ChartType = 'pie';
  pieChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: ''
    },
    pan: {
      enabled: true,
      mode: 'xy'
    }
  };

  user!: User;
  presupuestoMensual = 1000000;
  loadingPresupuesto = true;

  constructor(
    private router: Router,
    private reportService: ReportService,
    private loadingService: LoadingService,
    private globalService: GlobalService,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    console.log("a");
    // this.user = this.globalService.getUserData();
    // console.log('userid: ', this.user.id);
    // this.getUserBudget(this.user.id);
    // this.getReportExpenses();
    // this.getReportExpensesCategory();
  }

  getUserBudget(id: number) {
    this.loadingService.presentLoading('getUserBudget').then(() => {
      this.reportService.getUserBudgetById(id).subscribe(resp => {
        this.findBudgetMonth(resp);
      });
    }, () => {
      this.loadingPresupuesto = false;
      this.loadingService.dismissLoading('getUserBudget');
    });
  }

  findBudgetMonth(resp: any[]) {
    const date = new Date();
    const mesActual = date.getMonth() + 1;
    const presupuesto = resp.filter(data => data.month === mesActual);
    if (presupuesto.length > 0) {
      const dataM = resp.filter(data => data.month === mesActual).reduce((prev, curr) => {
        return prev.id > curr.id ? prev : curr;
      });
      this.presupuestoMensual = dataM.amount;
      console.log('presupuesto: ', this.presupuestoMensual, ' mes: ', mesActual);
      let pos = 0;
      this.budgetForMonth.forEach(budget => {
        if (pos >= mesActual - 1) {
          this.budgetForMonth[pos] = this.presupuestoMensual;
        }
        pos++;
      });
      this.chartData[0].data = this.budgetForMonth;
      console.log(this.chartData[0].data);
      this.loadingService.dismissLoading('getReportExpenses');
    } else {
      this.presupuestoMensual = 0;
    }
    this.loadingPresupuesto = false;
    this.loadingService.dismissLoading('getUserBudget');
  }

  getReportExpensesCategory() {
    this.loadingService.presentLoading('getReportExpensesCategory').then(() => {
      this.reportService.getExpensesReportCategory(this.user.id).subscribe(resp => {
        this.preparedReportExpensesCategory(resp);
      }, error => {
        console.log(error);
        this.loadingService.dismissLoading('getReportExpensesCategory');
        this.loadingService.presentToastMsg('Ha ocurrido un problema');
      });
    });
  }

  getReportExpenses() {
    this.loadingService.presentLoading('getReportExpenses').then(() => {
      this.reportService.getExpensesReport(this.user.id).subscribe(resp => {
        this.preparedReportExpenses(resp);
      }, error => {
        console.log(error);
        this.loadingService.dismissLoading('getReportExpenses');
        this.loadingService.presentToastMsg('Ha ocurrido un problema');
      });
    });
  }

  preparedReportExpenses(resp: ReportExpenses[]) {
    let monthInfo = null;
    // recuperar info del mes
    // actualizar el total del mes

    resp.forEach(reportMonth => {
      monthInfo = this.reportService.monthList;
      this.totalForMonth[monthInfo.JANUARY.index] = reportMonth.total;
    });
    this.chartData[1].data = this.totalForMonth;
    this.loadingService.getLoadingObservable();
  }

  preparedReportExpensesCategory(resp: ReportExpensesCategory[]) {
    this.loadingService.dismissLoading('getReportExpensesCategory');
    resp.forEach(reportMonth => {
      this.pieChartData[0].data?.push(reportMonth.total);
      this.pieChartLabels.push(reportMonth.category);
    });
  }

  showReportDialog(): void {
    const dialog = this.dialog
      .open(ReportFormComponent, {
        height: '450px',
        width: '930px',
        disableClose: false,
        data: {
          user: this.user,
          ultimoPresupuesto: this.presupuestoMensual,
        },
      });

    dialog.afterClosed().subscribe((data) => {
      if (data) {

        this.presupuestoMensual = data.totalForMonth;
        const date = new Date();
        const dataForm = {
          month: date.getMonth() + 1,
          year: date.getFullYear(),
          amount: data.totalForMonth
        };

        // this.reportService.updateAllBudget(this.user.id, data).subscribe(result => {
        //   window.location.reload();
        //   console.log(result)
        //   console.log('se guardo el presupuesto', response);
        // });
        // this.reportService.saveUSerBudget(this.user.id, data).subscribe(result => {
        //   this.getUserBudget(this.user.id);
        //   this.router.navigate(['menu/report']);
        //   window.location.reload();
        //   console.log(result);
        // },
        // error => {
        //   console.log(error);
        // });

      }
    });
  }
}
