import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterPreferencesRoutingModule } from './register-preferences-routing.module';
import { RegisterPreferencesComponent } from './register-preferences.component';
import { MaterialModule } from 'src/app/material/material.module';


@NgModule({
  declarations: [
    RegisterPreferencesComponent
  ],
  imports: [
    CommonModule,
    RegisterPreferencesRoutingModule,
    MaterialModule,
  ]
})
export class RegisterPreferencesModule { }
