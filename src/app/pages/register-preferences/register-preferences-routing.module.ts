import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterPreferencesComponent } from './register-preferences.component';

const routes: Routes = [
  {
    path: '',
    component: RegisterPreferencesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterPreferencesRoutingModule { }
