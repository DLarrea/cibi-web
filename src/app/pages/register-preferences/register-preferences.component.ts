import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion/accordion';
import { Router } from '@angular/router';
import { Preference, PreferenceCategory, PreferenceUserForNewAccount, UserPreference } from 'src/app/models/preferences';
import { GlobalService } from 'src/app/services/global.service';
import { LoadingService } from 'src/app/services/loading.service';
import { PreferenceService } from 'src/app/services/preference.service';

@Component({
  selector: 'app-register-preferences',
  templateUrl: './register-preferences.component.html',
  styleUrls: ['./register-preferences.component.scss']
})
export class RegisterPreferencesComponent implements OnInit {
  //@ViewChild(MatAccordion) accordion: MatAccordion;
  
  preferenceCategoryName = '';
  preferenceCategoryId = 0;
  listUserPreferences: UserPreference[] = [];
  preferenceCategoryList: PreferenceCategory[] = [];
  preferenceUserForNewAccountList: PreferenceUserForNewAccount[] = [];
  //preferenceUserForNewAccount: PreferenceUserForNewAccount = new PreferenceUserForNewAccount();

  constructor(
    private router: Router,
    private globalService: GlobalService,
    private loadingService: LoadingService,
    private preferenceService: PreferenceService
  ) { }

  ngOnInit(): void {
    this.getPreferenceCategory();
    // consultar si ya existe en memoria un listado de preferencias de usuario para nueva cuenta
    this.preferenceUserForNewAccountList = this.globalService.getPreferenceUserForNewAccountData();
    console.log('Listado de preferencias del storage', this.preferenceUserForNewAccountList);
  }

  getPreferenceCategory() {
    this.preferenceService.getPreferenceCategory().subscribe(resp => {
      console.log(resp);
      this.preferenceCategoryList = resp;
    }, error => {
      console.log(error);
    });
  }

  back() {
    this.router.navigate(['/register']);
  }

  preferenceByCategory(category: PreferenceCategory) {
    console.log(category);
    this.preferenceService.getPreferenceByPreferenceCatarogyId(category.id).subscribe(resp => {
      category.preferenceList = resp;
      this.preCargarPreferenciasUser(category);
    });
  }

  selectPreference(preference: Preference) {
    console.log(preference);
    console.log(this.preferenceUserForNewAccountList);
    const preferenceCategory = this.preferenceUserForNewAccountList.find(pc => pc.preferenceCategoryId === preference.preferenceCategoryId);
    if (preferenceCategory && this.existsPreferenceInCategory(preferenceCategory, preference)) {
      preferenceCategory.preferenceUserList = preferenceCategory.preferenceUserList.filter(p => p.id !== preference.id);
      this.globalService.setPreferenceUserForNewAccount(this.preferenceUserForNewAccountList);
    } else if (preferenceCategory && !this.existsPreferenceInCategory(preferenceCategory, preference)) {
      preferenceCategory.preferenceUserList.push(preference);
      this.globalService.setPreferenceUserForNewAccount(this.preferenceUserForNewAccountList);
    } else {
      this.createNewPreferenceStore(preference);
    }
  }

  existsPreferenceInCategory(preferenceCategory: PreferenceUserForNewAccount, preference: Preference): boolean {
    let obj = preferenceCategory.preferenceUserList.find(p => p.id === preference.id);
    if (obj) {
      console.log('Encontre el obj');
      return true;
    } else {
      console.log('No encontre el obj');
      return false;
    }
  }

  createNewPreferenceStore(preference: Preference) {
    const preferenceCategory = new PreferenceUserForNewAccount();
    preferenceCategory.preferenceCategoryId = preference.preferenceCategoryId;
    preferenceCategory.preferenceUserList.push(preference);
    this.preferenceUserForNewAccountList.push(preferenceCategory);
    this.globalService.setPreferenceUserForNewAccount(this.preferenceUserForNewAccountList);
  }

  preCargarPreferenciasUser(category: PreferenceCategory) {
    const userCategory = this.preferenceUserForNewAccountList.find(pc => pc.preferenceCategoryId === category.id);
    userCategory?.preferenceUserList.forEach(preference => {
      let obj = category.preferenceList.find(p => p.id === preference.id);
      if (obj) {
        obj.checked = true;
      }
    })
    
  }
}

//Buscar las categorias que el usuario selecciono. 
//Buscar las preferencias que estan marcadas de esa categoria.