import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material/material.module';
import { MyPreferencesRoutingModule } from './my-preferences-routing.module';
import { MyPreferencesComponent } from './my-preferences.component';
import {MatListModule} from '@angular/material/list'; 


@NgModule({
  declarations: [
    MyPreferencesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    MyPreferencesRoutingModule,
    FormsModule,
    MatListModule
  ]
})
export class MyPreferencesModule { }
