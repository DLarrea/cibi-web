import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { UserPreferenceCategory } from 'src/app/models/cart';
import { Preference, PreferenceCategory, PreferenceUserForNewAccount, UserPreference } from 'src/app/models/preferences';
import { GlobalService } from 'src/app/services/global.service';
import { LoadingService } from 'src/app/services/loading.service';
import { PreferenceService } from 'src/app/services/preference.service';
import { UserPreferenceService } from 'src/app/services/user-preference.service'
@Component({
  selector: 'app-my-preferences',
  templateUrl: './my-preferences.component.html',
  styleUrls: ['./my-preferences.component.scss']
})
export class MyPreferencesComponent implements OnInit {

  preferenceCategoryName = '';
  preferenceCategoryId = 0;
  listUserPreferences: UserPreference[] = [];
  preferenceCategoryList: PreferenceCategory[] = [];
  preferenceUserForNewAccountList: PreferenceUserForNewAccount[] = [];
  categorias: any[] = [];

  userLogged = false;
  preferenceUserCategoryList: UserPreferenceCategory[] = [];

  constructor(private router: Router,
    private globalService: GlobalService,
    private loadingService: LoadingService,
    private preferenceService: PreferenceService,
    private userPreferenceService: UserPreferenceService

  ) { }

  ngOnInit(): void {
    console.log("a");
    this.conseguirCategorias();
  }

  conseguirCategorias(){
    this.preferenceService.getPreferenceCategory().subscribe(res => {
      this.categorias = res;
      console.log("categorias", this.categorias)
    });
  }


  // getDataForUserLogged() {
  //  forkJoin([
  //       this.preferenceService.getPreferenceCategory(),
  //       this.userPreferenceService.getUserPreferences(this.globalService.clearStorage)
  //     ]).subscribe(results => {
  //       this.preferenceCategoryList = results[0];
  //       this.preferenceUserCategoryList = results[1];
  //       this.precargarCantPreferences();

  //     }, error => {
  //       console.log(error);
  //     });
  // }

  // getDataForUserNotLogged() {
  //   this.preferenceService.getPreferenceCategory().subscribe(resp => {
  //       this.preferenceCategoryList = resp;

  //     }, error => {
  //       console.log(error);
  //     });
  // }

  back() {
    this.router.navigate(['/new-account']);
  }

  goToPreferenceDetail(row: PreferenceCategory) {
    if (this.userLogged) {
      this.router.navigate(['/menu/preferences/detail', row.id], { queryParams: { name: row.name } });
    } else {
      this.router.navigate(['/new-account/preferences/detail', row.id], { queryParams: { name: row.name } });
    }
  }

  private precargarCantPreferences() {
    console.log('preferencias actuales del usuario: ', this.preferenceUserCategoryList);
    if (this.userLogged) {
      this.preferenceCategoryList.forEach(category => {
        console.log('Categoria: ', category);
        this.preferenceService.getPreferenceByPreferenceCatarogyId(category.id).subscribe(preferenceList => {
          console.log('Preferencias de la categoria: ', preferenceList);
          this.getCountPreferencesInExist(this.preferenceUserCategoryList, preferenceList, category);
        });
      });
    }
  }


  private getCountPreferencesInExist(userPreferencesList: any[], preferencesCategoryList: any[], category: PreferenceCategory) {
    userPreferencesList.forEach(up => {
      preferencesCategoryList.forEach(pc => {
        if (pc.id === up.preferenceId && category.id === pc.preferenceCategoryId) {
          console.log('Encontre la preferencia para la categoria ', category);
          category.count += 1;
        }
      });
    });
  }

}
