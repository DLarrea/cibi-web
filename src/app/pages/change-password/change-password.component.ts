import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { Snackbar } from 'src/app/interfaces/snackbar';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  user: any;
  userObservable$ = this.authService.getLoggedUserObservable();
  

  form = this.formbuilder.group({
    passwordOld: [null, Validators.required],
    password: [null, Validators.required],
    passwordRepeat: [null, Validators.required],
  });

  get passwordOld(): FormControl {
    return this.form.get('passwordOld') as FormControl;
  }

  get password(): FormControl {
    return this.form.get('password') as FormControl;
  }

  get passwordRepeat(): FormControl {
    return this.form.get('passwordRepeat') as FormControl;
  }

  constructor(
    private formbuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private userService: UserService,
    private snackBarService: SnackbarService,

  ) { }

  ngOnInit(): void {
    this.userObservable$.subscribe(
      resp => {
        console.log(resp)
        this.user = resp;
      }
    );
  }

  onSubmit() {

  }

  changePassword() {
    console.log("el id del usuario:", typeof(this.user.id) 
    );
    console.log("la contraseña del usuario:", typeof(this.form.value.passwordOld) );
    console.log("nueva contraseña:", typeof(this.form.value.password)); 
    if (this.isPasswordValid()) {
      this.userService.changePassword(this.user.id, this.form.value.passwordOld, this.form.value.password)
        .subscribe(resp => {
          console.log(resp);
          this.snackBarService.openSnackBar({
            message: 'Su contraseña ha sido modificada correctamente',
            position: 'top',
            className: 'snack-bar-class',
            classAction: 'success',
            success: false,
            duration: 5000
          } as Snackbar);
          this.router.navigateByUrl('/');
        }, error => {
          console.log('error ', error);
          if (error.status === 400) {
            this.snackBarService.openSnackBar({
              message: 'Su contraseña actual no coincide con la ingresada',
              classAction: 'error',
              success: false,
              className: 'snack-bar-class',
              duration: 5000,
              position: 'top'
            } as Snackbar);
          } else {
            this.snackBarService.openSnackBar({
              message: 'Vuelva a intentar mas tarde',
              classAction: 'error',
              success: false,
              className: 'snack-bar-class',
              duration: 5000,
              position: 'top'
            } as Snackbar);
          }
        });
    }
  }

  isPasswordValid() {
    const oldPassword = this.form.get('passwordOld')!.value;
    const newPassword = this.form.get('password')!.value;
    const newPasswordRepeat = this.form.get('passwordRepeat')!.value;
    if (oldPassword === null || oldPassword === '') {
      this.snackBarService.openSnackBar({
        message: 'Por favor complete el campo de su contraseña actual ',
        classAction: 'error',
        success: false,
        className: 'snack-bar-class',
        duration: 5000,
        position: 'top'
      } as Snackbar);
      return false;
    }
    if (newPassword === null || newPassword === '')  {
      this.snackBarService.openSnackBar({
        message: 'Por favor complete el campo de su contraseña nueva ',
        classAction: 'error',
        success: false,
        className: 'snack-bar-class',
        duration: 5000,
        position: 'top'
      } as Snackbar);
      return false;
    }
    if (newPasswordRepeat === null || newPasswordRepeat === '')  {
      this.snackBarService.openSnackBar({
        message: 'Por favor complete el campo de la repetición de su contraseña nueva',
        classAction: 'error',
        success: false,
        className: 'snack-bar-class',
        duration: 5000,
        position: 'top'
      } as Snackbar);
      return false;
    }
    if (!(newPassword === newPasswordRepeat))  {
      this.snackBarService.openSnackBar({
        message: 'Por favor verifique las contraseñas ingresadas',
        classAction: 'error',
        success: false,
        className: 'snack-bar-class',
        duration: 5000,
        position: 'top'
      } as Snackbar);
      return false;
    }
    return true;
  }

  back() {
    this.router.navigate(['/menu/profile']);
  }

  close(): void {
    this.router.navigate(['/'])
  }
}
