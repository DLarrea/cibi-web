import { Component, OnInit } from '@angular/core';
import { AddressService } from "../../../services/address.service";
import { Location } from "@angular/common";
import { Subscription } from "rxjs";
import { Address } from "../../../models/address";
import { ConfirmActionService } from "../../../dialog/confirm-action/confirm-action.service";
import { AuthService } from "../../../services/auth.service";
import { AddressFormComponent } from "../../../dialog/address-form/address-form.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  loading = true;
  loadingSubscription?: Subscription;
  user: any
  messageNotAddressList = 'La lista de direcciones esta vacía';
  addressList: Address[] = []
  userObservable$ = this.authService.getLoggedUserObservable();

  constructor(private addressService: AddressService,
              private _location: Location,
              public dialog: MatDialog,
              private authService: AuthService,
              private confirmActionService: ConfirmActionService) { }

  ngOnInit(): void {
    this.userObservable$.subscribe(data => {
      this.user = data;
      console.log(data)
    });
    this.loadAddress();
  }

  loadAddress() {
    //TODO: Manejo de mensajes de error
    this.addressService.getList(this.user.id).subscribe(data => {
      this.addressList = data;
      console.log(data);
    });
  }

  back() {
    this._location.back();
  }

  addAddressDialog(): void {
    const dialog = this.dialog
      .open(AddressFormComponent, {
        height: '780px',
        width: '930px',
        disableClose: false,
        data: {},
      });

    dialog.afterClosed().subscribe(data => {
      if (data) {
        this.addressService.create(this.user.id, data).subscribe(response => {
          this.loadAddress();
        })
      }
    });
  }

  handleClickDelete(id: number | undefined) {
    if (id) {
      const options = {
        title: 'Eliminar dirección',
        message: '¿Desea eliminar la dirección seleccionada?',
        cancelText: 'Cancelar',
        confirmText: 'Sí'
      };
      this.confirmActionService.open(options);

      this.confirmActionService.confirmed().subscribe(confirmed => {
        if (confirmed) {
          this.deleteAddress(id);
        }
      });
    }
  }

  deleteAddress(id: number) {
    this.addressService.delete(this.user.id, id).subscribe(response => {
      this.loadAddress();
    })
  }

  editAddressDialog(address: Address): void {
    if (address && address.id) {
      const dialog = this.dialog
        .open(AddressFormComponent, {
          height: '780px',
          width: '930px',
          disableClose: false,
          data: {
            address: address,
            title: 'Editar Dirección'},
        });

      dialog.afterClosed().subscribe(data => {
        if (data && address.id) {
          this.addressService.edit(this.user.id, address.id, data).subscribe(response => {
            this.loadAddress();
          });
        }
      });
    }



  }
}
