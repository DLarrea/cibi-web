import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from "../../material/material.module";
import { MyAddressRoutingModule } from "./my-address-routing.module";
import { ListComponent } from './list/list.component';
import { AgmCoreModule } from "@agm/core";


@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    MyAddressRoutingModule,
    MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCNYuJWWvEBf_VTFxzQkhfF1xVKhURdD6M'
    }),
  ]
})
export class MyAddressModule { }
