import { parseISO } from 'date-fns';
import { Moment } from 'moment';
import differenceInCalendarMonths from 'date-fns/differenceInCalendarMonths';

export const getTime = (fecha: Moment | string | undefined): number => {
  return typeof fecha === 'string' ? parseISO(fecha).getTime() : (fecha?.valueOf() ?? 0);
};

export const getDate = (fecha: Moment | string | undefined): Date | undefined => {
  return typeof fecha === 'string' ? parseISO(fecha) : fecha?.toDate();
};

export const diffBetweenMonths = (fechaInicio: Moment | string | undefined, fechaFin: Moment | string | undefined): number => {
  if (fechaInicio && fechaFin) {
    const inicio = getTime(fechaInicio);
    const fin = getTime(fechaFin);
    const calc = (fin - inicio) / 1000 / 60 / 60 / 24 / 30.41666667;
    return Math.round(calc);
  }
  return 0;
};

export const getVidaUtil = (
  fechaPrimeraCuota: Moment | string | undefined,
  fechaCuotaActual: Moment | string | undefined | null,
  horizonteAnalisis: number
): number => {
  if (fechaPrimeraCuota && fechaCuotaActual) {
    const inicio = getDate(fechaPrimeraCuota);
    const fin = getDate(fechaCuotaActual);
    if (inicio && fin) {
      console.log(inicio, fin, differenceInCalendarMonths(fin, inicio));
      const calc = differenceInCalendarMonths(fin, inicio);
      return horizonteAnalisis - (Math.round(calc));
    }
    return horizonteAnalisis;
  }
  return horizonteAnalisis;
};
