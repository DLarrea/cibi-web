import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Item, ShoppingList } from '../models/cart';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  private baseUrl = `${environment.api}/shopping_list`;
  private shoppingList: BehaviorSubject<ShoppingList[]> = new BehaviorSubject<ShoppingList[]>([]);

  constructor(private http: HttpClient) { }

  setShoppingList$(list: ShoppingList[]){
    this.shoppingList.next(list);
  }

  getShoppingList(): Observable<ShoppingList[]>{
    return this.shoppingList.asObservable();
  }

  getListById(id: number) {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<any>(url);
  }

  getByCartId(cartId: number): Observable<ShoppingList[]> {
    const url = `${this.baseUrl}/cart/${cartId}`;
    return this.http.get<any>(url)
      .pipe(
        map((list) => {
          return list.map((item: any) => {
            const obj = new ShoppingList();
            obj.id = item.id;
            obj.cartId = item.cart_id;
            obj.name = item.name;
            obj.active = item.active;
            // obj.color = null;
            obj.shoppingListItems = this.parseToItems(item.shopping_list_items);

            return obj;
          });
        }));
  }
  parseToItems(list: any[]): Item[] {
    return list.map(item => {
      return this.parseToItem(item);
    });
  }

  parseToItem(data: any): Item {
    const obj = new Item();
    obj.id = data.id;
    obj.qty = data.qty;
    obj.chooseItem = data.choose_item;
    obj.product = this.parseToProduct(data.product);
    obj.price = obj.product.price;
    obj.productId = obj.product.id;
    obj.quantity = data.qty;
    obj.status = data.status;
    obj.checked = false;
    return obj;
  }

  parseToProduct(data: any): Product {
    const subcategoryId = data.subcategory_id;
    delete data.subcategory_id;
    let obj = new Product();
    obj = data;
    obj.subcategoryId = subcategoryId;
    return obj;
  }

  getItems(listId: number): Observable<any[]> {
    const url = `${this.baseUrl}/items/${listId}`;
    return this.http.get<any>(url);
  }

  createList(data: any): Observable<any> {
    const url = `${this.baseUrl}/`;
    return this.http.post<any>(url, data);
  }

  editList(listId: number, data: any) {
    const url = `${this.baseUrl}/${listId}`;
    return this.http.put<any>(url, data);
  }

  addItem(data: any): Observable<any> {
    const url = `${this.baseUrl}/item/add`;
    return this.http.post<any>(url, data);
  }

  addItemToLists(data: any): Observable<any> {
    const url = `${this.baseUrl}/product/`;
    return this.http.post<any>(url, data);
  }

  removerItem(itemId: number): Observable<any> {
    const url = `${this.baseUrl}/item/${itemId}`;
    return this.http.delete<any>(url);
  }

  removeList(listId: number): Observable<any> {
    const url = `${this.baseUrl}/${listId}`;
    return this.http.delete<any>(url);
  }

  removeItemsFromList(listId: number, data: any): Observable<any> {
    const url = `${this.baseUrl}/${listId}/items`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: {
        shopping_list_items: data
      }
    };
    return this.http.delete<any>(url, options);
  }

  updateList(listId: number, list: any[]): Observable<any> {
    const data = {
      shopping_list_items: list
    };
    const url = `${this.baseUrl}/${listId}/items`;
    return this.http.post<any>(url, data);
  }

  addToList(listId: number, list: any[]): Observable<any> {
    const data = {
      shopping_list_items: list
    };
    const url = `${this.baseUrl}/${listId}/items/add`;
    return this.http.post<any>(url, data);
  }

  updateQuantityList(listItemId: number, qty: number) {
    const url = `${this.baseUrl}/item/${listItemId}/quantity/${qty}`;
    return this.http.put<any>(url, {});
  }
}
