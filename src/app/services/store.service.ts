import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Store } from '../models/store';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  baseUrl: string;
  constructor(private http: HttpClient
  ) {
    const apiUrl = environment.api || 'http://localhost:8001';
    this.baseUrl = `${apiUrl}/store`;
  }

  getStores(): Observable<Store[]> {
    const url = `${this.baseUrl}/`;
    return this.http.get<any>(url)
    .pipe(
      map((storeList) => {
        return storeList.map((store: any) => {
          let obj = new Store();
          obj = store;
          obj.checked = false;
          return obj;
        });
      }));;
  }

  getList(): Observable<Store[]> {
    const url = `${this.baseUrl}/full`;
    return this.http.get<any>(url)
    .pipe(
      map((storeList) => {
        return storeList.map((store: any) => {
          let obj = new Store();
          obj = store;
          obj.checked = false;
          obj.businessHours = 'Abierto de 8:00 a 21:00 hs.';
          obj.mapImage = 'assets/img/fernandodelamora.png';
          obj.address = store.address;
          obj.location = 'Fernando de la Mora'
          return obj;
        });
      }));;
  }
}
