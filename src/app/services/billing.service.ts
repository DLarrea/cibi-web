import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BillingInfo } from 'src/app/models/billing-info';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class BillingService extends BaseService {

  constructor(private http: HttpClient) {
    super();
  }

  create(userId: number, formData: any): Observable<BillingInfo> {
    return this.http.post<any>(`${this.apiUrl}/users/${userId}/billing-info/`, formData);
  }

  getList(userId: number): Observable<BillingInfo[]> {
    return this.http.get<any>(`${this.apiUrl}/users/${userId}/billing-info/`)
      .pipe(
        map((billingList) => {
          return billingList.map((billing: { id: number | undefined; is_active: boolean | undefined; razon_social: string | undefined; ruc: string | undefined; user_id: number | undefined; }) => {
            const billingObj = new BillingInfo();
            billingObj.id = billing.id;
            billingObj.isActive = billing.is_active;
            billingObj.razonSocial = billing.razon_social;
            billingObj.ruc = billing.ruc;
            billingObj.userId = billing.user_id;
            return billingObj;
          });
        }));
  }

  edit(userId: number, billingInfoId: number, formData: any): Observable<BillingInfo> {
    return this.http.put<any>(`${this.apiUrl}/users/${userId}/billing-info/${billingInfoId}`, formData);
  }

  delete(userId: number, billingInfoId: number): Observable<BillingInfo> {
    return this.http.delete<any>(`${this.apiUrl}/users/${userId}/billing-info/${billingInfoId}`);
  }
}