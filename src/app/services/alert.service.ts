import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertComponent } from '../dialog/alert/alert.component';
import { Snackbar } from '../interfaces/snackbar';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  async showAlertDialog (data: Snackbar) {
    this.snackBar.openFromComponent(AlertComponent,{
      verticalPosition: 'top',
      duration: 200000,
      data,
      panelClass: `${data.className}-${data.classAction}`
    });
  }
}
