import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Preference, PreferenceCategory, UserPreference, UserPreferenceCategory } from '../models/cart';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class UserPreferenceService extends BaseService {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  /**
   * @returns listado de user-preferencias-category por usuario
   */
  getUserPreferenceCategory(userId: number): Observable<UserPreferenceCategory[]> {
    const url = `${this.apiUrl}/${userId}/user-preference-category/`;
    return this.http.get<any>(url).pipe(
      map((list) => {
        return list.map((preference: any) => {
          return this.parserToPreferenceCategory(preference);
        });
      })
    );
  }

  /**
   * @returns listado de preferencias por preference-category por usuario
   */
  getUserPreferences(userId: number, preferenceCategoryId?: number): Observable<UserPreference[]> {
    const url = `${this.apiUrl}/user/${userId}/user-preference/`;
    return this.http.get<any>(url)
    .pipe(
      map( (list) => {
        return list.map((preference: any) => {
          return this.parserToPreference(preference);
        });
      })
    );
  }


  /**
   * Agregar preference category a usuario
   */
  addPrefrenceCategoryUser(userId: number, preferenceCategoryId: number): Observable<any> {
    const url = `${this.apiUrl}/user/${userId}/preference-category/${preferenceCategoryId}`;
    return this.http.post<any>(url, {});
  }

  /**
   * Remover preferencia category del usuario
   */
  deleteUserPreferenceCategory(userId: number, preferenceCategoryId: number): Observable<any> {
    const url = `${this.apiUrl}/user/${userId}/preference-category/${preferenceCategoryId}`;
    return this.http.delete<any>(url);
  }

  /**
   * Agregar preferencia a usuario
   */
   addPrefrenceUser(userId: number, preferenceId: number, preference: Preference): Observable<any> {
    const url = `${this.apiUrl}/user/${userId}/preference/${preferenceId}`;
    return this.http.post<any>(url, preference.name);
  }

  /**
   * Remover preferencia del usuario
   */
  deleteUserPreference(userId: number, preferenceId: number): Observable<any> {
    const url = `${this.apiUrl}/user/${userId}/preference/${preferenceId}`;
    return this.http.delete<any>(url);
  }

  private parserToPreferenceCategory(data: { id: number | undefined; user_id: number | undefined; is_active: boolean | undefined; preference_category_id: number | undefined; }): UserPreferenceCategory {
    const obj = new UserPreferenceCategory();
    obj.id = data.id;
    obj.userId = data.user_id;
    obj.isActive = data.is_active;
    obj.preferenceCategoryId = data.preference_category_id;
    return obj;
  }

  private parserToPreference(data: { id: number | undefined; name: string | undefined; description: string | undefined; user_id: number | undefined; preference_id: number | undefined; is_active: boolean | undefined; }): UserPreference {
    const obj = new UserPreference();
    obj.id = data.id;
    obj.name = data.name;
    obj.description = data.description;
    obj.userId = data.user_id;
    obj.preferenceId = data.preference_id;
    obj.isActive = data.is_active;
    obj.checked = true;
    return obj;
  }

}