import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BaseService } from './base.service';
import { map } from 'rxjs/operators';
import { Schedule } from '../models/schedule'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeliveryTimeService {
  private baseUrl = `${environment.api}/delivery_schedule`;

  constructor(private http: HttpClient) {
  }

  getSchedule(){
    const date = new Date().toISOString()
    return this.http.get<any>(`${this.baseUrl}/${date}/3`).pipe(
      map((scheduleList) => {
        return scheduleList.map((schedule: { id: number; start_time: string; end_time: string; }) => {
          const scheduleObj = new Schedule();
          scheduleObj.id = schedule.id;
          scheduleObj.startTime = schedule.start_time;
          scheduleObj.endTime = schedule.end_time;
          return scheduleObj;
        });
      }));
  }
}
