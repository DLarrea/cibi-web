import { Injectable } from '@angular/core';
import { PRINCIPAL_BREAKPOINT } from 'src/app/constant';

@Injectable({
  providedIn: 'root'
})
export class ResponsiveService {

  constructor() { }

  get isMobileScreen(): boolean {
    return window.innerWidth < PRINCIPAL_BREAKPOINT;
  }
}
