import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Characteristic } from '../models/characteristic';

@Injectable({
  providedIn: 'root'
})
export class CharacteristicsService {

  constructor(private http: HttpClient) {
    const apiUrl = environment.api || 'http://localhost:8001';

    this.baseUrl = `${apiUrl}/characteristics`;
  }

  baseUrl: string;

  getCharsFull() {
    const url = `${this.baseUrl}/full`;
    return this.http.get<any>(url);
  }

  getChars() {
    const url = `${this.baseUrl}/`;
    return this.http.get<any>(url);
  }

  getCharsByProductId(productId: number) {
    const url = `${this.baseUrl}/product/${productId}`;
    return this.http.get<any>(url).pipe(map(list => {
      return list.map((char: any) => {
        let obj = new Characteristic();
        obj = char;
        if (obj.icon) {
          obj.type = 'tag';
        } else {
          obj.type = 'component';
        }
        return obj;
      });
    }));
  }

  deleteChars(charId: any) {
    const url = `${this.baseUrl}/${charId}`;
    return this.http.delete<any>(url);
  }

}
