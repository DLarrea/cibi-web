import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RecoverPassword } from '../interfaces/user';
import { Address } from '../models/address';
import { User } from '../models/user';
import { Notifications } from '../models/notifications';
import { Preference, PreferenceUserForNewAccount } from '../models/preferences';
import { Store } from '../models/store';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = `${environment.api}/users`;

  constructor(
    private http: HttpClient
  ) { }


  changePassword(
    userId: any,
    oldPassword: string,
    newPassword: string
  ) {
    const url = `${this.baseUrl}/${userId}/password`;
    const data = {
      old_password: oldPassword,
      new_password: newPassword
    };
    return this.http.post<any>(url, data);
  }

  recoverPassword (email: string) {
    const data = {
      email
    }
    return this.http.post<RecoverPassword[]>(`${this.baseUrl}/recover-password`, data);
  }

  edit(user_id: number, formData: any) {
    const url = `${this.baseUrl}/${user_id}/`;
    console.log(url);
    return this.http.put<any>(url, formData);
  }

  me(): Observable<User> {
    const url = `${this.baseUrl}/me/`;
    return this.http.get<any>(url)
      .pipe(
        map((user: any) => {
          return this.parserToUser(user);
      }));
  }

  register(
    dataForm: any,
    userOptions: any,
    signUpWith: string
  ): Observable<any> {
    const url = `${this.baseUrl}/`;
    const signUpOptions = {
      cibi: false,
      fb: false,
      google: false,
      apple: false
    };
    // signUpOptions[signUpWith] = true;
    const data = {
      email: dataForm.get('email').value,
      password: dataForm.get('password').value,
      name: dataForm.get('name').value,
      last_name: '',
      phone_number: dataForm.get('phone').value,
      business_name: dataForm.get('bussinesName').value,
      ruc: dataForm.get('ruc').value,
      cibi_user: signUpOptions.cibi,
      google_user: signUpOptions.google,
      fb_user: signUpOptions.fb,
      apple_user: signUpOptions.apple,
      address_list: this.parserAddressForSendToBackend(userOptions.addressList),
      preference_user_list: this.parserPreferenceForSendToBackend(userOptions.preferenceUserList),
      store_list: this.parserStorageListForSendToBackend(userOptions.nearbyStoreList),
      notification_list: this.parserNotificationListForSendToBackend(userOptions.notificationList)
    };
    console.log(data)
    return this.http.post<any>(url, data);
  }

  // parserToUser(data): User {
  //   const obj = new User();
  //   if (!data) return obj;
  //   obj.email = data.email;
  //   obj.id = data.id;
  //   obj.isActive = data.is_active;
  //   obj.lastName = data.last_name;
  //   obj.name = data.name;
  //   obj.phoneNumber = data.phone_number;
  //   obj.ruc = data.ruc;
  //   return obj;
  // }

  parserAddressForSendToBackend(list: Address[]) {
    let address: any[] = [];
    list.forEach(addr => {
      address.push({
        address: addr.address,
        name: addr.name,
        lat_lng: `${addr.lat_lng}`
      });
    });
    return address;
  }

  parserPreferenceForSendToBackend(list: PreferenceUserForNewAccount []) {
    let data: any[] = [];
    list.forEach(pref => {
      data.push({
        preference_category_id: +pref.preferenceCategoryId,
        preference_user_list: this.parserPreferenceUserListForSendToBackend(pref.preferenceUserList)
      });
    });
    return data;
  }

  parserPreferenceUserListForSendToBackend(list: Preference[]) {
    let preferences: any[] = [];
    list.forEach(p => {
      preferences.push({
        preference_id: p.id
      });
    });
    return preferences;
  }

  parserStorageListForSendToBackend(list: Store[]) {
    let stores: any[] = [];
    list.forEach(p => {
      stores.push({
        id: p.id
      });
    });
    return stores;
  }

  parserNotificationListForSendToBackend(list: Notifications[]) {
    let notifications: any[] = [];
    list.forEach(n => {
      notifications.push({
        notification_id: n.id,
        email: n.email,
        sms: n.sms,
        push: n.push
      });
    });
    return notifications;
  }

  parserToUser(data: any): User {
    const obj = new User();
    if (!data) { return obj; }
    obj.email = data.email;
    obj.id = data.id;
    obj.isActive = data.is_active;
    obj.lastName = data.last_name;
    obj.name = data.name;
    obj.phoneNumber = data.phone_number;
    obj.ruc = data.ruc;
    obj.dateOfBirth = data.date_of_birth;
    obj.gender = data.gender;
    obj.image = data.image;
    obj.cibi_user = data?.cibi_user;
    // obj.fbUser = data?.fb_user;
    // obj.googleUser = data?.google_user;
    // obj.appleUser = data?.apple_user;
    return obj;
  }

}
