import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ReportExpenses, ReportExpensesCategory } from '../models/report';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService extends BaseService {

  monthList = {
    JANUARY: { name: 'Ene', index: 0 },
    FEBRUARY: { name: 'Feb', index: 1 },
    MARCH: { name: 'Mar', index: 2 },
    APRIL: { name: 'Apr', index: 3 },
    MAY: { name: 'May', index: 4 },
    JUNE: { name: 'Jun', index: 5 },
    JULY: { name: 'Jul', index: 6 },
    AUGUST: { name: 'Ago', index: 7 },
    SEPTEMBER: { name: 'Sep', index: 8 },
    OCTOBER: { name: 'Oct', index: 9 },
    NOVEMBER: { name: 'Nov', index: 10 },
    DECEMBER: { name: 'Dic', index: 11 }
  };

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  getExpensesReport(userId: number): Observable<ReportExpenses[]> {
    return this.http.get<any>(`${this.apiUrl}/users/${userId}/expenses/report`)
      .pipe(
        map((list) => {
          return list.map((data: { owner_id: number; total: number; date: string | number | Date; month: string; }) => {
            const obj = new ReportExpenses();
            obj.ownerId = data.owner_id;
            obj.total = data.total;
            obj.date = new Date(data.date);
            obj.month = data.month.trim();
            return obj;
          });
        }));
  }

  getExpensesReportCategory(userId: number): Observable<ReportExpensesCategory[]> {
    return this.http.get<any>(`${this.apiUrl}/users/${userId}/expenses-category/report`)
      .pipe(
        map((list) => {
          return list.map((data: { owner_id: number; total: number; category: string; }) => {
            const obj = new ReportExpensesCategory();
            obj.ownerId = data.owner_id;
            obj.total = data.total;
            obj.category = data.category;
            return obj;
          });
        }));
  }

  updateAllBudget(userId: number, data: any) {
    return this.http.post<any>(`${this.apiUrl}/user/${userId}/user-budget/all`, data);
  }

  saveUSerBudget(userId: number, formData: any) {
    return this.http.post<any>(`${this.apiUrl}/user/${userId}/user-budget`, formData);
  }

  getUserBudgetById(userId: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/user/${userId}/user-budget`);
  }
}
