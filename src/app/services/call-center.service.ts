import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class CallCenterService extends BaseService {

  constructor(private http: HttpClient) {
    super();
  }

  getList(): Observable<any[]> {
    return this.http.get<any>(`${this.apiUrl}/call-center/`);
  }
}
