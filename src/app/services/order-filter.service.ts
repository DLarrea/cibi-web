import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { OrderHistoryFilter } from '../interfaces/orderHistoryFilter';
import { OrderFull } from '../models/cart';
import { isWithinInterval } from 'date-fns';
import { parseISO, isBefore } from 'date-fns';

@Injectable({
  providedIn: 'root'
})
export class OrderFilterService {

  private list: OrderFull[] = []
  private filteredList = new BehaviorSubject<OrderFull[]>([]);
  constructor() { }

  setFilter(filter: OrderHistoryFilter){
    const list = this.list.filter(item => {
      let inDateRange = true;
      let createAt = new Date();
      let startDate = null;
      let endDate = null;

      if(item && item.createdAt){
        createAt = new Date(item.createdAt.getFullYear(), item.createdAt.getMonth(), item.createdAt.getDate());
      }
      if(filter && filter.dateRange){
        startDate = filter.dateRange.start?.toDate();
        endDate = filter.dateRange.end?.toDate();
      }
      if(startDate && endDate){
        inDateRange = isWithinInterval(createAt,{
          start: startDate,
          end: endDate
        })
      }

      else if(startDate){
        inDateRange = isWithinInterval(createAt,{
          start: startDate,
          end: new Date()
        })
      }

      else if(endDate){
        inDateRange = isBefore(createAt, endDate)
      }

      const filterByStores = filter.storeList && filter.storeList.length>0
        ? filter.storeList.some(store => store.id === item.store?.id)
        : true
      const filterByTypes = filter.purchaseTypeList && filter.purchaseTypeList.length>0
        ? filter.purchaseTypeList.some(type => type === item.purchaseType)
        : true

      return inDateRange && filterByStores && filterByTypes
    })

    this.filteredList.next(list);
  }

  setList(list: OrderFull[]){
    this.list = list.slice();
  }

  getFilteredList(): Observable<OrderFull[]>{
    return this.filteredList.asObservable();
  }

}

