import { environment } from 'src/environments/environment';

export class BaseService {
  apiUrl = environment.api;

  constructor() { }
}
