import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CartInfo, CartItems, Contact, ItemProduct, UserCartInvitationData } from '../models/cart';
import { BaseService } from './base.service';
import { MediaItem, MediaResource } from '../models/media';
import { Product, ProductGroup } from '../models/product';

@Injectable({
    providedIn: 'root'
})
export class MediaService extends BaseService {

    private activeProductSubject = new BehaviorSubject<Product>(null!);
    activeProduct$: Observable<Product> = this.activeProductSubject.asObservable();
    baseUrl: string;

    constructor(private http: HttpClient) {
        super();
        this.baseUrl = `${this.apiUrl}/mediaresources`;
    }

    setActiveProductInVideo(product: Product) {
        this.activeProductSubject.next(product);
    }

    // recuperar carritos del usuario
    getByProductId(productId: number): Observable<MediaResource[]> {
        return this.http.get<any>(`${this.baseUrl}/by-product/${productId}`)
            .pipe(
                map((result) => {
                    return result.map((data: MediaResource) => {
                        let obj = new MediaResource();
                        obj = data;
                        return obj;
                    });
                }));
    }

    // recuperar carritos del usuario
    getMediaItems(resourceId: number): Observable<MediaItem[]> {
        return this.http.get<any>(`${this.baseUrl}/${resourceId}/items`)
            .pipe(
                map((result) => {
                    return result.map((mediaItem: any) => {
                        return this.parseToMediaItem(mediaItem);
                    });
                }));
    }
    
    getVideos(): Observable<any[]> {
        return this.http.get<any>(`${this.baseUrl}`);
    }

    getProductGroupsByMedia(mediaId: number): Observable<ProductGroup[]> {
        const url = `${this.baseUrl}/product-group/${mediaId}`;
        return this.http.get<any>(url)
            .pipe(
                map((list) => {
                    return list.map((productGroup: ProductGroup) => {
                        const subcategoryId = productGroup.subcategory_id;
                        const variantes = productGroup.variants;
                        delete productGroup.subcategory_id;
                        //   delete productGroup.variants;
                        let obj = new ProductGroup();
                        obj = productGroup;
                        obj.subcategoryId = subcategoryId;
                        obj.variants = this.parseToProduct(variantes);
                        obj.product = obj.variants[0];
                        return obj;
                    });
                }));
    }

    parseToProduct(list: any): Product[] {
        const productList: Product[] = [];
        list.forEach((data: Product) => {
            const subcategoryId = data.subcategory_id;
            const productGroupId = data.product_group_id;
            delete data.subcategory_id;
            delete data.product_group_id;
            let obj = new Product();
            obj = data;
            obj.qty = 1;
            obj.details = this.parseToDetails(obj.details);
            obj.productGroupId = productGroupId;
            obj.subcategoryId = subcategoryId;
            productList.push(data);
        });
        return productList;
    }

    parseToDetails(list: any[] | undefined) {
        if (list) {
            return list.map((valor: { description: string; }) => {
                if (valor && valor.description) { return JSON.parse(valor.description); }
            });
        }
        return [];
    }

    parseToMediaItem(data: { id: number; media_resource_id: number; product_id: number; time: number; product: any; }): MediaItem {
        const obj = new MediaItem();
        obj.id = data.id;
        obj.mediaResourceId = data.media_resource_id;
        obj.productId = data.product_id;
        obj.time = data.time;
        obj.product = this.parseToSingleProduct(data.product);
        return obj;
    }

    parseToSingleProduct(data: Product) {
        const subcategoryId = data.subcategory_id;
        const productGroupId = data.product_group_id;
        delete data.subcategory_id;
        delete data.product_group_id;
        let obj = new Product();
        obj = data;
        obj.qty = 1;
        //obj.details = this.parseToDetails(obj.details);
        obj.productGroupId = productGroupId;
        obj.subcategoryId = subcategoryId;
        return obj;
    }
}
