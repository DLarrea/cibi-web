import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class UserPhotoService extends BaseService {

    constructor(
        private http: HttpClient
    ) {
        super();
    }

    ImgUser(userId: number, formData: any): Observable<User> {
        const options = {
          chunkedMode: false,
          mimeType: 'multipart/form-data',
          headers: {
            Accept: 'application/json'
          }
        };
        const url = `${this.apiUrl}/users/${userId}/img`;
        return this.http.post<any>(url, formData, options);
      }

      me(): Observable<User> {
        const url = `${this.apiUrl}users/me/`;
        return this.http.get<any>(url)
          .pipe(
            map((user: any) => {
              return this.parserToUser(user);
          }));
      }

      parserToUser(data: any): User {
        const obj = new User();
        if (!data) { return obj; }
        obj.email = data.email;
        obj.id = data.id;
        obj.isActive = data.is_active;
        obj.lastName = data.last_name;
        obj.name = data.name;
        obj.phoneNumber = data.phone_number;
        obj.ruc = data.ruc;
        obj.image = data.image;
        obj.cibi_user = data?.cibi_user;
        return obj;
      }

}
