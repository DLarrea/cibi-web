import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Address } from '../models/address';
import { Notifications } from '../models/notifications';
import { PreferenceUserForNewAccount } from '../models/preferences';
import { Store } from '../models/store';
import { User, UserFormData } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  clearStorage: any;
  user: any;
  userBehaviorSubject = new BehaviorSubject<User>(null!);
  lastUserEmailLogged: string | undefined;

  private userFormData: UserFormData = new UserFormData();
  private addressListForNewAccount: Address[] = [];
  private nearbyStoreForNewAccount: Store[] = [];
  private preferenceUserForNewAccount: PreferenceUserForNewAccount[] = [];
  private notificationForNewAccount: Notifications[] = [];
  private checkout: any = null;
  private purchaseChecked: any = null;
  private storeChecked: any = null;


  constructor() {
    this.init();
  }

  init() {
    //localStorage.clear();
    this.userFormData = this.restoreUserFormForNewAccount();
    this.addressListForNewAccount = this.restoreAddressListForNewAccount();
    this.nearbyStoreForNewAccount = this.restoreNearbyStoreForNewAccount();
    this.preferenceUserForNewAccount = this.restorePreferenceForNewAccount();
    this.notificationForNewAccount = this.restoreNotificationForNewAccount();
    this.checkout = this.restoreCheckout();
    this.purchaseChecked = this.restorePurchaseChecked();
    this.storeChecked = this.restoreStoreChecked();
  }

  // RESTORES
  restoreUserFormForNewAccount() {
    let data = localStorage.getItem('userFormData');
    if (data) {
      return JSON.parse(data);
    }
    return new UserFormData();
  }

  restoreAddressListForNewAccount() {
    let data = localStorage.getItem('addressListForNewAccount');
    if (data) {
      return JSON.parse(data);
    }
    return [];
  }

  restoreNearbyStoreForNewAccount() {
    let data = localStorage.getItem('nearbyStoreForNewAccount');
    if (data) {
      return JSON.parse(data);
    }
    return [];
  }

  restorePreferenceForNewAccount() {
    let data = localStorage.getItem('preferenceUserForNewAccount');
    if (data) {
      return JSON.parse(data);
    }
    return [];
  }

  restoreNotificationForNewAccount() {
    let data = localStorage.getItem('notificationForNewAccount');
    if (data) {
      return JSON.parse(data);
    }
    return [];
  }

  restoreCheckout() {
    let data = localStorage.getItem('checkout');
    if (data) {
      return JSON.parse(data);
    }
    return null;
  }

  restorePurchaseChecked() {
    let data = localStorage.getItem('purchaseChecked');
    if (data) {
      return JSON.parse(data);
    }
    return null;
  }

  restoreStoreChecked() {
    let data = localStorage.getItem('storeChecked');
    if (data) {
      return JSON.parse(data);
    }
    return null;
  }


  // SETTERS
  public setUserFormData(data: UserFormData) {
    localStorage.setItem('userFormData', JSON.stringify(data));
    this.userFormData = data;
  }

  public setLastUserEmailLogged(lastUserEmailLogged: string) {
    localStorage.setItem('lastUserEmailLogged', JSON.stringify(lastUserEmailLogged));
    this.lastUserEmailLogged = lastUserEmailLogged;
  }

  public setUser(user: any) {
    localStorage.setItem('user', JSON.stringify(user));
    this.setLastUserEmailLogged(user.email)
    this.user = null;
    this.user = user;
    this.userBehaviorSubject.next(this.user);
  }

  public setAddresListForNewAccount(addresList: Address[]) {
    localStorage.setItem('addressListForNewAccount', JSON.stringify(addresList));
    this.addressListForNewAccount = addresList;
  }

  public setNearbyStoreForNewAccount(storeList: Store[]) {
    localStorage.setItem('nearbyStoreForNewAccount', JSON.stringify(storeList));
    this.nearbyStoreForNewAccount = storeList;
  }

  public setPreferenceUserForNewAccount(preferenceList: PreferenceUserForNewAccount[]) {
    localStorage.setItem('preferenceUserForNewAccount', JSON.stringify(preferenceList));
    this.preferenceUserForNewAccount = preferenceList;
  }

  public setCheckoutInfo(checkout: any) {
    localStorage.setItem('checkout', JSON.stringify(checkout));
    this.checkout = checkout;
  }


  public setPurchaseChecked(purchaseChecked: any) {
    localStorage.setItem('checkout', JSON.stringify(purchaseChecked));
    this.purchaseChecked = purchaseChecked;
  }

  public setStoreChecked(storeChecked: any) {
    localStorage.setItem('checkout', JSON.stringify(storeChecked));
    this.storeChecked = storeChecked;
  }

  public setNotificationForNewAccount(notificationList: Notifications[]) {
    localStorage.setItem('notificationForNewAccount', JSON.stringify(notificationList));
    this.notificationForNewAccount = notificationList;
  }

  // GETTERS
  getUserFormData() {
    return this.userFormData ? this.userFormData : new UserFormData();
  }

  getAddressListForNewAccountData() {
    return this.addressListForNewAccount.length > 0 ? this.addressListForNewAccount : [];
  }

  getNearbyStoreForNewAccountData() {
    return this.nearbyStoreForNewAccount.length > 0 ? this.nearbyStoreForNewAccount : [];
  }

  getPreferenceUserForNewAccountData() {
    return this.preferenceUserForNewAccount.length > 0 ? this.preferenceUserForNewAccount : [];
  }

  getNotificationForNewAccountData() {
    return this.notificationForNewAccount.length > 0 ? this.notificationForNewAccount : [];
  }

  getCheckout(){
    return this.checkout;
  }

  getStoreCheckedData() {
    return this.storeChecked;
  }

  getPurchaseCheckedData() {
    return this.purchaseChecked;
  }

  getUserData(): Promise<any> {
    return localStorage.get('user');
  }

}
