import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Authentication } from '../interfaces/authentication';
import { User } from '../interfaces/user';
import { AUTH_KEY, TOKEN_KEY } from '../constant';

const authItem = localStorage.getItem(AUTH_KEY);

@Injectable({ providedIn: 'root' })
export class AuthService {
  private baseUrl = `${environment.api}`;
  private authenticationSubject = new BehaviorSubject<Authentication | undefined>(JSON.parse(authItem ? authItem : '{}'));

  constructor(private router: Router, private http: HttpClient) { }

  get authentication(): Observable<Authentication | undefined> {
    return this.authenticationSubject.asObservable();
  }

  get authenticationValue(): Authentication | undefined {
    const isEmpty = this.authenticationSubject.value ? !Object.keys(this.authenticationSubject.value).length : true;
    return isEmpty ? undefined : this.authenticationSubject.value;
  }

  get token(): string | undefined {
    return this.authenticationValue?.access_token;
  }

  get isAuthenticated(): boolean {
    return !!this.authenticationValue;
  }

  getLoggedUserObservable(): Observable<User | undefined> {
    return this.authentication.pipe(map(auth => auth?.user));
  }

  login(username: string, password: string): Observable<Authentication> {
    const formData = new FormData();
    formData.append('username', username);
    formData.append('password', password);
    return this.http.post<Authentication>(`${this.baseUrl}/login`, formData)
      .pipe(map(auth => {
        localStorage.setItem(AUTH_KEY, JSON.stringify(auth));
        localStorage.setItem(TOKEN_KEY, auth.access_token);
        this.authenticationSubject.next(auth);
        return auth;
      }));
  }

  logout(): void {
    localStorage.removeItem(AUTH_KEY);
    localStorage.removeItem(TOKEN_KEY);
    localStorage.clear();
    this.authenticationSubject.next(undefined);
    this.router.navigate(['/']);
  }

  loggedUser(): User | undefined {
    return this.authenticationValue?.user;
  }
}
