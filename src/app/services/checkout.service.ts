import { Injectable } from '@angular/core';
import { GlobalService } from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  checkout = {
    purchaseType: null,     // delivery | pickup
    deliveryAddress: null,  // address
    deliveryTime: null,
    billingInfo: null,      // billingInfo
    storeAddress: null,
    paymentMethod: null,
    userPaymentMethod: null
  };

  constructor(private storage: GlobalService) {
    // this.storage.getCheckout().then((resp: any) => {
    //   if (resp) {
    //     this.checkout = JSON.parse(resp);
    //   }
    // });
  }

  clear() {
    this.checkout = {
      purchaseType: null,     // delivery | pickup
      deliveryAddress: null,  // address
      deliveryTime: null,
      billingInfo: null,      // billingInfo
      storeAddress: null,
      paymentMethod: null,
      userPaymentMethod: null
    };
  }

  updateCheckout() {
    this.storage.setCheckoutInfo(this.checkout);
  }

  //
  public setPurchaseTypeOption(option: any) {
    this.checkout.purchaseType = option;
    this.updateCheckout();
  }

  getPurchaseOption() {
    return this.checkout.purchaseType;
  }

  //
  setDeliveryAddressOption(option: any) {
    this.checkout.deliveryAddress = option;
    this.updateCheckout();
  }

  getDeliveryAddressOption() {
    return this.checkout.deliveryAddress;
  }

  //
  setDeliveryTimeOption(option: any) {
    this.checkout.deliveryTime = option;
    this.updateCheckout();
  }

  getDeliveryTimeOption() {
    return this.checkout.deliveryTime;
  }

  //
  setBillingInfoOption(option: any) {
    this.checkout.billingInfo = option;
    this.updateCheckout();
  }

  getBillingInfoOption() {
    return this.checkout?.billingInfo;
  }

  //
  setStoreAddressOption(option: any) {
    this.checkout.storeAddress = option;
    this.updateCheckout();
  }

  getStoreAddressOption() {
    return this.checkout.storeAddress;
  }

  //
  setPaymentMethodOption(option: any) {
    this.checkout.paymentMethod = option;
    this.updateCheckout();
  }

  getPaymentMethodOption() {
    return this.checkout.paymentMethod;
  }

  //
  setUserPaymentMethodOption(option: any) {
    this.checkout.userPaymentMethod = option;
    this.updateCheckout();
  }

  getUserPaymentMethodOption() {
    return this.checkout.userPaymentMethod;
  }
}

