import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  
  loadingCtrl: any;
  loadingCont!: null;


  public isLoading = new BehaviorSubject<boolean>(false);

  constructor() {}

  getLoadingObservable(): Observable<boolean> {
    return this.isLoading.asObservable();
  }


  async presentLoading(idLoading = 'idDefault') {
     this.loadingCtrl.dismiss(null, null, idLoading).then(() => {
      this.loadingCont = null;
      // console.log('cerrando dissmiss', idLoading);
    });
  }

  dismissLoading(idLoading = 'idDefault') {
    this.loadingCtrl.dismiss(null, null, idLoading).then(() => {
      this.loadingCont = null;
      // console.log('cerrando dissmiss', idLoading);
    });
  }

   presentToastMsg(idLoading = 'idDefault') {
     this.loadingCtrl.dismiss(null, null, idLoading).then(() => {
      this.loadingCont = null;
      // console.log('cerrando dissmiss', idLoading);
    });
  }
}
