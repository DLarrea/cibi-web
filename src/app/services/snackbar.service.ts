import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Snackbar } from '../interfaces/snackbar';
import { SnackbarComponent } from '../shared/snackbar/snackbar.component';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  openSnackBar(data: Snackbar): void {
    this.snackBar.openFromComponent(SnackbarComponent,{
      verticalPosition: data.position,
      duration: data.duration,
      data,
      panelClass: `${data.className}-${data.classAction}`
    });
  }
}
