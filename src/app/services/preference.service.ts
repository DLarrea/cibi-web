import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Preference, PreferenceCategory } from '../models/preferences'; 
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class PreferenceService extends BaseService {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  /**
   * @returns listado de preferencias-category
   */
  getPreferenceCategory(): Observable<PreferenceCategory[]> {
    const url = `${this.apiUrl}/preference-category/`;
    return this.http.get<any>(url).pipe(
      map((list) => {
        return list.map((preference: any) => {
          return this.parserToPreferenceCategory(preference);
        });
      })
    );
  }
 
  /**
   * @returns listado de preferencias por preference-category
   */
  getPreferenceByPreferenceCatarogyId(preferenceCategoryId: number): Observable<Preference[]> {
    const url = `${this.apiUrl}/preference/?preference_category_id=${preferenceCategoryId}`;
    return this.http.get<any>(url)
    .pipe(
      map((list) => {
        return list.map((preference: any) => {
          return this.parserToPreference(preference);
        });
      })
    );
  }

  private parserToPreferenceCategory(data: any): PreferenceCategory {
    const obj = new PreferenceCategory();
    obj.id = data.id;
    obj.name = data.name;
    obj.description = data.description;
    obj.isActive = data.is_active;
    return obj;
  }

  private parserToPreference(data: any): Preference {
    const obj = new Preference();
    obj.id = data.id;
    obj.name = data.name;
    obj.description = data.description;
    obj.preferenceCategoryId = data.preference_category_id;
    obj.isActive = data.is_active;
    obj.checked = false;
    return obj;
  }

}
