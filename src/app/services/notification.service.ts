import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Notifications } from '../models/notifications';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  baseUrl: string;
  constructor(private http: HttpClient
  ) {
    const apiUrl = environment.api || 'http://localhost:8001';
    this.baseUrl = `${apiUrl}/notification`;
  }

  getListNotification(): Observable<Notifications[]> {
    const url = `${this.baseUrl}/full`;
    return this.http.get<any>(url)
    .pipe(
      map((notificationList) => {
        return notificationList.map((notification: Notifications) => {
          let obj = new Notifications();
          obj = notification;
          obj.id = notification.id;
          obj.active = notification.active;
          obj.checked = false;
          obj.classification = notification.classification;
          obj.email = true;
          obj.sms = true;
          obj.push = true;
          return obj;
        });
    }));
  }

}
