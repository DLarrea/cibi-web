import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CartInfo, CartItems, Contact, ItemProduct, UserCartInvitationData } from '../models/cart';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private baseUrl = `${environment.api}/carts`;

  private cartActiveSubject: BehaviorSubject<CartInfo> = new BehaviorSubject<CartInfo>({});

  constructor(private http: HttpClient) { }

  get cartActive(): Observable<CartInfo> {
    return this.cartActiveSubject.asObservable();
  }
  
  getcartActiveObservable(): Observable<CartInfo> {
    return this.cartActive.pipe(map(r => r));
  }

  setCartActive$(ownerId: number): void {
    this.getActiveCart(ownerId).subscribe(
      resp => {
        const obj = this.parseToCartInUse(resp)
        console.log(obj)
        this.cartActiveSubject.next(obj);
      }, error => {
        this.getCartsByUserID(ownerId).subscribe((results: CartInfo[]) => {
          const cartInfo = results.reduce((prev: any, curr: any) => {
            if (prev && prev.id && curr && curr.id)
            return prev.id < curr.id ? prev : curr;
          });
          console.log(cartInfo);
          if (cartInfo && cartInfo.id) {
            const obj = this.parseToCartInUse(cartInfo)
            if(obj.id){
              this.cartActiveSubject.next(obj);
              this.setCartInUse(obj.id).subscribe(res => {
                console.log(res);
              });
            }
          }
        })
      }
    );
  }

  parseToCartInUse(data: any): CartInfo {
    const cartItems = data.cartItems;
    delete data.cartItems;
    const obj = new CartInfo();
    obj.id = data.id;
    obj.active = data.active;
    obj.cartItems = this.parseToCartItems(cartItems);
    obj.ownerId = data.owner_id;
    obj.color = data.color;
    obj.name = data.name;
    obj.showIconLeft = data.default ? false : true;
    obj.showIconRight = false;
    obj.default = data.default;
    obj.inUse = data.in_use;
    return obj;
  }

  parseToCartInfo(data: any): CartInfo {
    const obj = new CartInfo();
    obj.id = data.id;
    obj.active = data.active;
    obj.cartItems = this.parseToCartItems(data.cart_items);
    obj.ownerId = data.owner_id;
    obj.color = data.color;
    obj.name = data.name;
    obj.showIconLeft = data.default ? false : true;
    obj.showIconRight = false;
    obj.default = data.default;
    obj.inUse = data.in_use;
    return obj;
  }

  // marcar Carrito en uso
  setCartInUse(cartId: number): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/${cartId}/in_use`, {});
  }

  getActiveCart(ownerId: number){
    const url = `${environment.api}/cart/${ownerId}/in_use/`;
    return this.http.get<any>(url)
    .pipe(
      map(data => {
        const cartItems = data.cart_items;
        delete data.cart_items;
        let obj = new CartInfo();
        obj = data;
        obj.cartItems = this.parseToCartItems(cartItems);

        this.cartActiveSubject.next(obj);
        return obj;
      })
    );
  }

  getList(): Observable<CartInfo[]> {
    const url = `${this.baseUrl}/`;
    return this.http.get<any>(url);
  }

  addcart(data: any) {
    const url = `${this.baseUrl}/`;
    return this.http.post<any>(url, data);
  }

  getCart(id: number): Observable<CartInfo> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<any>(url)
      .pipe(map(data => {
        const obj = data as CartInfo;
        obj.ownerId = data.owner_id;
        return obj;
      }));
  }

  addItem(data: any): Observable<any> {
    const url = `${this.baseUrl}/item/`;
    return this.http.post<any>(url, data);
  }

  addBulkItems(data: any, cartId: number): Observable<any> {
    const url = `${this.baseUrl}/${cartId}/items/`;
    return this.http.post<any>(url, data);
  }

  deleteCart(cartId: number) {
    const url = `${this.baseUrl}/${cartId}`;
    return this.http.delete<any>(url);
  }

  getCartItems(cartId: number): Observable<CartItems[]> {
    const url = `${this.baseUrl}/${cartId}/items`;
    return this.http.get<any>(url)
      .pipe(
        map((list) => {
          return list.map((item: any) => {
            const obj = new CartItems();
            obj.id = item.id;
            obj.cartID = item.cart_id;
            obj.cartItems = this.parseToItemProduct(item.cart_items);
            obj.chooseItem = item.choose_item;
            obj.productId = item.product_id;
            obj.qty = item.qty;
            return obj;
          });
        }));
  }

  deleteItemsCart(items: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: {
        items
      }
    };
    return this.http.delete<any>(`${this.baseUrl}/items/list`, options);
  }

  deleteItemCart(itemId: number): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.delete<any>(`${this.baseUrl}/item/${itemId}`, options);
  }

  deleteCartItems(cartId: number){
    return this.http.delete<any>(`${this.baseUrl}/${cartId}/items/`);
  }

  // getCartById(id: number) {
  //   return cartList[0];
  // }

  updateQuantityProduct(cartItemId: number, qty: number) {
    return this.http.put<any>(`${this.baseUrl}/item/${cartItemId}/quantity/${qty}`, {});
  }

  getItemsIds(list: any) {
    return list.map((p: any) => p.id);
  }

  editCart(form: any, cartId: number): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/${cartId}/`, form);
  }

  // recuperar carritos del usuario
  getCartsByUserID(userId: number): Observable<CartInfo[]> {
    return this.http.get<any>(`${this.baseUrl}/user/${userId}`)
      .pipe(
        map((list) => {
          return list.map((cart: any) => {
            return this.parseToCartInfo(cart);
          });
        }));
  }

  // aceptar invitacion al carrito
  confirmIvtitationToCart(userId: number, cartId: number): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/${cartId}/user/${userId}`, {});
  }

  // aprobar solicitud de integrante
  acceptUserRequestToCart(userId: number, cartId: number): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/${cartId}/user/${userId}/accept`, {});
  }

  // aprobar solicitud de integrante
  rejectUserRequestToCart(userId: number, cartId: number): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/${cartId}/user/${userId}/reject`, {});
  }

  // eliminar integrante del carrito logicamente (solo marca como reject el estado)
  deleteIntegrante(userId: number, cartId: number): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/${cartId}/user/${userId}`);
  }

  // recuperar datos del usuario que envia la invitacion
  getUserInviteData(cartId: number): Observable<UserCartInvitationData> {
    return this.http.get<any>(`${this.baseUrl}/${cartId}/invite-data`)
      .pipe(
        map(data => {
          const obj = new UserCartInvitationData();
          obj.avatar = data.avatar;
          obj.cartId = data.cart_id;
          obj.fromUser = data.from_user;
          obj.name = data.name;
          return obj;
        })
      );
  }

  // listar integrantes de un carrito
  getUserToCarts(cartId: number): Observable<Contact[]> {
    return this.http.get<any>(`${this.baseUrl}/${cartId}/users`)
      .pipe(
        map((list) => {
          return list.map((users: any) => {
            return this.parseUserContactCart(users);
          });
        }));
  }

  parseToUsersCart(list: any): Contact[] {
    return list.map((user: any) => {
      return this.parseUserContactCart(user);
    });
  }

  parseUserContactCart(data: any): Contact {
    const obj = new Contact();
    obj.avatar = data.avatar ? data.avatar : null;
    obj.name = data.name;
    obj.isOwner = data.is_owner;
    obj.status = data.status;
    obj.userId = data.user_id;
    obj.pendiente = data.status === 'PENDING' ? true : false;
    obj.role = data.is_owner ? 'administrador' : 'invitado';
    return obj;
  }

  // parser Respuesta a CartItems
  parseToCartItem(data: any): CartItems {
    const obj = new CartItems();
    obj.id = data.id;
    obj.cartID = data.cart_id;
    obj.cartItems = data.cart_items;
    obj.chooseItem = data.choose_item;
    obj.productId = data.product_id;
    obj.qty = data.qty;
    obj.checked = false;
    return obj;
  }

  parseToCartItems(list: any): CartItems[] {
    return list.map((item: any) => {
      return this.parseToCartItem(item);
    });
  }

  parseToItemProduct(data: any): ItemProduct {
    const subcategoryId = data.subcategory_id;
    const productGroupId = data.product_group_id;
    delete data.subcategory_id;
    delete data.product_group_id;
    let obj = new ItemProduct();
    obj = data;
    obj.subcategoryId = subcategoryId;
    obj.productGroupId = productGroupId;
    return obj;
  }

  verificarArticulosAgotados(list: any): boolean {
    let existeAgotados = false;
    const status = 'out_of_stock';
    list.forEach((element: any) => {
      if (element.cartItems.status === status) {
        existeAgotados = true;
      }
    });
    return existeAgotados;
  }
}
