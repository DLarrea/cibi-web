import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Advertising } from '../models/advertising';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class AdvertisingService extends BaseService {

  constructor(private http: HttpClient) {
    super(); 
  }

  getList(): Observable<Advertising[]> {
    return this.http.get<any>(`${this.apiUrl}/advertising/`).pipe(
      map((advertisingList) => {
        return advertisingList.map((ad:any) => {
          return new Advertising(ad);
        }) 
      })
    );
  }
}
