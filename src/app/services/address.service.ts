import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Address } from '../models/address';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class AddressService extends BaseService {

  constructor(private http: HttpClient) {
    super();
  }

  create(userId: number, formData: any): Observable<Address> {
    return this.http.post<any>(`${this.apiUrl}/users/${userId}/address/`, formData);
  }

  getList(userId: number): Observable<Address[]> {
    return this.http.get<any>(`${this.apiUrl}/users/${userId}/address/`)
      .pipe(
        map((addressList) => {
          return addressList.map((address: any) => {
            const obj = new Address();
            obj.id = address.id;
            obj.isActive = address.is_active;
            obj.name = address.name;
            obj.address = address.address;
            obj.userId = address.user_id;
            obj.lat_lng = address.lat_lng;
            obj.checked = false;
            return obj;
          });
        }));
  }

  edit(userId: number, addressId: number, formData: any): Observable<Address> {
    return this.http.put<any>(`${this.apiUrl}/users/${userId}/address/${addressId}`, formData);
  }

  delete(userId: number, addressId: number): Observable<Address> {
    return this.http.delete<any>(`${this.apiUrl}/users/${userId}/address/${addressId}`);
  }
}
