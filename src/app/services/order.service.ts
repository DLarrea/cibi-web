import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BillingInfo } from '../models/billing-info';
import { AddressUser, Cart, Item, Order, OrderFull, UserPaymentMethod } from '../models/cart';
import { Store } from '../models/store';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private baseUrl = `${environment.api}`;

  constructor(private http: HttpClient) { }

  addOrder(data: any) {
    const url = `${this.baseUrl}/orders/`;
    return this.http.post<any>(url, data);
  }

  addBulkOrder(data: any, orderId: number) {
    const url = `${this.baseUrl}/orders/${orderId}/items/`;
    return this.http.post<any>(url, data);
  }

  addItem(id: number, data: any) {
    const url = `${this.baseUrl}/orders/${id}/item`;
    return this.http.post<any>(url, data);
  }

  confirmOrder(data: any) {
    const url = `${this.baseUrl}/orders/${data.id}/confirm`;
    return this.http.post<any>(url, data);
  }

  getByUserId(userId: number): Observable<Order[]> {
    const url = `${this.baseUrl}/users/${userId}/orders`;
    return this.http.get<any>(url)
      .pipe(
        map((orderList) => {
          return orderList.map((order: any) => {
            const obj = new Order();
            obj.active = order.active;
            obj.address = 'Avda Fernando de la Mora'; // order.address_id;
            obj.billingInformationId = order.billing_information_id;
            obj.cartId = order.cart_id;
            obj.createdAt = new Date(order.created_at);
            obj.id = order.id;
            obj.ownerId = order.owner_id;
            obj.purchaseType = order.purchase_type;
            obj.status = order.status;
            obj.storeId = order.store_id;
            obj.totalAmount = order.total_amount;
            obj.userPaymentMethodId = order.user_payment_method_id;
            return obj;
          });
        }));
  }

  updateQuantityProduct(cartId: number, data: any) {
    return this.http.put<any>(`${this.baseUrl}/orders/item-by-cart/${cartId}/quantity`, data);
  }

  getOrdersByOwnerId(ownerId: number): Observable<OrderFull[]> {
    const url = `${this.baseUrl}/orders/?owner_id=${ownerId}`;
    return this.http.get<any>(url)
      .pipe(
        map((list) => {
          return list.map((order: any) => {
            return this.parserToOrderFull(order);
          });
        }));
  }

  // eliminar ordenes del carrito por listado
  deleteItemsCart(cartId: number, productIds: any[]): Observable<any> {
    const url = `${this.baseUrl}/orders/items/cart/${cartId}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: {
        items: productIds
      }
    };
    return this.http.delete<any>(url, options);
  }

  getOrdersHistory(ownerId: number, status: string): Observable<OrderFull[]> {
    const url = `${this.baseUrl}/orders/?owner_id=${ownerId}&status=${status}`;
    return this.http.get<any>(url)
      .pipe(
        map((list) => {
          return list.map((order: any) => {
            return this.parserToOrderFull(order);
          });
        }));
  }

  getOrdersHistoryByCart(ownerId: number, status: string, cartId: number): Observable<OrderFull[]> {
    const url = `${this.baseUrl}/orders/?owner_id=${ownerId}&status=${status}&cart_id=${cartId}`;
    return this.http.get<any>(url)
      .pipe(
        map((list) => {
          return list.map((order: any) => {
            return this.parserToOrderFull(order);
          });
        }));
  }


  getActiveOrderByCart(cartId: number): Observable<OrderFull | null> {
    const url = `${this.baseUrl}/orders/active-order/${cartId}`;
    return this.http.get<any>(url).pipe(
      map((order: any) => {
        return this.parserToOrderFull(order);
      })
    );
  }

  getOrderById (orderId: number): Observable<OrderFull | null> {
    const url = `${this.baseUrl}/orders/${orderId}`;
    return this.http.get<any>(url).pipe(
      map((order: any) => {
        return this.parserToOrderFull(order);
      })
    );
  }

  // parser order full
  parserToOrderFull(order: any) {
    const obj = new OrderFull();
    if (!order) { return null; }
    obj.id = order.id;
    obj.createdAt = new Date(order.created_at);
    obj.billingInformationId = order.billing_information_id;
    obj.address = order.address_id ? order.address_id : null;
    obj.cartId = order.cart_id;
    obj.ownerId = order.owner_id;
    obj.purchaseType = order.purchase_type;
    obj.status = order.status;
    obj.storeId = order.store_id;
    obj.totalAmount = order.total_amount;
    obj.userPaymentMethodId = order.user_payment_method_id;
    obj.addressUser = this.parseToAddressUser(order.address_user);
    obj.billingInfo = this.parseToBillingInfo(order.billing_info);
    obj.cart = this.parseToCart(order.cart);
    obj.items = this.parseToItems(order.items);
    obj.owner = this.parseToUser(order.owner);
    obj.store = this.parseToStore(order.store);
    obj.userPaymentMethod = this.parseToUserPaymentMethod(order.user_payment_method);
    return obj;
  }

  // parseando objetos
  parseToAddressUser(data: any): AddressUser {
    const obj = new AddressUser();
    if (!data) { return obj; }
    obj.id = data.id;
    obj.address = data.address;
    obj.isActive = data.is_active;
    obj.latLng = data.lat_lng ? data.lat_lng : null;
    obj.name = data.name;
    obj.userId = data.user_id;
    return obj;
  }

  parseToBillingInfo(data: any): BillingInfo {
    const obj = new BillingInfo();
    if (!data) { return obj; }
    obj.id = data.id;
    obj.address = data.address ? data.address : null;
    obj.isActive = data.is_active;
    obj.razonSocial = data.razon_social;
    obj.ruc = data.ruc;
    obj.phone = data.phone_number ? data.phone_number : null;
    obj.userId = data.user_id;
    return obj;
  }

  parseToCart(data: any): Cart {
    const obj = new Cart();
    if (!data) { return obj; }
    obj.id = data.id;
    obj.color = data.color;
    obj.name = data.name;
    obj.ownerId = data.owner_id;
    return obj;
  }

  parseToItems(listItems: any): Item[] {
    if (!listItems) { return []; }
    return listItems.map((item:any) => {
      const obj = new Item();
      obj.id = item.id;
      obj.price = item.price;
      obj.productId = item.product_id;
      obj.quantity = item.quantity;
      obj.status = item.status;
      obj.qty = item.qty;
      obj.chooseItem = item.choose_item;
      obj.product = item.product;
      return obj;
    });
  }

  parseToUser(data: any): User {
    const obj = new User();
    if (!data) { return obj; }
    obj.email = data.email;
    obj.id = data.id;
    obj.isActive = data.is_active;
    obj.lastName = data.last_name;
    obj.name = data.name;
    obj.phoneNumber = data.phone_number;
    obj.ruc = data.ruc;
    return obj;
  }

  parseToUserPaymentMethod(data: any): UserPaymentMethod {
    const obj = new UserPaymentMethod();
    if (!data) { return obj; }
    obj.id = data.id;
    obj.isActive = data.is_active;
    obj.paymentMethodId = data.payment_method_id;
    obj.userId = data.user_id;
    return obj;
  }

  parseToStore(data: any): Store {
    const obj = new Store();
    if (!data) { return obj; }
    obj.address = data.address;
    obj.id = data.id;
    obj.name = data.name;
    obj.type = data.type;
    return obj;
  }
}
