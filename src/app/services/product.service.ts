import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Filters, PopularProduct, Product, ProductGroup, ProductSubCategories } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private baseUrl = `${environment.api}`;

  constructor(
    private http: HttpClient
  ) { }

  getPopularProducts(limit:number, skip: number): Observable<any[]> {
    return this.http.get<any>(`${this.baseUrl}/products/popular?&skip=${skip}&limit=${limit}`);
  }

  getSubCategories(): Observable<ProductSubCategories[]> {
    const url = `${this.baseUrl}/subcategory/full`;
    return this.http.get<any[]>(url).pipe(
      map(list => {
        return list.map(
          categ => {
            let obj: ProductSubCategories = new ProductSubCategories();
            obj = categ;
            obj.displayName = categ.description;
            obj.disabled = false;
            return obj;
          }
        )
      })
    );
  }

  getProductsPaginated( limit:number, skip: number, subCategory?:number, search?: string): Observable<any[]> {
    
    let url = `${this.baseUrl}/products/active?skip=${skip}&limit=${limit}`;
    if(subCategory && search) {
      url = `${this.baseUrl}/products/subcategory/${subCategory}?skip=${skip}&limit=${limit}&search=${search}`;
    } else if(subCategory){
      url = `${this.baseUrl}/products/subcategory/${subCategory}?skip=${skip}&limit=${limit}`;
    } else if(search){
      url = `${this.baseUrl}/products?skip=${skip}&limit=${limit}&search=${search}`;
    }

    return this.http.get<any>(url);

  }

  getKeyWords(): Observable<any[]> {
    return this.http.get<any>(`${this.baseUrl}/products/keyword?&skip=0&limit=1000`);
  }

  //// Revisar desde aca
  getProducts(): Observable<Product[]> {
    return this.http.get<any>(`${this.baseUrl}/products/`).pipe(
      map(
        list => {
          return list.map(
            (data: any) => {
              let obj = new Product();
              obj = this.parserToProduct(data);
              return obj;
            }
          )
        }
      )
    );
  }


  getProductsByUserPreference(user:number): Observable<any[]> {
    return this.http.get<any>(`${this.baseUrl}/user/${user}/user-preference/`);
  }
  
  getProductsOffers(): Observable<Product[]> {
    const url = `${this.baseUrl}/offer-product/`;
    return this.http.get<any>(url).pipe(
      map(
        list => {
          return list.map(
            (data: any) => {
              let obj = new Product();
              obj = this.parserToProduct(data);
              return obj;
            }
          )
        }
      )
    );
  }

  getAllPopularProducts(filter: string): Observable<any[]> {
    let url;

    if(filter.length > 2) {
      url = `${this.baseUrl}/products/popular?filter=${filter}&skip=0&limit=100`;
      return this.http.get<any>(url);
    } else {
      url = `${this.baseUrl}/products/popular?&skip=0&limit=100`;
      return this.http.get<any>(url).pipe(
        map(
          list => {
            return list.map(
              (data: any) => {
                const obj = new PopularProduct();
                obj.product = this.parserToProduct(data.Product);
                obj.total = data.total;
                return obj;
              },
            )
          }
        )
      );
    }
  }



  // Productos por grupos
  getProductGroup(): Observable<ProductGroup[]> {
    const url = `${this.baseUrl}/product-group/`;
    return this.http.get<any>(url)
    .pipe(
      map((list) => {
        return list.map((productGroup: any) => {
          let obj = new ProductGroup();
          if (productGroup && productGroup.variants.length > 0) {
            const subcategoryId = productGroup.subcategory_id;
            const variantes = productGroup.variants;
            delete productGroup.subcategory_id;
            delete productGroup.variants;
            obj = productGroup;
            obj.subcategoryId = subcategoryId;
            obj.variants = this.parseToProduct(variantes);
            obj.product = obj.variants[0];
          }
          return obj;
        });
      }));
  }
  
  parseToProduct(list: any): Product[] {
    const productList: Product[] = [];
    list.forEach((data: any)=> {
      const subcategoryId = data.subcategory_id;
      const productGroupId = data.product_group_id;
      const shortName = data?.short_name ? data?.short_name : null;
      delete data.subcategory_id;
      delete data.product_group_id;
      delete data.short_name;
      let obj = new Product();
      obj = data;
      obj.qty = 0;
      obj.existInCart = false;
      obj.details = this.parseToDetails(obj.details);
      obj.productGroupId = productGroupId;
      obj.subcategoryId = subcategoryId;
      obj.shortName = shortName;
      productList.push(data);
    });
    return productList;
  }

  parseToDetails(list: any) {
    if (list) {
      return list.map((valor: any) => {
        if (valor && valor.description) { return JSON.parse(valor.description); }
      });
    }
  }

  // Erase variables with "_", needs to be camelCase
  parserToProduct(data: any): Product {
    let obj = new Product();
    data.subcategoryId = data.subcategory_id;
    data.productGroupId = data.product_group_id;
    delete data.subcategory_id;
    delete data.product_group_id;
    obj = data;
    obj.inCart = false;
    obj.qty = 0;
    obj.cartItemId = 0;
    obj.details = this.parseToDetails(obj.details);
    return obj;
  }

  // ###### By subcategory #######
  getProductsBySubCategory(id: number| undefined, filter: string, skip: number, limit: number): Observable<Product[]> {
    let url
    if(filter.length >=3) {
      url = `${this.baseUrl}/products/subcategory/${id}?filter=${filter}&skip=0&limit=100`;
      return this.http.get<any>(url);
    } else {
      url = `${this.baseUrl}/products/subcategory/${id}?&skip=${skip}&limit=${limit}`;
      return this.http.get<any>(url);
    }
  }

  getPopularProductsBySubCategory(id: number | undefined, filter: string): Observable<any[]> {
    let url

    if(filter.length >= 3) {
      url = `${this.baseUrl}/products/subcategory/${id}/popular?filter=${filter}&skip=0&limit=100`;
      return this.http.get<any>(url).pipe(
        map(
          list => {
            return list.map(
              (data: any) => {
                const obj = new PopularProduct();
                obj.product = this.parserToProduct(data.Product);
                obj.total = data.total;
                return obj;
              },
            )
          }
        )
      );
    } else {
      url = `${this.baseUrl}/products/subcategory/${id}/popular?skip=0&limit=100`;
      return this.http.get<any>(url).pipe(
        map(
          list => {
            return list.map(
              (data: any) => {
                const obj = new PopularProduct();
                obj.product = this.parserToProduct(data.Product);
                obj.total = data.total;
                return obj;
              },
            )
          }
        )
      );
    }
  }

  getMoreProducts(id: number, skip: number, limit: number): Observable<Product[]> {
    const url = `${this.baseUrl}/products/subcategory/${id}?skip=${skip}&limit=${limit}`;
    return this.http.get<any>(url);
  }
  // getProductsBySubCategory(id: number | undefined): Observable<Product[]> {
  //   const url = `${this.baseUrl}/products/subcategory/${id}`;
  //   return this.http.get<any>(url);
  // }

  getBrandsAll(): Observable<Product[]> {
    const url = `${this.baseUrl}/products/brands/all`;
    return this.http.get<any>(url).pipe(
      map(
        list => {
          return list.map(
            (data: any) => {
              let obj = new Product();
              obj = this.parserToProduct(data);
              obj.checked = false;
              return obj;
            },
          )
        }
      )
    );
  }

  filterBrandPrice(data: Filters): Observable<Product[]> {
    const url = `${this.baseUrl}/products/filter/brand_price`;
    return this.http.post<any>(url, data);
  }

  // ###### Search Service ######
  search(term: string): Observable<Product[]> {
    // console.log('buscando : ', term);
    return this.http.get<Product[]>(`${this.baseUrl}/products?filter=${term}`);
    /* return of(productListSlide)
      .pipe(
        map(products => products.filter(product => {
            if ( product.name.toLocaleLowerCase().indexOf(term.toLocaleLowerCase()) >= 0 ) {
              return product;
            }
          }))
      ); */
  }
}
