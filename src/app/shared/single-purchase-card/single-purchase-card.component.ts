import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProductDetailsComponent } from 'src/app/dialog/product-details/product-details.component';
import { CartInfo, Order, ProductToDelete } from 'src/app/models/cart';
import { Product } from 'src/app/models/product';
import { CartService } from 'src/app/services/cart.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-single-purchase-card',
  templateUrl: './single-purchase-card.component.html',
  styleUrls: ['./single-purchase-card.component.scss']
})
export class SinglePurchaseCardComponent implements
  OnInit {
  imageApi = environment.image_api;

  cartInfo: CartInfo = new CartInfo;
  cartOrder: Order = new Order;

  productToAdd: Product = new Product();
  productToDelete: ProductToDelete = {
    p: new Product(),
    toDelete: false
  }
  productAdded = false;
  productsCount = 0;

  showTrash = false;
  value = 1;
  i = 1;
  totalAmount: number = 0;

  @Input() data: any;
  @Input() sale: boolean = false;
  @Input() addToList: boolean = true;

  @Output() productAddToList: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() productAddToCart: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() productRemoveFromCart: EventEmitter<ProductToDelete> = new EventEmitter<ProductToDelete>();
  @Output() productPlusQty: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() productMinusQty: EventEmitter<Product> = new EventEmitter<Product>();


  constructor(
    public dialog: MatDialog,
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    // this.cartService.getcartActiveObservable().subscribe(
    //   resp => {
    //     console.log("El carrito a utilizar", resp);
    //   }
    // );


    // console.log("ayuda", this.data.id)
    // console.log("ayuda", this.data.inCart)
    if (this.data.inCart) {
      this.productAdded = this.data.inCart
      this.productsCount = this.data.qty
      this.i = this.data.qty
    }
    this.i = this.data.qty
  }

  productDetailsDialog(data: Product): void {
    const dialog = this.dialog
      .open(ProductDetailsComponent, {
        height: '810px',
        width: '1400px',
        disableClose: false,
        data,
      });

  }


  /* Funciones para el boton de agregar */
  handleDelete() {
    this.productAdded = !this.productAdded;
    console.log(this.data)
    this.productRemoveFromCart.emit({
      p: this.data,
      toDelete: true
    });
  }

  handleMinus() {
    console.log(this.i)
    if (this.i > 1) {
      this.i--;
      this.value = this.i;
      if (this.i === 1) {
        this.showTrash = true;
      }
      this.data.qty--;
      this.productMinusQty.emit(this.data);
    }
  }

  handlePlus() {
    if (this.i < 10) {
      this.i++;
      this.showTrash = false;
      this.data.qty++;
      console.log('thisdata: ', this.data)
      this.productPlusQty.emit(this.data);
    }
  }

}
