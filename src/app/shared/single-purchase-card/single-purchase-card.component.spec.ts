import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglePurchaseCardComponent } from './single-purchase-card.component';

describe('SinglePurchaseCardComponent', () => {
  let component: SinglePurchaseCardComponent;
  let fixture: ComponentFixture<SinglePurchaseCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SinglePurchaseCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglePurchaseCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
