import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RightSidenavHistoryFilterComponent } from './right-sidenav-history-filter.component';

describe('RightSidenavHistoryFilterComponent', () => {
  let component: RightSidenavHistoryFilterComponent;
  let fixture: ComponentFixture<RightSidenavHistoryFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RightSidenavHistoryFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RightSidenavHistoryFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
