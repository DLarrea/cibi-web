import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { OrderHistoryFilter } from 'src/app/interfaces/orderHistoryFilter';
import { Store } from 'src/app/models/store';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-right-sidenav-history-filter',
  templateUrl: './right-sidenav-history-filter.component.html',
  styleUrls: ['./right-sidenav-history-filter.component.scss']
})
export class RightSidenavHistoryFilterComponent implements OnInit {

  filterGroup = this.formBuilder.group({
    startDate: null,
    endDate: null,
    store: null,
    purchaseType: null,
  });

  types = [
    'delivery',
    'pickup'
  ];
  stores: Store[] = [];

  @Output() filterApplied = new EventEmitter<any>();
  @Output() clearFilter = new EventEmitter<boolean>(false);
  @Output() closePressed = new EventEmitter<boolean>(false);

  constructor(
    private formBuilder: FormBuilder,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.storeService.getStores().subscribe(
      resp => {
        this.stores = resp;
      },
      error => {
        console.log('No se pudieron traer las tiendas', error);
      }
    );
  }

  filterNow(): void {
    const obj: OrderHistoryFilter = {
      dateRange: {
        start: this.filterGroup.value.startDate,
        end: this.filterGroup.value.endDate
      },
      storeList: this.filterGroup.value.store,
      purchaseTypeList: this.filterGroup.value.purchaseType
    }
    this.filterApplied.emit(obj);
  }

  cFilter(): void {
    this.clearFilter.emit(true);
  }

  close(): void {
    this.closePressed.emit(true);
  }

}
