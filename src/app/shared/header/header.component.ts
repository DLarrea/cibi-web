import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MenuConfigService } from 'src/app/services/menu-config.service';
import { PanelService } from "../../services/panel.service";
import { TemplatePortal } from "@angular/cdk/portal";
import { CartService } from 'src/app/services/cart.service';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router'
import { CartInfo } from 'src/app/models/cart';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';
import { ContactComponent } from 'src/app/dialog/contact/contact.component';
import { MatDialog } from '@angular/material/dialog';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  items: MenuItem[] = [
    {
      label: 'Mi perfil',
      icon: 'pi pi-fw pi-user',
      routerLink: '/profile'
    },
    {
      label: 'Cerrar sesión',
      icon: 'pi pi-fw pi-sign-out',
      command: () => {
        this.logout();
      }
    }
  ];
  imageApi = environment.image_api;
  imageFile: File = new File([], '');


  form = this.formBuilder.group({
    image: ['']

  });

  userObservable$ = this.authService.getLoggedUserObservable();
  toggledState = false;
  userExist = false;
  cartInfo = new CartInfo();
  user!: User;
  displayPurchase: boolean = false;
  @Input() backGroundColor: string = "transparent";

  @Output() login = new EventEmitter<boolean>();

  isStore = false;
  constructor(
    public menuConfigService: MenuConfigService,
    public formBuilder: FormBuilder,
    private authService: AuthService,
    private panelService: PanelService,
    private vcf: ViewContainerRef,
    private cartService: CartService,
    private userService: UserService,
    private router: Router,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.userObservable$.subscribe(
      (resp: any) =>{
        this.userExist = resp? true : false;
        this.user = resp
        if (this.userExist) {
          this.cartService.setCartActive$(this.user.id);
          this.cartService.getcartActiveObservable().subscribe(res => {
            console.log(res)
            this.cartInfo = res
          });
        }
      }
    )
    this.getUserData()
  }

  getUserData() {
    this.userService.me().subscribe(resp => {
      console.log("hola, llamo al servicio", resp);
      this.user = resp;
    });
  }

  toggleSidebarMenu(): void {
    this.menuConfigService.toggleSidebarMenu();
  }

  ingresar(): void {
    this.login.emit(true);
  }

  changePassword(): void {
    this.router.navigate(['/change-password'])
  }

  goToProfile(): void {
    this.router.navigate(['/profile']);
  }

  logout(): void {
    this.authService.logout();
  }

}
