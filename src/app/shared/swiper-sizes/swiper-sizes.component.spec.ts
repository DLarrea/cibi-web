import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwiperSizesComponent } from './swiper-sizes.component';

describe('SwiperSizesComponent', () => {
  let component: SwiperSizesComponent;
  let fixture: ComponentFixture<SwiperSizesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwiperSizesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwiperSizesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
