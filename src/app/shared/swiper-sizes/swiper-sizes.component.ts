import { Component, Input } from '@angular/core';
import { Product } from 'src/app/models/product';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-swiper-sizes',
  templateUrl: './swiper-sizes.component.html',
  styleUrls: ['./swiper-sizes.component.scss']
})
export class SwiperSizesComponent {
  textRow: string | undefined = '';
  numSlides = 0;
  imageApi = environment.image_api;

  @Input() productVariants: Product[] = [];

  constructor() { }
}
