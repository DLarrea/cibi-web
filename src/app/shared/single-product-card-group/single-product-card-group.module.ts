import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';
import SwiperCore, { Autoplay, Keyboard, Navigation, Pagination } from "swiper";
import { SwiperModule } from 'swiper/angular';
import { SingleProductCardGroupComponent } from './single-product-card-group.component';

// install Swiper modules
SwiperCore.use([Autoplay, Keyboard, Pagination, Navigation]);


@NgModule({
  declarations: [
    SingleProductCardGroupComponent
  ],
  exports: [
    SingleProductCardGroupComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    SwiperModule,
  ]
})
export class SingleProductCardGroupModule { }
