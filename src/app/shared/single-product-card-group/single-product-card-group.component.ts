import { Component, Input, OnInit } from '@angular/core';
import { ProductGroup } from 'src/app/models/product';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-single-product-card-group',
  templateUrl: './single-product-card-group.component.html',
  styleUrls: ['./single-product-card-group.component.scss']
})
export class SingleProductCardGroupComponent implements OnInit {

  imageApi: string = environment.image_api;

  @Input() productGroupData: ProductGroup = new ProductGroup();
  
  constructor(
  ) { }

  ngOnInit(): void {
   // console.log(this.productGroupData);
  }

  slideChange(productGroup: ProductGroup): void {
    console.log(productGroup);
  }


}

