import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleProductCardGroupComponent } from './single-product-card-group.component';

describe('SingleProductCardGroupComponent', () => {
  let component: SingleProductCardGroupComponent;
  let fixture: ComponentFixture<SingleProductCardGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleProductCardGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleProductCardGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
