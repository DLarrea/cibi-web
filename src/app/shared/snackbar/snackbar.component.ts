import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { Snackbar } from 'src/app/interfaces/snackbar';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {
  // verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private sB: MatSnackBar,
    @Inject(MAT_SNACK_BAR_DATA) public data: Snackbar
  ) { }

  ngOnInit(): void {
    console.log("Se debe mostrar en el snackbar", this.data);
  }

  dismiss(): void{
    this.sB.dismiss();
  }
}
