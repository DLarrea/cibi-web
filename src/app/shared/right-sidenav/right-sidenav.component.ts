import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-right-sidenav',
  templateUrl: './right-sidenav.component.html',
  styleUrls: ['./right-sidenav.component.scss']
})
export class RightSidenavComponent implements OnInit {

  brandList: any[] = [];
  filterGroup: FormGroup; 
  @Input() set brands(list: any[]){
    this.brandList = list;
  };

  constructor(private formBuilder: FormBuilder){
    this.filterGroup = this.formBuilder.group({
      brands: [],
      desde: 0,
      hasta: 0
    });
  }

  ngOnInit(): void {}

  filtrar = () => {

  }

  // searchFormControl = new FormControl(null);

  // brandFiltered: any[] = [];
  // allBrands: any[] = [];
  // @Input() set brands(list: any[]){
  //   this.brandFiltered = list;
  //   this.allBrands = list;
  // };

  // @Input() opened: boolean = true;
  // @Input() numMode: number = 1;

  // @Output() closePressed = new EventEmitter<boolean>(false);
  // @Output() filterApplied = new EventEmitter<any>();

  // constructor(
  //   private formBuilder: FormBuilder,
  // ) { }

  // ngOnInit(): void {
  //   setTimeout (() => {
  //     console.log(this.brands);
  //   }, 5000);
  //   this.searchFormControl.valueChanges.subscribe((val) => {
  //     this.doFilter(val);
  //   });
  // }

  // doFilter(value: string): void {
  //   const regex = new RegExp(value, 'gi');
  //   this.brandFiltered = this.allBrands.filter(v => regex.test(v.brand));
  // }

  // filterNow(): void {
  //   let nameBrandList: any[] = [];
  //   this.brandFiltered.filter(p => p.checked).map(
  //     r => nameBrandList.push(r.brand)
  //   );
  //   const data = {
  //     brands: nameBrandList,
  //     price_from: (this.filterGroup.get('desde')?.value)? this.filterGroup.get('desde')?.value : 0,
  //     price_to: (this.filterGroup.get('hasta')?.value)? this.filterGroup.get('hasta')?.value : 0,
  //   };
  //   this.filterApplied.emit(data);
  // }

  // close(): void {
  //   this.closePressed.emit(true);
  // }
}
