import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddProductComponent } from 'src/app/dialog/add-product/add-product.component';
import { ProductDetailsComponent } from 'src/app/dialog/product-details/product-details.component';
import { CartInfo, Order, ProductToDelete } from 'src/app/models/cart';
import { Product } from 'src/app/models/product';
import { CartService } from 'src/app/services/cart.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-single-product-card',
  templateUrl: './single-product-card.component.html',
  styleUrls: ['./single-product-card.component.scss']
})
export class SingleProductCardComponent implements OnInit {
  imageApi = environment.image_api;
  showAdd = true;
  cartInfo: CartInfo = new CartInfo;
  cartOrder: Order = new Order;

  productToAdd: Product = new Product();
  productToDelete: ProductToDelete = {
    p: new Product(),
    toDelete: false
  }
  productAdded = false;
  productsCount = 0;

  showTrash = false;
  value = 1;
  i = 1;

  @Input() data: Product = new Product;
  @Input() sale: boolean = false;
  @Input() addToList: boolean = true;

  @Output() productAddToList: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() productAddToCart: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() productRemoveFromCart: EventEmitter<ProductToDelete> = new EventEmitter<ProductToDelete>();
  @Output() productPlusQty: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() productMinusQty: EventEmitter<Product> = new EventEmitter<Product>();

  constructor(
    public dialog: MatDialog,
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    // this.cartService.getcartActiveObservable().subscribe(
    //   resp => {
    //     console.log("El carrito a utilizar", resp);
    //   }
    // );


    // console.log("ayuda", this.data.id)
    // console.log("ayuda", this.data.inCart)
    if (this.data.inCart){
      this.productAdded = this.data.inCart;
      this.productsCount = this.data.qty;
      this.i = this.data.qty;
      if(this.i === 1){
        this.showTrash = true;
      }
    }
  }

  productDetailsDialog(data: Product): void {
    const dialog = this.dialog
      .open(ProductDetailsComponent, {
        height: '810px',
        width: '1400px',
        disableClose: false,
        data,
      });

    // dialog.afterClosed().subscribe(data => {
    //   if (data) {
    //     const address = new Address();
    //     address.name = data.name;
    //     address.address = data.address;
    //     address.latLng = {"lat": data.lat,"lon": data.long};
    //     this.addressList.push(address);
    //     console.log(data);
    //   }
    // });
  }

  addProductToCart(data: Product): void {
    this.productToAdd = data;
    this.productAdded = true;
    this.showTrash = true;
    this.productToAdd.qty = this.value;
    console.log("watafa")

    this.productAddToCart.emit(this.productToAdd);
  }


  /* Funciones para el boton de agregar */
  handleDelete() {
    this.productAdded = !this.productAdded;
    this.productRemoveFromCart.emit({
      p: this.data,
      toDelete: true
    });
  }

  handleMinus() {
    if (this.i > 0) {
      this.i--;
      this.value = this.i;
      if (this.i === 1) {
        this.showTrash = true;
      }
      this.data.qty --;
      this.productMinusQty.emit(this.data);
    }
  }

  handlePlus() {
    if (this.i < 10) {
      this.i++;
      this.showTrash = false;
      this.data.qty ++;
      this.productPlusQty.emit(this.data);
    }
  }

  selectedProductToList(data: Product): void {
    data.checked = !data.checked;
    this.productAddToList.emit(data);
  }

  openDialog(data: Product) {
    console.log (data)
    this.dialog.open(AddProductComponent,
      {
        height: '580px',
        width: '500px',
        disableClose: false,
        data: data,
      });
  }
}

