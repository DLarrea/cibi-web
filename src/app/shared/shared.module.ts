import { PortalModule } from "@angular/cdk/portal";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// import Swiper core and required modules
import SwiperCore, { Autoplay, Keyboard, Navigation, Pagination } from "swiper";
import { SwiperModule } from 'swiper/angular';
import { DirectivesModule } from '../directives/directives.module';
import { MaterialModule } from '../material/material.module';
import { MyPurchaseComponent } from "../pages/my-purchase/my-purchase.component";
import { HeaderComponent } from './header/header.component';
import { LeftSidenavComponent } from './left-sidenav/left-sidenav.component';
import { RightSidenavHistoryFilterComponent } from './right-sidenav-history-filter/right-sidenav-history-filter.component';
import { RightSidenavComponent } from './right-sidenav/right-sidenav.component';
import { SingleProductCardComponent } from './single-product-card/single-product-card.component';
import { SinglePurchaseCardComponent } from './single-purchase-card/single-purchase-card.component';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { SwiperCharacteristicsComponent } from './swiper-characteristics/swiper-characteristics.component';
import { SwiperSizesComponent } from './swiper-sizes/swiper-sizes.component';
import { SwiperComponent } from './swiper/swiper.component';
import {SidebarModule} from 'primeng/sidebar';


// install Swiper modules
SwiperCore.use([Autoplay, Keyboard, Pagination, Navigation]);

//primeng
import {ButtonModule} from 'primeng/button';
import {AvatarModule} from 'primeng/avatar';
import {TieredMenuModule} from 'primeng/tieredmenu';
import {InputTextModule} from 'primeng/inputtext';

@NgModule({
  declarations: [
    HeaderComponent,
    LeftSidenavComponent,
    RightSidenavComponent,
    SnackbarComponent,
    SwiperComponent,
    SwiperCharacteristicsComponent,
    SwiperSizesComponent,
    SingleProductCardComponent,
    RightSidenavHistoryFilterComponent,
    MyPurchaseComponent,
    SinglePurchaseCardComponent,

  ],
  exports: [
    HeaderComponent,
    LeftSidenavComponent,
    RightSidenavComponent,
    SnackbarComponent,
    SwiperComponent,
    SwiperCharacteristicsComponent,
    SwiperSizesComponent,
    SingleProductCardComponent,
    RightSidenavHistoryFilterComponent,
    SinglePurchaseCardComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    SwiperModule,
    SidebarModule,
    DirectivesModule,
    FormsModule,
    ReactiveFormsModule,
    PortalModule,
    ButtonModule,
    AvatarModule,
    TieredMenuModule,
    InputTextModule
  ]
})
export class SharedModule { }
