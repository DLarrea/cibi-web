import { AfterViewInit, Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MenuItem } from 'src/app/interfaces/menu-item';
import { AuthService } from 'src/app/services/auth.service';
import { MenuConfigService } from 'src/app/services/menu-config.service';
import { ResponsiveService } from 'src/app/services/responsive.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';
import { PanelService } from "../../services/panel.service";
import { UserPhotoService } from 'src/app/services/user-photo.service';
import { ContactComponent } from 'src/app/dialog/contact/contact.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-left-sidenav',
  templateUrl: './left-sidenav.component.html',
  styleUrls: ['./left-sidenav.component.scss']
})
export class LeftSidenavComponent implements OnInit, OnDestroy, AfterViewInit {

  display = true;
  imageApi = environment.image_api;
  user: any;
  imageFile: File = new File([], '');

  form = this.formBuilder.group({
    image: ['']

  });

  userObservable$ = this.authService.getLoggedUserObservable();
  toggleSidebarMenuSubscription?: Subscription;
  sidebarMode: MatDrawerMode = 'over';
  @ViewChild('rightPanel', { static: true }) private rightPanel: MatSidenav | undefined;

  menuList: MenuItem[] = [
    {
      displayName: 'Inicio',
      iconName: 'basket-outline',
      route: '/products',
      disabled: false,
      extra: null
    },
    {
      displayName: 'Mi Perfil',
      iconName: 'person-outline',
      route: '/profile',
      disabled: false,
      extra: null
    },
    {
      displayName: 'Mis preferencias',
      iconName: 'options-outline',
      route: '/mypreferences',
      disabled: false,
      extra: null
    },
    // {
    //   displayName: 'Mis carritos',
    //   iconName: 'layers-outline',
    //   route: '/mycarts',
    //   disabled: false,
    //   extra: null
    // },
    // {
    //   displayName: 'Mis Direcciones',
    //   iconName: 'location-outline',
    //   route: '/myaddress',
    //   disabled: false,
    //   extra: null
    // },
    // {
    //   displayName: 'Mis suscripciones',
    //   iconName: 'bookmarks-outline',
    //   route: '/',
    //   disabled: false,
    //   extra: null
    // },
    {
      displayName: 'Reportes',
      iconName: 'bar-chart-outline',
      route: '/myreport',
      disabled: false,
      extra: null
    },
    {
      displayName: 'Tiendas cercanas',
      iconName: 'store-sidebar',
      route: '/storesnearby',
      disabled: false,
      extra: null
    },
    {
      displayName: 'Historial de compras',
      iconName: 'bag-outline',
      route: '/shopping-history',
      disabled: false,
      extra: null
    },
    {
      displayName: 'Atención al cliente',
      iconName: 'headphones',
      route: null,
      disabled: false,
      extra: 'popup-cliente'
    },
    {
      displayName: 'Cerrar sesión',
      iconName: 'log-out-outline',
      route: '/cerrar-sesion',
      disabled: false,
      extra: null
    },
  ];

  @ViewChild('sidenav') sidenav?: MatSidenav;

  @HostListener('window:resize')
  onResize(): void {
    this.configSidebarMode();
  }

  constructor(
    public formBuilder: FormBuilder,
    public menuConfigService: MenuConfigService,
    private responsiveService: ResponsiveService,
    private authService: AuthService,
    public panelService: PanelService,
    private userService: UserService,
    private UserPhotoService: UserPhotoService,
    public dialog: MatDialog,
  ) {
    this.authService.getLoggedUserObservable().subscribe(
      data => {
        this.user = data;
      }
    )
  }

  ngOnInit(): void {
    // this.user = this.authService.loggedUser();
    this.fetchToggleSidebarMenu();
    this.panelService.panel = this.rightPanel;
  }

  ngOnDestroy(): void {
    this.toggleSidebarMenuSubscription?.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.configSidebarMode();
  }

  closeSidebarMenu(path: string | null): void {
    this.menuConfigService.setSidebarMenu(false);

    if (path && path === '/cerrar-sesion') {
      this.authService.logout();
    } else if(path && path === 'popup-cliente'){
      this.openAtencionCliente();
    }

  }

  private fetchToggleSidebarMenu(): void {
    this.toggleSidebarMenuSubscription = this.menuConfigService
      .getToggleSidebarMenuObservable().subscribe(
        data => {
          this.display = data;
        }
      )
  }

  private configSidebarMode(): void {
    setTimeout(() => {
      this.sidebarMode = 'over';
      this.menuConfigService.setSidebarMenu(false);
      // if (this.responsiveService.isMobileScreen) {
      //   this.sidebarMode = 'over';
      //   this.menuConfigService.setSidebarMenu(false);
      // } else {
      //   this.sidebarMode = 'over';
      //   this.menuConfigService.setSidebarMenu(false);
      // }
    });
  }

  private openAtencionCliente(): void {
    this.dialog.open(ContactComponent, {
      disableClose: false,
      data: {
      }
    })
    .afterClosed();
  }

  hideSidebar = () => {
    this.menuConfigService.setSidebarMenu(false);
  }
}
