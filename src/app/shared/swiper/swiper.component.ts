import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CartItems, ProductToDelete } from 'src/app/models/cart';
import { MediaResource } from 'src/app/models/media';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper.component.html',
  styleUrls: ['./swiper.component.scss']
})
export class SwiperComponent{
  textRow: string | undefined = '';

  @Input() productList: Product[] = [];
  @Input() cartItems: CartItems[] = [];
  @Input() videoList: MediaResource[]=[];
  @Input() set textRowSwiper(val: string | undefined) {
    this.textRow = val;
  }

  @Input() likes: boolean = false;
  @Input() sale: boolean = false;
  @Input() numSlides: number = 5;
  @Input() addToList: boolean = false;

  @Input() productsCount: number = 0;
  @Input() isBySubCategory = false;
  @Input() set idSubCategory(id: number){
    if(id > 0){
      this.productService.getProductsBySubCategory(id, '', 0, 100).subscribe(
        resp => {
          this.productList = resp;
          this.productInCart();
        }
      );
    }
  };

  @Input() set sortType(num: number){
    switch(num) {
      case 1: {
        this.productList.sort((a, b) => (a.name > b.name) ? 1 : -1);
        break;
      }
      case 2: {
        this.productList.sort((a, b) => (a.name < b.name) ? 1 : -1);
        break;
      }
      case 3: {
        this.productList.sort((a, b) => (a.price > b.price) ? 1 : -1);
        break;
      }
      case 4: {
        this.productList.sort((a, b) => (a.price < b.price) ? 1 : -1);
        break;
      }
      default: {
        // console.log('No se pudo ordenar');
        break;
      }
    }
  };

  @Output() showMore = new EventEmitter<boolean>(false);
  @Output() productAddToList = new EventEmitter<Product>();
  @Output() productAddToCart = new EventEmitter<Product>();
  @Output() productRemoveFromCart = new EventEmitter<object>();
  @Output() productPlusQty: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() productMinusQty: EventEmitter<Product> = new EventEmitter<Product>();

  // @HostListener('window:resize')
  //onResize(): void {
  //   window.outerWidth
  // hacer algo
  // }

  constructor(
    private productService: ProductService,
  ) { }

  // ngOnInit(): void {
  //   // this.productInCart();
  //   // console.log("product list: ", this.productList);
  // }

  productInCart(){
    if(this.productList){
      let lisProdIds: number[] = [];
      this.cartItems.forEach(element => {
        // console.log("product Id: ", element.productId)
        lisProdIds.push(element.productId);
        const index = this.productList.findIndex(p => p.id === element.productId);
        if (index > -1) {
          this.productList[index].inCart = true;
          this.productList[index].cartItemId = element.id;
          this.productList[index].qty = element.qty;
          // console.log("Encontre este producto ya en el carrito: ", this.productList[index])
        }
      });    }
  }
  sM(): void {
    this.showMore.emit(true);
  }

  onProductAddToCart(event: Product): void {
    this.productAddToCart.emit(event);
  }

  onProductPlus(event: Product): void {
    this.productPlusQty.emit(event);
  }

  onProductMinus(event: Product): void {
    this.productMinusQty.emit(event);
  }

  onProductRemoveFromCart(event: ProductToDelete): void {
    this.productRemoveFromCart.emit(event);
  }

  onProductAddToList(event: Product): void {
    this.productAddToList.emit(event);
  }
}
