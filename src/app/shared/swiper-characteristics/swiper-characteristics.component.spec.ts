import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwiperCharacteristicsComponent } from './swiper-characteristics.component';

describe('SwiperCharacteristicsComponent', () => {
  let component: SwiperCharacteristicsComponent;
  let fixture: ComponentFixture<SwiperCharacteristicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwiperCharacteristicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwiperCharacteristicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
