import { Component, Input } from '@angular/core';
import { Characteristic } from 'src/app/models/characteristic';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-swiper-characteristics',
  templateUrl: './swiper-characteristics.component.html',
  styleUrls: ['./swiper-characteristics.component.scss']
})
export class SwiperCharacteristicsComponent {
  textRow: string | undefined = '';
  numSlides = 0;
  imageApi = environment.image_api;

  @Input() charSlide: Characteristic[] = [];

  constructor() { }

}
