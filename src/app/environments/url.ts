export const url = {
  home: {
    _root: '',
  },
  landing: {
    _root: 'landing',
  },
  account: {
    _root: 'account',
    login: 'login',
    recoverAccount: 'recover-password',
    confirmAccount: 'confirm-account',
  },
  register: {
    _root: 'register',
    notifications: 'notifications',
    preferences: 'preferences',
    listStoresNearby: 'list-stores-nearby',
    address: 'address'
  },
  products: {
    _root: 'products',
  },
  profile: {
    _root: 'profile',
  },
  mycarts: {
    _root: 'mycarts',
    cart: 'cart'
  },
  myaddress: {
    _root: 'myaddress'
  },
  shoppinghistory: {
    _root: 'shopping-history'
  },
  mypreferences: {
    _root: 'mypreferences'
  },
  mybilling: {
    _root: 'mybilling'
  },
  myreport: {
    _root: 'myreport'
  },
  videos: {
    _root: 'videos',
  },
  mypurchasebuy: {
    _root: 'mypurchase'
  },
  changepassword: {
    _root: 'change-password'
  },
  storesnearby: {
    _root: 'storesnearby'
  }
};
