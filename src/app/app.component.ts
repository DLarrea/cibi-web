import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatIconRegistry } from '@angular/material/icon';
import { MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { LoadingService } from './services/loading.service';
import { registerLocaleData } from '@angular/common';
import localePY from '@angular/common/locales/es-PY';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  loading = false;
  // loadingDialogRef?: MatDialogRef<LoadingDialogComponent>;
  loadingSubscription?: Subscription;
  httpErrorSubscription?: Subscription;
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    // private httpErrorService: HttpErrorService,
    private loadingService: LoadingService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.iconRegistry("cart","../assets/icon/cart.svg");
    this.iconRegistry("close-outline","../assets/icon/close-outline.svg");
    this.iconRegistry("close-outline-blue","../assets/icon/close-outline-blue.svg");
    this.iconRegistry("bag-outline","../assets/icon/bag-outline.svg");
    this.iconRegistry("bar-chart-outline","../assets/icon/bar-chart-outline.svg");
    this.iconRegistry("basket-outline","../assets/icon/basket-outline.svg");
    this.iconRegistry("bookmarks-outline","../assets/icon/bookmarks-outline.svg");
    this.iconRegistry("card-outline","../assets/icon/card-outline.svg");
    this.iconRegistry("cash-outline","../assets/icon/cash-outline.svg");
    this.iconRegistry("information-circle-outline","../assets/icon/information-circle-outline.svg");
    this.iconRegistry("layers-outline","../assets/icon/layers-outline.svg");
    this.iconRegistry("location-outline","../assets/icon/location-outline.svg");
    this.iconRegistry("options-outline","../assets/icon/options-outline.svg");
    this.iconRegistry("person-outline","../assets/icon/person-outline.svg");
    this.iconRegistry("videocam-outline","../assets/icon/videocam-outline.svg");
    this.iconRegistry("trash-outline","../assets/icon/trash-outline.svg");
    this.iconRegistry("trash-outline-orange","../assets/icon/trash-outline-orange.svg");
    this.iconRegistry("trash-outline-white","../assets/icon/trash-outline-white.svg");
    this.iconRegistry("create-outline","../assets/icon/create-outline.svg");
    this.iconRegistry("edit-outline-orange","../assets/icon/edit-outline-orange.svg");
    this.iconRegistry("log-out-outline","../assets/icon/log-out-outline.svg");
    this.iconRegistry("chevron-back-outline","../assets/icon/chevron-back-outline.svg");
    this.iconRegistry("chevron-forward-outline","../assets/icon/chevron-forward-outline.svg");
    this.iconRegistry("image-outline-grey","../assets/icon/image-outline-grey.svg");
    this.iconRegistry("funnel-outline","../assets/icon/funnel-outline.svg");
    this.iconRegistry("swap-vertical-outline","../assets/icon/swap-vertical-outline.svg");
    this.iconRegistry("heart-outline-orange","../assets/icon/heart-outline-orange.svg");
    this.iconRegistry("share-outline-orange","../assets/icon/share-outline-orange.svg");
    this.iconRegistry("people","../assets/icon/people.svg");
    this.iconRegistry("arrow-back-outline","../assets/icon/arrow-back-outline.svg");
    this.iconRegistry("ellipsis-horizontal","../assets/icon/ellipsis-horizontal.svg");
    this.iconRegistry("person-circle-outline","../assets/icon/person-circle-outline.svg");
    this.iconRegistry("create-outline-orange","../assets/icon/create-outline-orange.svg");
    this.iconRegistry("create-outline-white","../assets/icon/create-outline-white.svg");
    this.iconRegistry("trash-outline-orange","../assets/icon/trash-outline-orange.svg");
    this.iconRegistry("cart-white","../assets/icon/cart-white.svg");
    this.iconRegistry("add-circle-outline","../assets/icon/add-circle-outline.svg");
    this.iconRegistry("checkmark-circle","../assets/icon/checkmark-circle.svg");
    this.iconRegistry("home-outline-orange","../assets/icon/home-outline-orange.svg");
    this.iconRegistry("car-sport-outline-orange","../assets/icon/car-sport-outline-orange.svg");
    this.iconRegistry("delivery","../assets/icon/delivery.svg");
    this.iconRegistry("pickup","../assets/icon/pickup.svg");
    this.iconRegistry("store","../assets/icon/store.svg");
    this.iconRegistry("remove-circle-outline","../assets/icon/remove-circle-outline.svg");
    this.iconRegistry("add-circle-outline","../assets/icon/add-circle-outline.svg");
    this.iconRegistry("headphones","../assets/icon/headphones.svg");
    this.iconRegistry("store-sidebar","../assets/icon/store-sidebar.svg");
  }

  ngOnInit(): void {
    //this.listenErrors();
    this.listenLoading();
    registerLocaleData( localePY );
  }

  ngOnDestroy(): void {
    this.httpErrorSubscription?.unsubscribe();
    this.loadingSubscription?.unsubscribe();
  }

  // listenErrors(): void {
  //   this.httpErrorSubscription = this.httpErrorService
  //     .getHttpErrorListObservable()
  //     .subscribe((errors: string[]) => {
  //       this.snackBar.openFromComponent(HttpErrorSnackBarComponent, {
  //         verticalPosition: this.verticalPosition,
  //         panelClass: ['http-error-snackbar'],
  //         duration: 999999,
  //         data: errors,
  //       });
  //     });
  // }

  listenLoading(): void {
    this.loadingSubscription = this.loadingService
      .getLoadingObservable()
      .subscribe({
        next: (loading) => {
          setTimeout(() => {
            this.loading = loading;
          }, 0);
        }
      });
  }

  iconRegistry(name: string, path: string): void{
    this.matIconRegistry.addSvgIcon(
      name,
      this.domSanitizer.bypassSecurityTrustResourceUrl(path),
    )
  }
  // openLoadingDialog(): MatDialogRef<LoadingDialogComponent> {
  //   return this.dialog.open(LoadingDialogComponent, {
  //     panelClass: 'loading-dialog',
  //     disableClose: true,
  //   });
  // }
}
