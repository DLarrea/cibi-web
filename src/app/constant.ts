export const AUTH_KEY = 'auth';
export const TOKEN_KEY = 'TOKEN';
export const USER = 'me';
export const COMERCIO = 'comercio';
export const VISIBLE_STATUS = {
  SI: 'SI',
  NO: 'NO',
};
export const VISIBLE_STATUS_FILTER_LIST = Object.values(VISIBLE_STATUS);
export const PRINCIPAL_BREAKPOINT = 1280;
export const BREAK_POINTS = {
  'sm': '768px',
  'md': '1024px',
  'lg': '1200px'
};