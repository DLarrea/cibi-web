import { Moment } from "moment";
import { Store } from "../models/store";

export interface OrderHistoryFilter{
  dateRange?: DateRange<Moment>;
  purchaseTypeList?: string[];
  storeList?: Store[];
}

export interface DateRange<D> {
  start?: D | null;
  end?: D | null;
}
