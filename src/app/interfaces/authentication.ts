import { User } from './user';

export interface Authentication {
  access_token: string;
  token_type: string;
  user: User;
}
