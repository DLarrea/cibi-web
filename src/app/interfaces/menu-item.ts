export interface MenuItem {
  displayName: string;
  route: string | null;
  routeExact?: boolean;
  children?: MenuItem[];
  iconName?: string;
  disabled?: boolean;
  extra?: any;
}
