import { MatSnackBarVerticalPosition } from "@angular/material/snack-bar";

export interface Snackbar {
  code: number,
  message: string,
  success: boolean,
  className: string,
  classAction: string,
  duration: number,
  position: MatSnackBarVerticalPosition
}