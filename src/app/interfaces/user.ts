export interface User {
  id: number;
  name: string;
  last_name: string;
  email: string;
  address: string;
  phone_number: string;
  ruc: string;
  cibi_user: boolean;
  google_user: boolean;
  fb_user: boolean;
  apple_user: boolean;
  password: boolean;
  is_active: boolean;
}

export interface RecoverPassword {
  email: string;
}
