import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { InputThousandsSeparatorDirective } from './input-thousands-separator.directive';
import { OnlyPositiveNumberDirective } from './only-positive-number.directive';


@NgModule({
  declarations: [
    OnlyPositiveNumberDirective,
    InputThousandsSeparatorDirective
  ],
  exports: [
    OnlyPositiveNumberDirective,
    InputThousandsSeparatorDirective
  ],
  imports: [
    CommonModule,
  ]
})
export class DirectivesModule { }
