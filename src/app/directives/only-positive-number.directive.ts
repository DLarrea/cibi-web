import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlyPositiveNumber]'
})
export class OnlyPositiveNumberDirective {

  constructor() { }

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent): void {
    // tslint:disable-next-line: deprecation
    if (event.which === 109 || event.which === 189) {
      event.preventDefault();
      return;
    }
  }

}
