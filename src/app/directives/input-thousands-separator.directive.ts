import { Directive, ElementRef, forwardRef, HostListener, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { MAT_INPUT_VALUE_ACCESSOR } from '@angular/material/input';
import { numberWithCommas, numberWithoutCommas, preventTypingNonNumericCharacters, zeroCommaTerminatedRegex } from '../helper/thousands-separator';

@Directive({
  selector: '[appInputThousandsSeparator]',
  providers: [
    { provide: MAT_INPUT_VALUE_ACCESSOR, useExisting: InputThousandsSeparatorDirective },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputThousandsSeparatorDirective),
      multi: true,
    }
  ]
})
export class InputThousandsSeparatorDirective {

  // tslint:disable-next-line:variable-name
  private _value: number | undefined;

  constructor(private elementRef: ElementRef<HTMLInputElement>) {
    if (elementRef.nativeElement.type === 'number') {
      throw Error('El input con la directiva appInputThousandsSeparator debe ser tipo texto y no tipo numérico');
    }
  }

  get value(): string | number | undefined {
    return this._value ? Number(this._value) : undefined;
  }

  @Input()
  set value(value: string | number | undefined) {
    this._setValue(value);
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent): void {
    preventTypingNonNumericCharacters(event);
  }

  @HostListener('input', ['$event.target.value'])
  onInput(value: string | undefined): void {
    if (typeof value === 'string' && !(zeroCommaTerminatedRegex.test(value))) {
      this._value = numberWithoutCommas(value);
      this._onChange(this._value);
      this.formatValue(value);
    } else {
      this.elementRef.nativeElement.value = value?.toString() ?? '';
    }
  }

  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }

  _onChange(value: any): void { }

  writeValue(value: any): void {
    // En este entra cuando se carga el valor desde el angularForm y no lo que el usuario introduce por teclado
    this._setValue(value);
  }

  registerOnChange(fn: (value: any) => void): void {
    this._onChange = fn;
  }

  registerOnTouched(): void { }

  private _setValue(value: string | number | undefined): void {
    this._value = numberWithoutCommas(value);
    this.formatValue(this._value);
  }

  private formatValue(value: string | number | undefined): void {
    this.elementRef.nativeElement.value = numberWithCommas(value);
  }
}
