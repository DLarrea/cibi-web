import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputThousandsSeparatorDirective } from './input-thousands-separator.directive';

@Component({ template: '<input #directive appInputThousandsSeparator>' })
class TestComponent {
  @ViewChild('directive') directive?: InputThousandsSeparatorDirective;
}

describe('InputThousandsSeparatorDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestComponent,
        InputThousandsSeparatorDirective,
      ]
    });
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
  });

  it('should create an instance', () => {
    fixture.detectChanges();
    const directive = component.directive;
    expect(directive).toBeTruthy();
  });
});
